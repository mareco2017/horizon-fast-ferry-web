<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone_number')->unique()->nullable();
            $table->string('email', 150)->unique()->nullable();
            $table->string('first_name', 50)->nullable()->default('');
            $table->string('last_name', 50)->nullable()->default('');
            $table->tinyInteger('gender')->nullable();
            $table->date('dob')->nullable();
            $table->string('device_type')->nullable()->default('');
            $table->string('timezone')->nullable();
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
