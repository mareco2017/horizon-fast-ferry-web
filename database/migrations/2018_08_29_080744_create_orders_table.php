<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference_number')->nullable();
            $table->longText('description')->nullable();
            $table->unsignedInteger('contact_person_id')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->json('options')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->nullableMorphs('ownable');
            
            $table->foreign('contact_person_id')->references('id')->on('contact_persons');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
