<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 150);
            $table->unsignedInteger('cover_id')->nullable();
            $table->unsignedInteger('publisher_id')->nullable();
            $table->longText('tags')->nullable();
            $table->longText('intro')->nullable();
            $table->longText('desc')->nullable();
            $table->string('slug', 200);
            $table->tinyInteger('status')->default(0);
            
            $table->foreign('cover_id')->references('id')->on('attachments');
            $table->foreign('publisher_id')->references('id')->on('admins');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
