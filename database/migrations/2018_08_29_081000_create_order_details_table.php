<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->double('price');
            $table->double('discount');
            $table->integer('quantity')->default(1);
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->dateTime('expiry_date')->nullable();
            $table->dateTime('claim_date')->nullable();
            $table->json('options')->nullable();
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('passenger_id')->nullable();
            $table->tinyInteger('type')->nullable(); // Ticket: Adult / Child / Infant | Car / Wifi
            $table->tinyInteger('status')->default(0);

            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('passenger_id')->references('id')->on('passengers');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
