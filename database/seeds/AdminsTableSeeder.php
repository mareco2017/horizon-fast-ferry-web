<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::updateOrCreate([
            'email' => 'admin@admin.com',
            'phone_number' => '+6281912312312',
        ], [
            'email' => 'admin@admin.com',
            'phone_number' => '+6281912312312',
            'first_name' => 'admin',
            'last_name' => '',
            'password' => bcrypt('admin'),
            'gender' => 0,
            'dob' => '1999-01-01',
            'timezone' => 'GMT+7'
        ]);
    }
}
