<?php

use Illuminate\Database\Seeder;
use App\Models\Config as AppConfig;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AppConfig::updateOrCreate(
            ['key' => 'additional_tax'],
            ['value' => '5']
        );

        AppConfig::updateOrCreate(
            ['key' => 'facebook_url'],
            ['value' => 'https://facebook.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'twitter_url'],
            ['value' => 'https://twitter.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'instagram_url'],
            ['value' => 'https://instagram.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'point_rewards'],
            ['value' => '5']
        );

        AppConfig::updateOrCreate(
            ['key' => 'contact_us'],
            ['value' => '+628123456789']
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_infant_one_way'],
            ['value' => 6]
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_infant_two_ways'],
            ['value' => 20]
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_adult_local_one_way'],
            ['value' => 24]
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_adult_foreign_one_way'],
            ['value' => 24]
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_adult_local_two_ways'],
            ['value' => 34]
        );

        AppConfig::updateOrCreate(
            ['key' => 'btm_sg_adult_foreign_two_ways'],
            ['value' => 40]
        );

        AppConfig::updateOrCreate(
            ['key' => 'sg_btm_adult_one_way'],
            ['value' => 24]
        );

        AppConfig::updateOrCreate(
            ['key' => 'sg_btm_adult_two_ways'],
            ['value' => 40]
        );

        AppConfig::updateOrCreate(
            ['key' => 'sg_btm_infant_one_way'],
            ['value' => 6]
        );

        AppConfig::updateOrCreate(
            ['key' => 'sg_btm_infant_two_ways'],
            ['value' => 20]
        );

        AppConfig::updateOrCreate(
            ['key' => 'terminal_fee'],
            ['value' => 7]
        );

        AppConfig::updateOrCreate(
            ['key' => 'surcharge'],
            ['value' => 1]
        );

        AppConfig::updateOrCreate(
            ['key' => 'upgrade_business_class'],
            ['value' => 10]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_price'],
            ['value' => 7]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_refundable_deposit'],
            ['value' => 50]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_price_batam'],
            ['value' => 7]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_price_singapore'],
            ['value' => 9]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_penalty_price_batam'],
            ['value' => 5.5]
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_penalty_price_singapore'],
            ['value' => 8.5]
        );

        AppConfig::updateOrCreate(
            ['key' => 'car_5_seater'],
            ['value' => 60]
        );

        AppConfig::updateOrCreate(
            ['key' => 'car_10_seater'],
            ['value' => 105]
        );

        AppConfig::updateOrCreate(
            ['key' => 'hff_contact_us'],
            ['value' => 'hffbatam@gmail.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_rental_email'],
            ['value' => 'wifi@horizonfastferry.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'car_rental_email'],
            ['value' => 'carcharter@horizonfastferry.com']
        );

        AppConfig::updateOrCreate(
            ['key' => 'wifi_enabled'],
            ['value' => 'true']
        );
    }
}
