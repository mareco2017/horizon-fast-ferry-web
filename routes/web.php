<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Admin
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    // Guest
	Route::group(['middleware' => 'guest:web-admin', 'guard' => 'web-admin'], function() {
		Route::get('login', 'AuthController@loginPage')->name('login.page');
		Route::post('login', 'AuthController@login')->name('login');
	});

	// Logged In
	Route::group(['middleware' => 'auth:web-admin', 'guard' => 'web-admin'], function() {
	    Route::post('logout', 'AuthController@logout')->name('logout');

    	Route::get('', 'DashboardController@index')->name('dashboard');
		
		// User
		Route::group(['namespace' => 'User', 'prefix' => 'users', 'as' => 'users.'], function() {
			Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
				// List
				Route::get('/', 'UserController@index')->name('index');
				Route::post('/', 'UserController@ajax')->name('ajax');
				
				Route::get('/new', 'UserController@edit')->name('create');
				Route::get('/{user}/edit', 'UserController@edit')->name('edit');
				Route::post('/store', 'UserController@update')->name('store');
				Route::post('/{user}/update', 'UserController@update')->name('update');
				Route::delete('/{user}/delete', 'UserController@delete')->name('delete');
				Route::get('/{user}/detail', 'UserController@detail')->name('detail');
			});
		});
		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
			Route::get('/', 'OrderController@index')->name('index');
			Route::post('/', 'OrderController@ajax')->name('ajax');
			Route::post('/export-excel', 'OrderController@exportExcel')->name('export-excel');
			Route::get('/{order}/detail', 'OrderController@detail')->name('detail');
			Route::get('/{order}/edit', 'OrderController@edit')->name('edit');
			Route::post('/{order}/update', 'OrderController@update')->name('update');
			Route::post('/{order}/cancel', 'OrderController@cancel')->name('cancel');
		});
		// Articles
		Route::group(['prefix' => 'article', 'as' => 'article.'], function() {
			Route::get('/', 'ArticleController@index')->name('index');
			Route::post('/', 'ArticleController@ajax')->name('ajax');
			Route::get('/new', 'ArticleController@edit')->name('create');
			Route::get('/{article}/edit', 'ArticleController@edit')->name('edit');
			Route::post('/store', 'ArticleController@update')->name('store');
			Route::post('/{article}/update', 'ArticleController@update')->name('update');
			Route::delete('/{article}/delete', 'ArticleController@delete')->name('delete');
		});
		// Comments
		Route::group(['prefix' => 'comment', 'as' => 'comment.'], function() {
			Route::get('/', 'CommentController@index')->name('index');
			Route::post('/', 'CommentController@ajax')->name('ajax');
			Route::get('/new', 'CommentController@edit')->name('create');
			Route::get('/{comment}/edit', 'CommentController@edit')->name('edit');
			Route::post('/store', 'CommentController@update')->name('store');
			Route::post('/{comment}/update', 'CommentController@update')->name('update');
			Route::delete('/{comment}/delete', 'CommentController@delete')->name('delete');
		});
		// Latest Promotion
		Route::group(['prefix' => 'latest-promotion', 'as' => 'latest-promotion.'], function() {
			Route::get('/', 'PromoController@index')->name('index');
			Route::post('/', 'PromoController@ajax')->name('ajax');
			Route::get('/new', 'PromoController@edit')->name('create');
			Route::get('/{promo}/edit', 'PromoController@edit')->name('edit');
			Route::post('/store', 'PromoController@update')->name('store');
			Route::post('/{promo}/update', 'PromoController@update')->name('update');
			Route::delete('/{promo}/delete', 'PromoController@delete')->name('delete');
		});
		// Config
		Route::group(['prefix' => 'config', 'as' => 'config.'], function() {
			Route::get('/', 'ConfigController@index')->name('index');
			Route::post('/', 'ConfigController@ajax')->name('ajax');
			Route::get('/new', 'ConfigController@edit')->name('create');
			Route::get('/{config}/edit', 'ConfigController@edit')->name('edit');
			Route::post('/store', 'ConfigController@update')->name('store');
			Route::post('/{config}/update', 'ConfigController@update')->name('update');
			Route::delete('/{config}/delete', 'ConfigController@delete')->name('delete');
		});
		// Passenger
		Route::group(['prefix' => 'passenger', 'as' => 'passenger.'], function() {
			Route::get('/', 'PassengerController@index')->name('index');
			Route::post('/', 'PassengerController@ajax')->name('ajax');
			Route::get('/new', 'PassengerController@edit')->name('create');
			Route::get('/{passenger}/edit', 'PassengerController@edit')->name('edit');
			Route::post('/store', 'PassengerController@update')->name('store');
			Route::post('/{passenger}/update', 'PassengerController@update')->name('update');
			Route::delete('/{passenger}/delete', 'PassengerController@delete')->name('delete');
		});
		// Preference
		Route::group(['namespace' => 'Preference', 'prefix' => 'preferences', 'as' => 'preferences.'], function() {
			// Faq
			Route::group(['prefix' => 'faq', 'as' => 'faq.'], function(){
				Route::get('/', 'FaqController@index')->name('index');
				Route::post('/list', 'FaqController@ajax')->name('ajax');
				Route::get('/new', 'FaqController@edit')->name('create');
				Route::post('/store', 'FaqController@update')->name('store');
				Route::get('/customize-urut', 'FaqController@customizeUrut')->name('customize-urut');
				Route::post('/update-urut', 'FaqController@updateUrut')->name('update-urut');
				Route::get('/{faq}/edit', 'FaqController@edit')->name('edit');
				Route::post('/{faq}/save', 'FaqController@update')->name('update');
				Route::delete('/{faq}/delete', 'FaqController@delete')->name('delete');
			});
		});
	});
});

Route::view('/batam-ferry-timing', 'batam-ferry-timing.index');
Route::get('/{any}', 'SpaController@index')->where('any', '.*');
