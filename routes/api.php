<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1', 'as' => 'api.'], function() {
	Route::post('register', 'AuthController@register')->name('register');
	Route::post('quick-register', 'AuthController@quickRegister')->name('quick-register');
	Route::post('refresh', 'AuthController@refresh')->name('refresh');
	Route::post('check-exists', 'AuthController@checkExists')->name('check-exists');
	Route::get('get-latest-promotions', 'LatestPromotionController@get')->name('get-latest-promotions');
	Route::get('get-batam-highlights', 'ArticleController@getBatamHighlight')->name('get-batam-highlights');
	Route::get('get-articles', 'ArticleController@get')->name('get-articles');
	Route::get('get-article/{article}', 'ArticleController@getArticleDetail')->name('get-article-detail');
	Route::get('get-faqs', 'FaqController@get')->name('get-faqs');
	Route::post('send-enquiry', 'ContactUsController@send')->name('send-enquiry');
	Route::post('article/{article}/post-comment', 'ArticleController@postComment')->name('post-comment');

	// Passenger
	Route::group(['prefix' => 'passenger', 'as' => 'passenger.'], function() {
    	Route::get('search', 'PassengerController@search')->name('search');
	});

	// Contact Person
	Route::group(['prefix' => 'contact-person', 'as' => 'contact-person.'], function() {
    	Route::get('search', 'ContactPersonController@search')->name('search');
	});

	// Schedule
	Route::group(['prefix' => 'schedule', 'as' => 'schedule.'], function() {
		Route::get('daily', 'ScheduleController@daily')->name('daily');
	});

	// Ticket
	Route::group(['prefix' => 'ticket', 'as' => 'ticket.'], function() {
		Route::get('price', 'TicketController@price')->name('price');
		Route::get('wifi-price', 'TicketController@wifiPrice')->name('wifi-price');
	});

	// Order
	Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
		Route::get('search', 'OrderController@search')->name('search');
		Route::post('reserve', 'OrderController@reserve')->name('reserve');
		Route::post('reserve-wifi', 'OrderController@reserveWifi')->name('reserve-wifi');
		Route::post('reserve-car', 'OrderController@reserveCar')->name('reserve-car');
		Route::post('cancel', 'OrderController@cancel')->name('cancel');
		Route::post('midtrans-handler', 'OrderController@midtransHandler')->name('midtrans-handler');
		Route::post('stripe-charge', 'OrderController@stripeCharge')->name('stripe-charge');
		Route::post('stripe-payment-intent', 'OrderController@stripePaymentIntent')->name('stripe-payment-intent');
	});

	/*
	|--------------------------------------------------------------------------
	| User
	|--------------------------------------------------------------------------
	*/
	
	// Guest
	Route::group(['middleware' => 'guest:api-user', 'guard' => 'api-user'], function() {
		Route::post('login', 'AuthController@login')->name('login');
		Route::post('social-login/{type}', 'AuthController@socialLogin')->name('social-login');
		Route::post('password/email', 'ForgotPasswordController@getResetToken')->name('reset-email');
		Route::post('password/reset', 'ResetPasswordController@reset')->name('reset');
	});

	// Logged In
	Route::group(['middleware' => 'auth:api-user', 'guard' => 'api-user'], function() {
	    Route::post('logout', 'AuthController@logout')->name('logout');

		// User
		Route::group(['prefix' => 'user', 'as' => 'user.'], function() {
	    	Route::get('me', 'UserController@me')->name('me');
	    	Route::post('update-profile', 'UserController@updateProfile')->name('update-profile');
	    	Route::post('change-password', 'UserController@changePassword')->name('change-password');
		});

		// Passenger
		Route::group(['prefix' => 'passenger', 'as' => 'passenger.'], function() {
	    	Route::get('owned', 'PassengerController@getOwnedPassengers')->name('owned-list');
	    	Route::post('store', 'PassengerController@store')->name('store');
		});

		// Contact Person
		Route::group(['prefix' => 'contact-person', 'as' => 'contact-person.'], function() {
	    	Route::get('owned', 'ContactPersonController@getOwnedContactPersons')->name('owned-list');
	    	Route::post('store', 'ContactPersonController@store')->name('store');
		});

		// Order
		Route::group(['prefix' => 'order', 'as' => 'order.'], function() {
	    	Route::get('owned', 'OrderController@owned')->name('owned');
		});

	});
});
