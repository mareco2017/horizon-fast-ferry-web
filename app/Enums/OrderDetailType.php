<?php

namespace App\Enums;

final class OrderDetailType {

	const TICKET_ADULT = 0;
	const TICKET_CHILD = 1;
	const TICKET_INFANT = 2;
	const CAR_RENTAL = 3;
	const WIFI_RENTAL = 4;

	public static function getList() {
		return [
			OrderDetailType::TICKET_ADULT,
			OrderDetailType::TICKET_CHILD,
			OrderDetailType::TICKET_INFANT,
			OrderDetailType::CAR_RENTAL,
			OrderDetailType::WIFI_RENTAL
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Ticket Adult";
			case 1:
				return "Ticket Child";
			case 2:
				return "Ticket Infant";
			case 3:
				return "Car Rental";
			case 4:
				return "Wifi Rental";
		}
	}

	public static function getType($val) {
		switch ($val) {
			case 0:
				return "Adult";
			case 1:
				return "Child";
			case 2:
				return "Infant";
			case 3:
				return "Car";
			case 4:
				return "Wifi";
		}
	}

}

?>
