<?php

namespace App\Enums;

final class OrderStatus {

	const PENDING = 0;
	const WAITING_FOR_PAYMENT = 1;
	const COMPLETED = 2;
	const CANCELLED = 3;
	const EXPIRED = 4;
	const REFUNDED = 5;

	public static function getList() {
		return [
			OrderStatus::PENDING,
			OrderStatus::WAITING_FOR_PAYMENT,
			OrderStatus::COMPLETED,
			OrderStatus::CANCELLED,
			OrderStatus::EXPIRED,
			OrderStatus::REFUNDED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Waiting for Payment";
			case 2:
				return "Completed";
			case 3:
				return "Cancelled";
			case 4:
				return "Expired";
			case 5:
				return "Refunded";
		}
	}

}

?>
