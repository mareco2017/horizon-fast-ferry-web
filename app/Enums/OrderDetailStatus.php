<?php

namespace App\Enums;

final class OrderDetailStatus {

	const PENDING = 0;
	const RESERVED = 1;
	const CHECKED_IN = 2;
	const RELEASED = 3;
	const EXPIRED = 4;
	const ERROR = 5;

	public static function getList() {
		return [
			OrderDetailStatus::PENDING,
			OrderDetailStatus::RESERVED,
			OrderDetailStatus::CHECKED_IN,
			OrderDetailStatus::RELEASED,
			OrderDetailStatus::EXPIRED,
			OrderDetailStatus::ERROR
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Reserved";
			case 2:
				return "Checked In";
			case 3:
				return "Released";
			case 4:
				return "Expired";
			case 5:
				return "Error";
		}
	}

}

?>
