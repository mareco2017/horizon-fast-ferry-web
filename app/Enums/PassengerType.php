<?php

namespace App\Enums;

final class PassengerType {

	const ADULT = 0;
	const INFANT = 1;

	public static function getList() {
		return [
			PassengerType::ADULT,
			PassengerType::INFANT,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Adult";
			case 1:
				return "Infant";
		}
	}
}
?>
