<?php

namespace App\Enums;

final class TripType {

	const DEPARTURE = "DEPARTURE";
	const RETURN = "RETURN";

	public static function getList() {
		return [
			TicketType::DEPARTURE,
			TicketType::RETURN
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case "DEPARTURE":
				return "Departure";
			case "RETURN":
				return "Return";
		}
	}
}

?>
