<?php

namespace App\Enums;

final class TicketType {

	const ECONOMY = 0;
	const BUSINESS = 1;
	const VIP = 2;

	public static function getList() {
		return [
			TicketType::ECONOMY,
			TicketType::BUSINESS,
			TicketType::VIP
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Economy";
			case 1:
				return "Business";
			case 2:
				return "VIP";
		}
	}

	// Used for API to horizon
	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "Economy";
			case 1:
				return "Business";
			case 2:
				return "VIP";
		}
	}

}

?>
