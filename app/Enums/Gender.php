<?php

namespace App\Enums;

final class Gender {

	const MAlE = 0;
	const FEMALE = 1;
	const OTHER = 2;

	public static function getList() {
		return [
			Gender::MAlE,
			Gender::FEMALE,
			Gender::OTHER
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Male";
			case 1:
				return "Female";
			case 2:
				return "Other";
		}
	}

	// Used for API to horizon
	public static function getCode($val) {
		switch ($val) {
			case 0:
				return "M";
			case 1:
				return "F";
			case 2:
				return "O";
		}
	}

}

?>
