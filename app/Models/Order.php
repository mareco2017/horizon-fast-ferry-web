<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use App\Enums\OrderStatus;
use App\Enums\OrderDetailType;
use App\Enums\OrderDetailStatus;

class Order extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reference_number', 'description', 'contact_person_id', 'expiry_date', 'options', 'payment', 'wifi_payment', 'wifi_refundable_payment', 'ownable_type', 'ownable_id' 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object',
        'payment' => 'object',
        'wifi_payment' => 'object',
        'wifi_refundable_payment' => 'object'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'status_string', 'total'
    ];

    protected $dates = [
        'expiry_date', 'deleted_at'
    ];

    public function ownable()
    {
    	return $this->morphTo();
    }

    public function details()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }

    public function contactPerson()
    {
        return $this->belongsTo('App\Models\ContactPerson');
    }

    public function scopeActive($query)
    {
        return $query->whereIn('status', [OrderStatus::PENDING, OrderStatus::WAITING_FOR_PAYMENT]);
    }

    public function isActive()
    {
        return in_array($this->status, [OrderStatus::PENDING, OrderStatus::WAITING_FOR_PAYMENT]);
    }

    public function isCompleted()
    {
        return $this->status == OrderStatus::COMPLETED;
    }

    public function getTotalTicketAttribute($value)
    {
        $result = DB::table('order_details')->whereIn('type', [OrderDetailType::TICKET_ADULT, OrderDetailType::TICKET_CHILD])->where('order_id', $this->id)->selectRaw('SUM((price - discount + terminal_fee + surcharge + upgrade_business) * quantity) as total, order_id')->groupBy('order_id')->first();
        if (!$result) return 0;
        return $result->total;
    }

    public function getTotalWifiAttribute($value)
    {
        $result = DB::table('order_details')->where('type', OrderDetailType::WIFI_RENTAL)->where('order_id', $this->id)->selectRaw('SUM((price - discount + terminal_fee + surcharge + upgrade_business) * quantity) as total, order_id')->groupBy('order_id')->first();
        if (!$result) return 0;
        return $result->total;
    }

    public function getTotalCarAttribute($value)
    {
        $result = DB::table('order_details')->where('type', OrderDetailType::CAR_RENTAL)->where('order_id', $this->id)->selectRaw('SUM((price - discount + terminal_fee + surcharge + upgrade_business) * quantity) as total, order_id')->groupBy('order_id')->first();
        if (!$result) return 0;
        return $result->total;
    }

    public function getTotalAttribute($value)
    {
        $result = DB::table('order_details')->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->where('order_id', $this->id)->selectRaw('SUM((price - discount + terminal_fee + surcharge + upgrade_business) * quantity) as total, order_id')->groupBy('order_id')->first();
        if (!$result) return 0;
        return $result->total;
    }

    public function getTotalDiscountAttribute($value)
    {
        $result = $this->orderDetails->sum('discount');
        
        return $result;
    }

    public function getTotalBeforeDiscountAttribute($value)
    {
        $result = $this->orderDetails->sum('total');
        
        return $result;
    }

    public function getTotalTicketPriceAttribute($value)
    {
        $result = $this->details()->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->ticket()->selectRaw('SUM((price + upgrade_business) * quantity) as totalPrice, order_id')->groupBy('order_id')->first();
        
        return $result ? $result->totalPrice : 0;
    }

    public function getTotalAdultTicketPriceAttribute($value)
    {
        $result = $this->details()->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->typeTicket(OrderDetailType::TICKET_ADULT)->selectRaw('SUM((price + upgrade_business) * quantity) as totalPrice, order_id')->groupBy('order_id')->first();

        return $result ? $result->totalPrice : 0;
    }

    public function getTotalInfantTicketPriceAttribute($value)
    {
        $result = $this->details()->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->typeTicket(OrderDetailType::TICKET_CHILD)->selectRaw('SUM((price + upgrade_business) * quantity) as totalPrice, order_id')->groupBy('order_id')->first();

        return $result ? $result->totalPrice : 0;
    }

    public function getTotalSurchargeAttribute($value)
    {
        $result = $this->details()->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->sum('surcharge');
        return $result;
    }

    public function getTotalTerminalFeeAttribute($value)
    {
        $result = $this->details()->whereNotIn('status', [OrderDetailStatus::RELEASED, OrderDetailStatus::EXPIRED, OrderDetailStatus::ERROR])->sum('terminal_fee');
        
        return $result;
    }

    public function getStatusStringAttribute($value)
    {
        return OrderStatus::getString($this->status);
    }
}
