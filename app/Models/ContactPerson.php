<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactPerson extends Model
{
    use SoftDeletes;
    
    protected $table = 'contact_persons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number', 'email', 'first_name', 'last_name', 'ownable_type', 'ownable_id' 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'fullname'
    ];

    protected $dates = [
        'deleted_at',
    ];

    public function ownable()
    {
    	return $this->morphTo();
    }

    public function getFullnameAttribute($value)
    {
        return $this->last_name ? $this->first_name . ' ' . $this->last_name : $this->first_name;
    }
}
