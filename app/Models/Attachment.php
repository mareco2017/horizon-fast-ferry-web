<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Attachment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'attachable_type', 'attachable_id', 'key', 'options'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url'];
    protected $dates = ['deleted_at'];
    
    public function attachable()
    {
        return $this->morphTo();
    }

    public function scopeType($query, $type = null)
    {
        return $query->where('options->type', $type);
    }

    public function getUrlAttribute($value)
    {
        return Storage::disk($this->options->disk)->url($this->key);
    }
}
