<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Enums\ActiveStatus;

class Faq extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'answer', 'urut', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $hidden = [];

    public function scopeActive($query) {
        return $query->where('status', ActiveStatus::ACTIVE)->where('urut', '!=', null);
    }

    public function scopeInactive($query) {
        return $query->where('status', ActiveStatus::ACTIVE)->where('urut', null);
    }

    public function scopeSort($query) {
        return $query->orderBy('urut');
    }
}
