<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Verification;
use App\Enums\VerificationStatus;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone_number', 'email', 'first_name', 'last_name', 'password', 'gender', 'dob', 'device_type', 'profile_picture_id', 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'google' => 'object',
        'facebook' => 'object',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'fullname', 'phone_number_verified', 'email_verified'
    ];

    protected $dates = [
        'deleted_at',
    ];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'ownable');
    }

    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'ownable');
    }

    public function verifications()
    {
        return $this->morphMany('App\Models\Verification', 'ownable');
    }

    public function passengers()
    {
        return $this->morphMany('App\Models\Passenger', 'ownable');
    }

    public function availablePassengers()
    {
        return $this->passengers()->where('passport_number', '!=', '');
    }

    public function contactPersons()
    {
        return $this->morphMany('App\Models\ContactPerson', 'ownable');
    }

    public function getFullnameAttribute($value)
    {
        return $this->last_name ? $this->first_name . ' ' . $this->last_name : $this->first_name;
    }

    public function getPhoneNumberVerifiedAttribute($value)
    {
        if (!$this->phone_number) return false;
        $verification = Verification::where('key', $this->phone_number)->latest()->first();
        return $verification ? $verification->status == VerificationStatus::VERIFIED : false;
    }

    public function getEmailVerifiedAttribute($value)
    {
        if (!$this->email) return false;
        $verification = Verification::where('key', $this->email)->latest()->first();
        return $verification ? $verification->status == VerificationStatus::VERIFIED : false;
    }
}
