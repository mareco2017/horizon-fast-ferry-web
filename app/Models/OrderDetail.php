<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\OrderDetailType;
use App\Enums\OrderDetailStatus;
use App\Enums\TripType;
use DB;

class OrderDetail extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'price', 'discount', 'terminal_fee', 'surcharge', 'upgrade_business', 'quantity', 'start_date', 'end_date', 'expiry_date', 'claim_date', 'options', 'order_id', 'passenger_id', 'type', 'status'  
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'object'
    ];

    protected $appends = [
        'status_string', 'total'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $dates = [
        'start_date', 'end_date', 'claim_date', 'expiry_date', 'deleted_at'
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function passenger()
    {
    	return $this->belongsTo('App\Models\Passenger');
    }

    public function isTicket()
    {
        return in_array($this->type, [OrderDetailType::TICKET_ADULT, OrderDetailType::TICKET_CHILD, OrderDetailType::TICKET_INFANT]);
    }

    public function scopeTicket($query)
    {
        return $query->whereIn('type', [OrderDetailType::TICKET_ADULT, OrderDetailType::TICKET_CHILD, OrderDetailType::TICKET_INFANT]);
    }

    public function scopeWifiRental($query)
    {
        return $query->whereIn('type', [OrderDetailType::WIFI_RENTAL]);
    }

    public function scopeCarRental($query)
    {
        return $query->whereIn('type', [OrderDetailType::CAR_RENTAL]);
    }

    public function scopeTypeTicket($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeReserved($query)
    {
        return $query->where('status', OrderDetailStatus::RESERVED);
    }

    public function scopeDeparture($query)
    {
        // return $query->where(DB::raw('JSON_CONTAINS(`options`, "$.trip_type")'), '=', TripType::DEPARTURE);
        return $query->where('options->trip_type', TripType::DEPARTURE);
    }

    public function scopeReturn($query)
    {
        // return $query->where(DB::raw('JSON_CONTAINS(`options`, "$.trip_type")'), '=', TripType::RETURN);
        return $query->where('options->trip_type', TripType::RETURN);
    }

    public function scopeCheckedIn($query)
    {
        return $query->where('status', OrderDetailStatus::CHECKED_IN);
    }

    public function getTotalAttribute($value)
    {
        return ($this->price - $this->discount + $this->terminal_fee + $this->surcharge + $this->upgrade_business) * $this->quantity;
    }

    public function getStatusStringAttribute($value)
    {
        return OrderDetailStatus::getString($this->status);
    }
}
