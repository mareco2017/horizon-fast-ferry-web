<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\Gender;

class Passenger extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'passport_number', 'name', 'nationality', 'gender', 'dob', 'passport_expiry', 'issuing_country', 'passport_issue', 'type', 'ownable_type', 'ownable_id' 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = [
        'gender_string',
        'dob_day',
        'dob_month',
        'dob_year',
        'passport_expiry_day',
        'passport_expiry_month',
        'passport_expiry_year',
        'passport_issue_day',
        'passport_issue_month',
        'passport_issue_year',
    ];

    protected $dates = [
        'dob', 'passport_expiry', 'passport_issue', 'deleted_at',
    ];

    public function ownable()
    {
    	return $this->morphTo();
    }

    public function getGenderStringAttribute()
    {
        return Gender::getString($this->gender);
    }

    public function getDobDayAttribute()
    {
        return $this->dob ? $this->dob->format('d') : '';
    }

    public function getDobMonthAttribute()
    {
        return $this->dob ? $this->dob->format('m') : '';
    }

    public function getDobYearAttribute()
    {
        return $this->dob ? $this->dob->format('Y') : '';
    }

    public function getPassportExpiryDayAttribute()
    {
        return $this->passport_expiry ? $this->passport_expiry->format('d') : '';
    }

    public function getPassportExpiryMonthAttribute()
    {
        return $this->passport_expiry ? $this->passport_expiry->format('m') : '';
    }

    public function getPassportExpiryYearAttribute()
    {
        return $this->passport_expiry ? $this->passport_expiry->format('Y') : '';
    }

    public function getPassportIssueDayAttribute()
    {
        return $this->passport_issue ? $this->passport_issue->format('d') : '';
    }

    public function getPassportIssueMonthAttribute()
    {
        return $this->passport_issue ? $this->passport_issue->format('m') : '';
    }

    public function getPassportIssueYearAttribute()
    {
        return $this->passport_issue ? $this->passport_issue->format('Y') : '';
    }
}
