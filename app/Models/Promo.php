<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Enums\ActiveStatus;
use Carbon\Carbon;

class Promo extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date', 'end_date', 'status'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = ['cover_url'];
    protected $hidden = [];
    protected $dates = ['start_date', 'end_date', 'deleted_at'];

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE)->where('end_date', '>=', Carbon::now()->subDay());
    }

    public function getCoverUrlAttribute($value)
    {
        return $this->cover ? $this->cover->url : '';
    }
}
