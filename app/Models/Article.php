<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\Enums\ActiveStatus;
use App\Helpers\Enums\VerificationStatus;

class Article extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'cover_id', 'publisher_id', 'tags', 'desc', 'intro', 'slug_url'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    protected $appends = ['cover_url', 'publisher_name'];

    protected $dates = [
        'deleted_at',
    ];

    public function publisher()
    {
        return $this->belongsTo('App\Models\Admin');
    }

    public function cover()
    {
        return $this->belongsTo('App\Models\Attachment');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment')->where('status', VerificationStatus::VERIFIED);
    }

    public function getCoverUrlAttribute($value)
    {
        return $this->cover ? $this->cover->url : '';
    }

    public function getPublisherNameAttribute($value)
    {
        return $this->publisher ? $this->publisher->fullname : 'No name';
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeActive($query)
    {
        return $query->where('status', ActiveStatus::ACTIVE);
    }
}
