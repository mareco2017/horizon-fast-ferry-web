<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use App\Enums\TicketType;
use App\Enums\PassengerType;
use App\Enums\OrderStatus;

class WifiOrderExport implements FromQuery, WithMapping, WithHeadings, WithEvents, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    protected $startDate;
    protected $endDate;
    protected $orderStatus;
    protected $orders;

    public function __construct($startDate, $endDate, $status)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->orderStatus = $status;
        $this->orders = Order::query();
    }

    public function query()
    {
        $this->orders->where('options->wifi_only', true);

        if ($this->startDate || $this->endDate) {
            $date1 = $this->startDate ? Carbon::parse($this->startDate)->timezone('Asia/Jakarta') : null;
            $date2 = $this->endDate ? Carbon::parse($this->endDate)->timezone('Asia/Jakarta')->endOfDay() : null;
            
            if ($date1 && $date2) {
                $this->orders->whereBetween('created_at', [$date1->setTimezone('UTC'), $date2->setTimezone('UTC')]);
            } else if ($date1 && !$date2) {
                $this->orders->whereDate('created_at', '>=', $date1->setTimezone('UTC'));
            } else if (!$date1 && $date2) {
                $this->orders->where('created_at', '<=', $date2->setTimezone('UTC'));
            }
        }
        if ($this->orderStatus) {
            $this->orders->where('status', $this->orderStatus);
        }

        return $this->orders;
    }
    
    public function headings(): array
    {
        return [
            [
                'Transaction ID',
                'Website Booking Reference',
                'Contact Person',
                '',
                'Transaction Status',
                'Created Date',
                'Payment Details',
                '',
                '',
                'Purchase Details',
                '',
                '',
                '',
                'Passenger Details',
                '',
                '',
                '',
            ],
            [
                '',
                '',
                'Email Address',
                'Phone Number',
                '',
                '',
                'Stripe Payment ID',
                'Amount',
                'Fee',
                'Device Qty',
                'Rent Start Date',
                'Rent End Date',
                'Usage In',
                'Passport No',
                'Passenger Full Name',
                'Nationality',
                'Age Category',
            ]
        ];
    }

    public function map($order): array
    {
        $wifiDetail = $order->details()->wifiRental()->first();

        $row = [];

        $row[] = $order->id;
        $row[] = $order->reference_number;
        $row[] = $order->contactPerson->email;
        $row[] = $order->contactPerson->phone_number;
        $row[] = OrderStatus::getString($order->status);
        $row[] = $order->created_at->timezone('Asia/Jakarta')->format('l; F j; Y g:i:s A');
        $row[] = $order->payment ? $order->payment->id : '-';
        $row[] = $order->total ? $order->total : '';
        $row[] = $order->total ? ($order->total * 3.4/100) + 0.5 : '';
        $row[] = $order->options->wifi_devices;
        $row[] = $order->options->wifi_start_date;
        $row[] = $order->options->wifi_end_date;
        $row[] = $order->options->departure_from == 'singapore' ? 'Batam' : 'Singapore';

        if ($wifiDetail->passenger) {
            $row[] = $wifiDetail->passenger->passport_number ? $wifiDetail->passenger->passport_number : '-';
            $row[] = $wifiDetail->passenger->name;
            $row[] = $wifiDetail->passenger->nationality;
            $row[] = PassengerType::getString($wifiDetail->passenger->type);
        } else {
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
        }


        return $row;
    }
    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                /**
                 * Merging
                 */

                // Vertical Merge
                $event->sheet->getDelegate()->mergeCells('A1:A2');
                $event->sheet->getDelegate()->mergeCells('B1:B2');
                $event->sheet->getDelegate()->mergeCells('E1:E2');
                $event->sheet->getDelegate()->mergeCells('F1:F2');

                // Horizontal Merge
                $event->sheet->getDelegate()->mergeCells('C1:D1');
                $event->sheet->getDelegate()->mergeCells('G1:I1');
                $event->sheet->getDelegate()->mergeCells('J1:M1');
                $event->sheet->getDelegate()->mergeCells('N1:Q1');

                /** 
                 * Style
                 */
                $firstTable = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $bordered = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                ];
                
                // First Table
                $hr = $event->sheet->getHighestRow();
                // $ftRowHeight = $hr - 27;
                $fr = 3;
                $ftRowHeight = $hr;
                $ftColumnHeight = 'Q';
                $fcellRange = 'A1:'.$ftColumnHeight.$ftRowHeight;

                $event->sheet->getStyle($fcellRange)->applyFromArray($firstTable);

                $headingStyles = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                ];
                $event->sheet->getStyle('A1:'.$ftColumnHeight.'2')->applyFromArray($headingStyles);
            },
        ];
    }
}
