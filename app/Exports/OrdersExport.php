<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use App\Enums\TicketType;
use App\Enums\PassengerType;
use App\Enums\OrderStatus;

class OrdersExport implements FromQuery, WithMapping, WithHeadings, WithEvents, WithColumnFormatting, ShouldAutoSize
{
    use Exportable;

    protected $startDate;
    protected $endDate;
    protected $orderStatus;
    protected $orders;

    public function __construct($startDate, $endDate, $status)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->orderStatus = $status;
        $this->orders = Order::query();
    }

    public function query()
    {
        if ($this->startDate || $this->endDate) {
            $date1 = $this->startDate ? Carbon::parse($this->startDate)->timezone('Asia/Jakarta') : null;
            $date2 = $this->endDate ? Carbon::parse($this->endDate)->timezone('Asia/Jakarta')->endOfDay() : null;
            
            if ($date1 && $date2) {
                $this->orders->whereBetween('created_at', [$date1->setTimezone('UTC'), $date2->setTimezone('UTC')]);
            } else if ($date1 && !$date2) {
                $this->orders->whereDate('created_at', '>=', $date1->setTimezone('UTC'));
            } else if (!$date1 && $date2) {
                $this->orders->where('created_at', '<=', $date2->setTimezone('UTC'));
            }
        }
        if ($this->orderStatus) {
            $this->orders->where('status', $this->orderStatus);
        }

        return $this->orders;
    }
    
    public function headings(): array
    {
        return [
            [
                'Transaction ID',
                'Website Booking Reference',
                'Contact Person',
                '',
                'Transaction Status',
                'Created Date',
                'Payment Details',
                '',
                '',
                'Purchase Details',
                '',
                '',
                'Passenger Details',
                '',
                '',
                '',
                'Departure Details',
                '',
                '',
                '',
                'Return Details',
                '',
                '',
                '',
                'Wifi Details',
                '',
                '',
                '',
            ],
            [
                '',
                '',
                'Email Address',
                'Phone Number',
                '',
                '',
                'Stripe Payment ID',
                'Amount',
                'Fee',
                'Qty',
                'Trip Type',
                'Seat Type',
                'Passport No',
                'Passenger Full Name',
                'Nationality',
                'Age Category',
                'Departure Point',
                'Transaction Reference',
                'Departure Schedule',
                'Departure BP No.',
                'Return Point',
                'Transaction Reference',
                'Return Schedule',
                'Return BP No.',
                'Device Qty',
                'Rent Start Date',
                'Rent End Date',
                'Usage In',
            ]
        ];
    }

    public function map($order): array
    {
        $departDetails = $order->details()->departure()->get();
        $returnDetails = $order->details()->return()->get();
        $wifiDetail = $order->details()->wifiRental()->first();
        $length = $departDetails->count();

        $row = [];
        
        if (count($departDetails)) {
            foreach($departDetails as $key => $detail) {
                $data = [];
                if ($key == 0) {
                    $data[] = $order->id;
                    $data[] = $order->reference_number;
                    $data[] = $order->contactPerson->email;
                    $data[] = $order->contactPerson->phone_number;
                    $data[] = OrderStatus::getString($order->status);
                    $data[] = $order->created_at->timezone('Asia/Jakarta')->format('l; F j; Y g:i:s A');
                    $data[] = $order->payment ? $order->payment->id : '-';
                    $data[] = $order->total ? $order->total : '';
                    $data[] = $order->total ? ($order->total * 3.4/100) + 0.5 : '';
                    $data[] = $length;
                    $data[] = $order->options->two_way ? 'Round Trip' : 'One Way';
                    $data[] = TicketType::getString($order->options->type);
                } else {
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                }
    
                if ($detail->passenger) {
                    $data[] = $detail->passenger->passport_number;
                    $data[] = $detail->passenger->name;
                    $data[] = $detail->passenger->nationality;
                    $data[] = PassengerType::getString($detail->passenger->type);
                } else {
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                }
    
                if ($key == 0) {
                    $data[] = ucfirst($order->options->departure_from);
                    $data[] = isset($order->options->departure_booking_reference) ? $order->options->departure_booking_reference : '-';
                    $data[] = Carbon::parse($order->options->departure_date)->format('d M Y').' @ '.$order->options->departure_time;
                } else {
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                }
    
                $data [] = isset($detail->options->boarding_pass_number) ? $detail->options->boarding_pass_number : '-';
    
                if ($key == 0) {
                    $data[] = ucfirst($order->options->return_from);
                    $data[] = isset($order->options->return_booking_reference) ? $order->options->return_booking_reference : '-';
                    $data[] = Carbon::parse($order->options->return_date)->format('d M Y').' @ '.$order->options->return_time;
                } else {
                    $data[] = '';
                    $data[] = '';
                    $data[] = '';
                }
    
                $data [] = isset($returnDetails[$key]->options->boarding_pass_number) ? $returnDetails[$key]->options->boarding_pass_number : '-';
                
                if ($wifiDetail) {
                    if ($key == 0) {
                        $data[] = $order->options->wifi_devices;
                        $data[] = $order->options->wifi_start_date;
                        $data[] = $order->options->wifi_end_date;
                        $data[] = $order->options->departure_from == 'singapore' ? 'Batam' : 'Singapore';
                    } else {
                        $data[] = '';
                        $data[] = '';
                        $data[] = '';
                        $data[] = '';
                    }
                } else {
                    $data[] = '-';
                    $data[] = '-';
                    $data[] = '-';
                    $data[] = '-';
                }
    
                $row[] = $data;
            };
        } elseif ($wifiDetail) {
            $row[] = $order->id;
            $row[] = $order->reference_number;
            $row[] = $order->contactPerson->email;
            $row[] = $order->contactPerson->phone_number;
            $row[] = OrderStatus::getString($order->status);
            $row[] = $order->created_at->timezone('Asia/Jakarta')->format('l; F j; Y g:i:s A');
            $row[] = $order->payment ? $order->payment->id : '-';
            $row[] = $order->total ? $order->total : '';
            $row[] = $order->total ? ($order->total * 3.4/100) + 0.5 : '';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';

            if ($wifiDetail->passenger) {
                $row[] = $wifiDetail->passenger->passport_number ? $wifiDetail->passenger->passport_number : '-';
                $row[] = $wifiDetail->passenger->name;
                $row[] = $wifiDetail->passenger->nationality;
                $row[] = PassengerType::getString($wifiDetail->passenger->type);
            } else {
                $row[] = '';
                $row[] = '';
                $row[] = '';
                $row[] = '';
            }
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';
            $row[] = '-';

            $row[] = $order->options->wifi_devices;
            $row[] = $order->options->wifi_start_date;
            $row[] = $order->options->wifi_end_date;
            $row[] = $order->options->departure_from == 'singapore' ? 'Batam' : 'Singapore';
        }

        return $row;
    }
    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER,
            'R' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER,
            'V' => \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                /**
                 * Merging
                 */

                // Vertical Merge
                $event->sheet->getDelegate()->mergeCells('A1:A2');
                $event->sheet->getDelegate()->mergeCells('B1:B2');
                $event->sheet->getDelegate()->mergeCells('E1:E2');
                $event->sheet->getDelegate()->mergeCells('F1:F2');

                // Horizontal Merge
                $event->sheet->getDelegate()->mergeCells('C1:D1');
                $event->sheet->getDelegate()->mergeCells('G1:I1');
                $event->sheet->getDelegate()->mergeCells('J1:L1');
                $event->sheet->getDelegate()->mergeCells('M1:P1');
                $event->sheet->getDelegate()->mergeCells('Q1:T1');
                $event->sheet->getDelegate()->mergeCells('U1:X1');
                $event->sheet->getDelegate()->mergeCells('Y1:AB1');

                /** 
                 * Style
                 */
                $firstTable = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $bordered = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                ];
                
                // First Table
                $hr = $event->sheet->getHighestRow();
                // $ftRowHeight = $hr - 27;
                $fr = 3;
                $ftRowHeight = $hr;
                $ftColumnHeight = 'AB';
                $fcellRange = 'A1:'.$ftColumnHeight.$ftRowHeight;

                $event->sheet->getStyle($fcellRange)->applyFromArray($firstTable);

                $headingStyles = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                ];
                $event->sheet->getStyle('A1:'.$ftColumnHeight.'2')->applyFromArray($headingStyles);

                // Second Table
                $stRowHeight = $ftRowHeight + 3;
                $stMaxRowHeight = $stRowHeight + 2;
                $stColumnHeight = 'C';
                $sCellRange = 'A'.$stRowHeight.':'.$stColumnHeight.$stMaxRowHeight;

                $leftHeading = [
                    'font' => [
                        'bold' => true,
                        'italic' => true
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $center = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $bottomHeading = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                ];

                $event->sheet->setCellValue('A'.$stRowHeight,'Payment Data');

                $event->sheet->setCellValue('B'.$stRowHeight,'Payment Received:');
                $event->sheet->setCellValue('B'.($stRowHeight + 1),'Payment Fee');
                $event->sheet->setCellValue('B'.($stRowHeight + 2),'Net Received:');

                $event->sheet->setCellValue('C'.$stRowHeight,'=SUM(H3:H'.$ftRowHeight.')');
                $event->sheet->setCellValue('C'.($stRowHeight + 1),'=SUM(I3:I'.$ftRowHeight.')');
                $event->sheet->setCellValue('C'.($stRowHeight + 2),'=C'.$stRowHeight.'-C'.($stRowHeight+1));

                $event->sheet->mergeCells('A'.$stRowHeight.':A'.$stMaxRowHeight);

                $event->sheet->getStyle($sCellRange)->applyFromArray($bordered);
                $event->sheet->getStyle('A'.$stRowHeight.':A'.$stMaxRowHeight)->applyFromArray($leftHeading);
                $event->sheet->getStyle('C'.$stRowHeight.':C'.$stMaxRowHeight)->applyFromArray($center);
                $event->sheet->getStyle('B'.$stMaxRowHeight.':C'.$stMaxRowHeight)->applyFromArray($bottomHeading);


                // Third Table
                $ttRowHeight = $stMaxRowHeight + 2;
                $ttMaxRowHeight = $ttRowHeight + 20;
                $ttColumnHeight = 'E';
                $tCellRange = 'A'.$ttRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight;

                $event->sheet->setCellValue('A'.$ttRowHeight, 'Selling details(Adult)');
                $event->sheet->setCellValue('A'.($ttRowHeight+10), 'Selling details(Infant)');
                $event->sheet->setCellValue('A'.($ttRowHeight+20), 'Total Pax');

                $event->sheet->mergeCells('A'.$ttRowHeight.':A'.($ttRowHeight+9));
                $event->sheet->mergeCells('A'.($ttRowHeight+10).':A'.($ttRowHeight+19));
                $event->sheet->mergeCells('A'.$ttMaxRowHeight.':D'.$ttMaxRowHeight);

                $event->sheet->setCellValue('B'.$ttRowHeight, 'Departure from Singapore');
                $event->sheet->setCellValue('B'.($ttRowHeight+4), 'Departure from Batam');
                $event->sheet->setCellValue('B'.($ttRowHeight+10), 'Departure from Singapore');
                $event->sheet->setCellValue('B'.($ttRowHeight+14), 'Departure from Batam');
                
                $event->sheet->mergeCells('B'.$ttRowHeight.':B'.($ttRowHeight+3));
                $event->sheet->mergeCells('B'.($ttRowHeight+4).':B'.($ttRowHeight+9));
                $event->sheet->mergeCells('B'.($ttRowHeight+10).':B'.($ttRowHeight+13));
                $event->sheet->mergeCells('B'.($ttRowHeight+14).':B'.($ttRowHeight+19));

                $event->sheet->setCellValue('C'.$ttRowHeight, 'One Way');
                $event->sheet->setCellValue('C'.($ttRowHeight+2), 'Round Trip');
                $event->sheet->setCellValue('C'.($ttRowHeight+4), 'One Way');
                $event->sheet->setCellValue('C'.($ttRowHeight+6), 'Round Trip');
                $event->sheet->setCellValue('C'.($ttRowHeight+10), 'One Way');
                $event->sheet->setCellValue('C'.($ttRowHeight+12), 'Round Trip');
                $event->sheet->setCellValue('C'.($ttRowHeight+14), 'One Way');
                $event->sheet->setCellValue('C'.($ttRowHeight+16), 'Round Trip');
                
                $event->sheet->mergeCells('C'.$ttRowHeight.':C'.($ttRowHeight+1));
                $event->sheet->mergeCells('C'.($ttRowHeight+2).':C'.($ttRowHeight+3));
                $event->sheet->mergeCells('C'.($ttRowHeight+4).':C'.($ttRowHeight+5));
                $event->sheet->mergeCells('C'.($ttRowHeight+6).':C'.($ttRowHeight+9));
                $event->sheet->mergeCells('C'.($ttRowHeight+10).':C'.($ttRowHeight+11));
                $event->sheet->mergeCells('C'.($ttRowHeight+12).':C'.($ttRowHeight+13));
                $event->sheet->mergeCells('C'.($ttRowHeight+14).':C'.($ttRowHeight+15));
                $event->sheet->mergeCells('C'.($ttRowHeight+16).':C'.($ttRowHeight+19));

                $event->sheet->setCellValue('D'.$ttRowHeight, 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+1), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+2), 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+3), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+4), 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+5), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+6), 'Economy - Indonesian');
                $event->sheet->setCellValue('D'.($ttRowHeight+7), 'Economy - Foreign');
                $event->sheet->setCellValue('D'.($ttRowHeight+8), 'Business Class - Indonesian');
                $event->sheet->setCellValue('D'.($ttRowHeight+9), 'Business Class - Foreign');
                $event->sheet->setCellValue('D'.($ttRowHeight+10), 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+11), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+12), 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+13), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+14), 'Economy');
                $event->sheet->setCellValue('D'.($ttRowHeight+15), 'Business Class');
                $event->sheet->setCellValue('D'.($ttRowHeight+16), 'Economy - Indonesian');
                $event->sheet->setCellValue('D'.($ttRowHeight+17), 'Economy - Foreign');
                $event->sheet->setCellValue('D'.($ttRowHeight+18), 'Business Class - Indonesian');
                $event->sheet->setCellValue('D'.($ttRowHeight+19), 'Business Class - Foreign');

                $event->sheet->setCellValue('E'.$ttRowHeight, "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+1), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+2), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+3), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+4), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+5), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+6), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\",O$fr:O$hr,\"*ID*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+7), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\",O$fr:O$hr,\"<>ID\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+8), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\",O$fr:O$hr,\"*ID*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+9), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Adult*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\",O$fr:O$hr,\"<>ID\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+10), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+11), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+12), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+13), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Singapore*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+14), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Economy*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+15), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*One Way*\",L$fr:L$hr,\"*Business*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+16), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\",O$fr:O$hr,\"*ID*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+17), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Economy*\",O$fr:O$hr,\"<>ID\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+18), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\",O$fr:O$hr,\"*ID*\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+19), "=SUMIFS(J$fr:J$hr,P$fr:P$hr,\"*Infant*\",Q$fr:Q$hr,\"*Batam*\",K$fr:K$hr,\"*Round Trip*\",L$fr:L$hr,\"*Business*\",O$fr:O$hr,\"<>ID\")");
                $event->sheet->setCellValue('E'.($ttRowHeight+20), "=SUM(E$ttRowHeight:E".($ttMaxRowHeight-1).")");

                $event->sheet->getStyle($tCellRange)->applyFromArray($bordered);
                $event->sheet->getStyle('A'.$ttRowHeight.':A'.($ttMaxRowHeight - 1))->applyFromArray($leftHeading);
                $event->sheet->getStyle('B'.$ttRowHeight.':C'.($ttMaxRowHeight - 1))->applyFromArray($center);
                $event->sheet->getStyle('E'.$ttRowHeight.':E'.($ttMaxRowHeight - 1))->applyFromArray($center);
                $event->sheet->getStyle('A'.$ttMaxRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight)->applyFromArray($center);
                $event->sheet->getStyle('A'.$ttMaxRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight)->applyFromArray($bottomHeading);
            },
        ];
    }
}
