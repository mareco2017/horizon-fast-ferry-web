<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Carbon\Carbon;
use App\Enums\PassengerType;
use App\Enums\TicketType;

class OrdersExportView implements WithColumnFormatting, ShouldAutoSize, FromView, WithEvents
{
    protected $startDate;
    protected $endDate;
    protected $orders;

    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }
    
    public function view(): View
    {
        $orders = Order::query();
        
        if ($this->startDate || $this->endDate) {
            $date1 = $this->startDate ? Carbon::parse($this->startDate)->timezone('Asia/Jakarta') : null;
            $date2 = $this->endDate ? Carbon::parse($this->endDate)->timezone('Asia/Jakarta')->endOfDay() : null;
            
            if ($date1 && $date2) {
                $orders->whereBetween('created_at', [$date1->setTimezone('UTC'), $date2->setTimezone('UTC')]);
            } else if ($date1 && !$date2) {
                $orders->whereDate('created_at', '>=', $date1->setTimezone('UTC'));
            } else if (!$date1 && $date2) {
                $orders->where('created_at', '<=', $date2->setTimezone('UTC'));
            }
        }

        $this->orders = $orders->get();

        $thirdTableData = [
            'AdSgOwEc' => $this->getPax(PassengerType::ADULT, 'singapore', 0, TicketType::ECONOMY),
            'AdSgOwBc' => $this->getPax(PassengerType::ADULT, 'singapore', 0, TicketType::BUSINESS),
            'AdSgRtEc' => $this->getPax(PassengerType::ADULT, 'singapore', 1, TicketType::ECONOMY),
            'AdSgRtBc' => $this->getPax(PassengerType::ADULT, 'singapore', 1, TicketType::BUSINESS),
            'AdBaOwEc' => $this->getPax(PassengerType::ADULT, 'batam', 0, TicketType::ECONOMY),
            'AdBaOwBc' => $this->getPax(PassengerType::ADULT, 'batam', 0, TicketType::BUSINESS),
            'AdBaRtEcId' => $this->getPax(PassengerType::ADULT, 'batam', 1, TicketType::ECONOMY, 'ID'),
            'AdBaRtEcFo' => $this->getPax(PassengerType::ADULT, 'batam', 1, TicketType::ECONOMY, 'FO'),
            'AdBaRtBcId' => $this->getPax(PassengerType::ADULT, 'batam', 1, TicketType::BUSINESS, 'ID'),
            'AdBaRtBcFo' => $this->getPax(PassengerType::ADULT, 'batam', 1, TicketType::BUSINESS, 'FO'),

            'InSgOwEc' => $this->getPax(PassengerType::INFANT, 'singapore', 0, TicketType::ECONOMY),
            'InSgOwBc' => $this->getPax(PassengerType::INFANT, 'singapore', 0, TicketType::BUSINESS),
            'InSgRtEc' => $this->getPax(PassengerType::INFANT, 'singapore', 1, TicketType::ECONOMY),
            'InSgRtBc' => $this->getPax(PassengerType::INFANT, 'singapore', 1, TicketType::BUSINESS),
            'InBaOwEc' => $this->getPax(PassengerType::INFANT, 'batam', 0, TicketType::ECONOMY),
            'InBaOwBc' => $this->getPax(PassengerType::INFANT, 'batam', 0, TicketType::BUSINESS),
            'InBaRtEcId' => $this->getPax(PassengerType::INFANT, 'batam', 1, TicketType::ECONOMY, 'ID'),
            'InBaRtEcFo' => $this->getPax(PassengerType::INFANT, 'batam', 1, TicketType::ECONOMY, 'FO'),
            'InBaRtBcId' => $this->getPax(PassengerType::INFANT, 'batam', 1, TicketType::BUSINESS, 'ID'),
            'InBaRtBcFo' => $this->getPax(PassengerType::INFANT, 'batam', 1, TicketType::BUSINESS, 'FO'),
        ];
        
        return view('templates.excels.orders', [
            'orders' => $this->orders,
            'thirdTableData' => $thirdTableData,
        ]);
    }

    public function getPax($ageCategory, $departure, $twoWay, $class, $local = null)
    {
        $orders = $this->orders;
        $data = '';

        if ($orders->count() > 0) {
            $dTickets = collect();
            foreach ($orders as $order) {
                if ($order->options->departure_from == $departure && $order->options->two_way == $twoWay && $order->options->type == $class && count($order->details) > 0) {
                    foreach ($order->details()->ticket()->departure()->get() as $detail) {
                        $dTickets->push($detail);
                    }
                }
            }
            
            $data = $dTickets->filter(function($ticket) use ($ageCategory, $local) {
                $bool = false;

                if ($ticket->passenger) {
                    if ($ticket->passenger->type == $ageCategory) {
                        if ($local && $local == 'ID') {
                            if ($ticket->passenger->nationality == 'ID') $bool = true;
                        } else if ($local && $local != 'ID') {
                            if ($ticket->passenger->nationality == 'ID') {
                                $bool = false;
                            } else {
                                $bool = true;
                            }
                        } else {
                            $bool = true;
                        }
                    }
                }

                return $bool;
            })->count();
        }

        return $data;
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER,
            'Q' => NumberFormat::FORMAT_NUMBER,
            'U' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $firstTable = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $bordered = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['hex' => '000000'],
                        ],
                        'outline' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
                            'color' => ['hex' => '000000'],
                        ],
                    ],
                ];
                
                // First Table
                $highestRow = $event->sheet->getHighestRow();
                $ftRowHeight = $highestRow - 27;
                $ftColumnHeight = 'W';
                $fcellRange = 'A1:'.$ftColumnHeight.$ftRowHeight;

                $event->sheet->getStyle($fcellRange)->applyFromArray($firstTable);

                $headingStyles = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                ];
                $event->sheet->getStyle('A1:W2')->applyFromArray($headingStyles);

                // Second Table
                $stRowHeight = $ftRowHeight + 3;
                $stMaxRowHeight = $stRowHeight + 2;
                $stColumnHeight = 'C';
                $sCellRange = 'A'.$stRowHeight.':'.$stColumnHeight.$stMaxRowHeight;

                $leftHeading = [
                    'font' => [
                        'bold' => true,
                        'italic' => true
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $center = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ];

                $bottomHeading = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'startColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                        'endColor' => [
                            'argb' => 'FFd8d8d8',
                        ],
                    ],
                ];
                $paymentReceived = $event->sheet->setCellValue('C'.$stRowHeight,'=SUM(G3:G'.$ftRowHeight.')');
                $paymentFee = $event->sheet->setCellValue('C'.($stRowHeight + 1),'=SUM(H3:H'.$ftRowHeight.')');
                $netReceived = $event->sheet->setCellValue('C'.($stRowHeight + 2),'=C'.$stRowHeight.'-C'.($stRowHeight+1));

                $event->sheet->getStyle($sCellRange)->applyFromArray($bordered);
                $event->sheet->getStyle('A'.$stRowHeight.':A'.$stMaxRowHeight)->applyFromArray($leftHeading);
                $event->sheet->getStyle('C'.$stRowHeight.':C'.$stMaxRowHeight)->applyFromArray($center);
                $event->sheet->getStyle('B'.$stMaxRowHeight.':C'.$stMaxRowHeight)->applyFromArray($bottomHeading);

                // Third Table
                $ttRowHeight = $stMaxRowHeight + 2;
                $ttMaxRowHeight = $ttRowHeight + 20;
                $ttColumnHeight = 'E';
                $tCellRange = 'A'.$ttRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight;

                $event->sheet->getStyle($tCellRange)->applyFromArray($bordered);
                $event->sheet->getStyle('A'.$ttRowHeight.':A'.($ttMaxRowHeight - 1))->applyFromArray($leftHeading);
                $event->sheet->getStyle('B'.$ttRowHeight.':C'.($ttMaxRowHeight - 1))->applyFromArray($center);
                $event->sheet->getStyle('E'.$ttRowHeight.':E'.($ttMaxRowHeight - 1))->applyFromArray($center);
                $event->sheet->getStyle('A'.$ttMaxRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight)->applyFromArray($center);
                $event->sheet->getStyle('A'.$ttMaxRowHeight.':'.$ttColumnHeight.$ttMaxRowHeight)->applyFromArray($bottomHeading);
            },
        ];
    }
}
