<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Enquiry extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $email;
    public $name;
    public $enquiry;
    public $tickets;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $name, $enquiry)
    {
        $this->email = $email;
        $this->name = $name;
        $this->enquiry = $enquiry;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->email, $this->name)
                    ->view('templates.mail.enquiry');
    }
}
