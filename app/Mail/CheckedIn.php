<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\User;
use App\Models\Order;
use App\Models\Config as AppConfig;
use App\Enums\OrderDetailType;
use App\Enums\OrderDetailStatus;

class CheckedIn extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $order;
    public $contactPerson;
    public $departureTickets;
    public $returnTickets;
    public $barcode;
    public $wifi;
    public $car;
    public $pdf;
    public $wifiPenaltyPrice;
    public $wifiOnly;
    public $eBPs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user = null, Order $order, $pdf, $eBPs = [])
    {
        $this->user = $user;
        $this->order = $order;
        $this->contactPerson = $order->contactPerson;
        $this->departureTickets = $order->details()->departure()->ticket()->with('passenger')->get();
        $this->returnTickets = $order->details()->return()->ticket()->with('passenger')->get();
        $this->barcode = '<img src="https://barcode.tec-it.com/barcode.ashx?data='. $order->reference_number .'&code=Code128&dpi=96&dataseparator=" alt="barcode"/>';
        $this->wifi = $order->details()->typeTicket(OrderDetailType::WIFI_RENTAL)->first();
        $this->car = $order->details()->typeTicket(OrderDetailType::CAR_RENTAL)->first();
        $this->pdf = base64_encode($pdf);
        $this->wifiPenaltyPrice = $order->options->departure_from == 'singapore' ? AppConfig::where('key', 'wifi_penalty_price_batam')->value('value') : AppConfig::where('key', 'wifi_penalty_price_singapore')->value('value');
        $this->wifiOnly = isset($order->options->wifi_only) ? $order->options->wifi_only : false;
        $this->carOnly = isset($order->options->car_only) ? $order->options->car_only : false;
        $this->eBPs = $eBPs;
        \Log::info("Construct inside Mail instance Complete");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $desc = null;
            
        $hasWifi = $this->wifi && ($this->wifi->status == OrderDetailStatus::RESERVED || $this->wifi->status == OrderDetailStatus::CHECKED_IN);
        $hasCar = $this->car && ($this->car->status == OrderDetailStatus::RESERVED || $this->car->status == OrderDetailStatus::CHECKED_IN);
        \Log::info('Building mail: '.$this->order->reference_number);
        if (count($this->departureTickets) > 0 && ($hasWifi || $hasCar)) {
            $desc = ' (included ';

            if ($hasWifi && $hasCar) {
                $desc .= 'Wifi & Car';
            } else if ($hasWifi) {
                $desc .= 'Wifi';
            } else if ($hasCar) {
                $desc .= 'Car';
            }
            
            $desc .= ' Rental)';
        }
        $email = $this->subject('Checked In : '.$this->order->reference_number.$desc)
                ->view('templates.mail.checked-in')
                ->attachData(base64_decode($this->pdf), 'receipt.pdf', [
                    'mime' => 'application/pdf',
                ]);
        foreach ($this->eBPs as $eBP) {
            $email->attachData(base64_decode($eBP->pdf), $eBP->filename.'.pdf', [
                'mime' => 'application/pdf',
            ]);
        }
        \Log::info("All mail data attached. Sending Now");
        return $email;
    }
}
