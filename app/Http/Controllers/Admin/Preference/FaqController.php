<?php

namespace App\Http\Controllers\Admin\Preference;

use App\Http\Controllers\Controller;
use App\Helpers\Enums\ActiveStatus;
use App\Models\Faq;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Helpers\Managers\FaqManager;
use DB;
use Exception;

class FaqController extends Controller
{
    public function index(Request $request) 
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.preferences.faq.index')
            ->with('statusList', $statusList);
    }

    public function ajax(Request $request) 
    {
        $data = Faq::query();
        return Datatables::of($data)
            ->filter(function($query) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $query->where('status', $request->status_enum);
                }
                if ($request->has('type_enum') && $request->type_enum !== null) {
                    $query->where('type', $request->type_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.preferences.faq.edit', ['faq' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.preferences.faq.delete', ['faq' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->editColumn('status', function($model) {
                return ActiveStatus::getString($model->status);
            })
            ->editColumn('question', function($model) {
                return strlen($model->question) > 100 ? substr($model->question, 0, 100) : $model->question;
            })
            ->editColumn('answer', function($model) {
                return strlen($model->answer) > 100 ? substr($model->answer, 0, 100) : $model->answer;
            })
            ->make(true);
    }

    public function edit(Request $request, Faq $faq) 
    {
        $statusList = ActiveStatus::getArray();
        
        return view('admin.preferences.faq.form')
            ->with('faq', $faq)
            ->with('statusList', $statusList);
    }

    public function update(Request $request, Faq $faq) 
    {
        $messageType = $faq->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'question' => 'required',
            'answer' => 'required',
            'status' => 'required|integer',
        ]);

        try {
            DB::beginTransaction();
            $faqManager = new FaqManager;
            $updatedFaq = $faqManager->update($faq, $request->all());
            DB::commit();
            return redirect()->route('admin.preferences.faq.index')->with('success', 'FAQ successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->withInput()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Faq $faq)
    {
        try {
            DB::beginTransaction();
            $faqManager = new FaqManager;
            $updatedFaq = $faqManager->delete($faq);
            DB::commit();
            return response()->json(['message' => 'FAQ Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }

    public function customizeUrut(Request $request)
    {
        $displayedFaqs = Faq::active()->sort()->get();
        $undisplayedFaqs = Faq::inactive()->sort()->get();
        
        return view('admin.preferences.faq.customize-urut')
            ->with('type', $request->type)
            ->with('displayedFaqs', $displayedFaqs)
            ->with('undisplayedFaqs', $undisplayedFaqs);
    }

    public function updateUrut(Request $request)
    {
        try {
            DB::beginTransaction();
            $orders = $request->order ? explode(',', $request->order) : [];
            $nullifiers = $request->nullifier ? explode(',', $request->nullifier) : [];
            
            $faqManager = new FaqManager($request->user());
            $success = $faqManager->updateValues($orders, $nullifiers);
            
            DB::commit();
            return redirect()->route('admin.preferences.faq.index')->with('success', 'FAQ rearranged');
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }
}