<?php

namespace App\Http\Controllers\Admin\User;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\User;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.users.user.index');
    }

    public function edit(Request $request, User $user)
    {

    }

    // public function detail(Request $request, User $user)
    // {

    // }

    public function ajax(Request $request)
    {
    	$data = User::query();
    	return Datatables::of($data)
        ->addColumn('action', function ($model) {
            $str = '<div class="btn-group btn-group-circle">';
            $str .= '<a href='. route('admin.users.user.detail', ['user' => $model->id]) .' class="action"><span class="far fa-eye"></span></a>';
            $str .= '<a href='. route('admin.users.user.edit', ['user' => $model->id]) .' class="action"><span class="far fa-edit"></span></a>';
            $str .= '<a data-url="'. route('admin.users.user.delete', ['user' => $model->id]) .'" class="action" data-toggle="modal" data-target="#modalDelete" data-title="Confirmation" data-table-name="#users-table" data-message="Would you like to delete this role?"><span class="far fa-trash-alt"></span></a>';
            $str .= '</div>';
            return $str;
        })
        ->escapeColumns([])
        ->make(true);
    }

    public function update(Request $request, User $user)
    {
        // $validatedData = $request->validate([
        //     'status' => 'required',
        // ]);

        // try {
        //     DB::beginTransaction();
        //     $userManager = new UserManager($user);
        //     $updatedUser = $userManager->updateStatus($request->status);
        //     DB::commit();
        //     return redirect()->route('admin.user.index')->with('success', 'User Updated');
        // } catch (Exception $e) {
        //     DB::rollBack();
        //     $errorMessage = $e->getMessage();
        //     return redirect()->back()->with('error', $errorMessage);
        // }
    }

    public function delete(Request $request, User $user)
    {
        // $user->delete();

        // if ($request->ajax() || $request->wantsJson()) {
        //     return response('Success', 200);
        // }
        // return redirect()->route('admin.user.index');
    }
}