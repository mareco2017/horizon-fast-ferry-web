<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use App\Models\Comment;
use App\Models\Article;
use Yajra\Datatables\Datatables;
use App\Helpers\Managers\CommentManager;
use App\Helpers\Enums\VerificationStatus;

class CommentController extends Controller
{
    public function index(Request $request)
    {
        $statusList = VerificationStatus::getArray();
        return view('admin.comments.index')
            ->with('statusList', $statusList);
    }

    public function edit(Request $request, Comment $comment)
    {
        $statusList = VerificationStatus::getArray();
        return view('admin.comments.form')
            ->with('comment', $comment)
            ->with('statusList', $statusList);
    }

    public function ajax(Request $request)
    {
    	$data = Comment::with('article');
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.comment.edit', $model) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.comment.delete', $model) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('status', function ($model) {
                return VerificationStatus::getString($model->status);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Comment $comment)
    {
        $messageType = $comment->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'name' => 'required|string|alpha_spaces',
            'email' => 'required|email',
            'desc' => 'required',
            'status' => 'required|integer',
            'article_id' => 'required|integer',
        ]);

        try {
            DB::beginTransaction();

            $article = Article::find($request->article_id);
            if (!$article) {
                throw new Exception('Article Not Found!', 404);
            }

            $commentManager = new CommentManager;
            $updatedComment = $commentManager->update($comment, $request->all(), $article);

            DB::commit();
            return redirect()->route('admin.comment.index')->with('success', 'Comment successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->withInput()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Comment $comment)
    {
        try {
            DB::beginTransaction();
            $commentManager = new CommentManager;
            $updatedComment = $commentManager->delete($comment);
            DB::commit();
            return response()->json(['message' => 'Comment Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}