<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Promo;
use App\Helpers\Enums\ActiveStatus;
use App\Helpers\Managers\PromoManager;
use Yajra\Datatables\Datatables;
use DB;
use Exception;
use Validator;

class PromoController extends Controller
{
    public function index(Request $request)
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.latest-promotion.index')
            ->with('statusList', $statusList);
    }

    public function edit(Request $request, Promo $promo)
    {
        $statusList = ActiveStatus::getArray();
        
        return view('admin.latest-promotion.form')
            ->with('promo', $promo)
            ->with('statusList', $statusList);

    }

    public function ajax(Request $request)
    {
    	$data = Promo::query();
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.latest-promotion.edit', ['user' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.latest-promotion.delete', ['user' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->addColumn('start_end_date', function ($model) {
                $str = $model->start_date->format('j F Y').' / '.$model->end_date->format('j F Y');
                return $str;
            })
            ->editColumn('status', function($model) {
                return ActiveStatus::getString($model->status);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Promo $promo) 
    {
        $messageType = $promo->id ? 'updated' : 'created';

        $rules = [
            'status' => 'required|integer',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'cover' => 'file',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('cover', 'required', function($data) use ($promo) {
            return !$promo->id;
        });
        $validator->validate();
        
        try {
            DB::beginTransaction();
            $promoManager = new PromoManager;
            $updatedPromo = $promoManager->update($promo, $request->all());

            DB::commit();
            return redirect()->route('admin.latest-promotion.index')->with('success', 'Promo successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Promo $promo)
    {
        try {
            DB::beginTransaction();
            $promoManager = new PromoManager($request->user());
            $updatedPromo = $promoManager->delete($promo);
            DB::commit();
            return response()->json(['message' => 'Promo Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}