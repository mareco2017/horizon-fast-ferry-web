<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Validator;
use App\Models\Order;
use App\Models\ContactPerson;
use Yajra\Datatables\Datatables;
use App\Enums\OrderStatus;
use App\Enums\OrderDetailType;
use App\Helpers\Managers\OrderManager;
use Carbon\Carbon;
use Excel;
use App\Exports\OrdersExport;
use App\Exports\WifiOrderExport;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $statusList = OrderStatus::getArray();
        $wifiStatus = [
            0 => 'No',
            1 => 'Has Wifi',
            2 => 'Wifi Only',
        ];
        return view('admin.order.index')
            ->with('statusList', $statusList)
            ->with('wifiStatus', $wifiStatus);
    }

    public function edit(Request $request, Order $order)
    {
        $dTickets = $order->details()->ticket()->departure()->get();
        $rTickets = $order->details()->ticket()->return()->get();
        $wifiRental = $order->details()->wifiRental()->first();
        $contactPerson = $order->contactPerson;
        return view('admin.order.form')
            ->with('order', $order)
            ->with('dTickets', $dTickets)
            ->with('rTickets', $rTickets)
            ->with('contactPerson', $contactPerson)
            ->with('wifiRental', $wifiRental);
    }

    public function ajax(Request $request)
    {
    	$data = Order::with('contactPerson');
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('start_date') || $request->has('end_date')) {
                    $date1 = $request->start_date ? Carbon::parse($request->start_date)->timezone('Asia/Jakarta') : null;
                    $date2 = $request->end_date ? Carbon::parse($request->end_date)->timezone('Asia/Jakarta')->endOfDay() : null;
                    
                    if ($date1 && $date2) {
                        $model->whereBetween('created_at', [$date1->setTimezone('UTC'), $date2->setTimezone('UTC')]);
                    } else if ($date1 && !$date2) {
                        $model->whereDate('created_at', '>=', $date1->setTimezone('UTC'));
                    } else if (!$date1 && $date2) {
                        $model->where('created_at', '<=', $date2->setTimezone('UTC'));
                    }
                }
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                }
                if ($request->has('wifi') && $request->wifi !== null) {
                    if ($request->wifi == 2) {
                        $model->where('options->wifi_only', true);
                    } else if ($request->wifi == 1) {
                        $model->where('options->wifi_devices', '>', 0);
                    } else {
                        $model->where('options->wifi_devices', 0);
                    }
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                if ($model->status == OrderStatus::WAITING_FOR_PAYMENT) {
                    $str .= '<a href='. route('admin.order.edit', ['order' => $model]) .' class="action" title="Complete This Order"><span class="fas fa-check"></span></a>';
                    $str .= '<a data-url="'. route('admin.order.cancel', ['order' => $model]) .'" class="action" data-toggle="modal" data-method="POST" data-target="#modalConfirm" data-table-name="#table" data-message="Are you sure want to CANCEL this Order? This may cause a system <b>ERROR</b> or <b>FAILURE</b> towards this app"><span class="fas fa-times"></span></a>';
                }
                $str .= '<a href='. route('admin.order.detail', ['order' => $model]) .' class="action" title="Detail"><span class="far fa-eye"></span></a>';
                $str .= '</div>';
                return $str;
            })
            // ->addColumn('contact_person', function ($model) {
            //     $contactPerson = $model->contactPerson;
            //     $str = '<p>Email: ' . $contactPerson->email . '</p>' . '<p class="mb-0">Phone Number: ' . $contactPerson->phone_number . '</p>';
            //     return $str;
            // })
            ->addColumn('wifi', function ($model) {
                return $model->details()->wifiRental()->first() ? 'Yes' : 'No';
            })
            ->addColumn('departure', function ($model) {
                return ucfirst($model->options->departure_from);
            })
            ->addColumn('status', function ($model) {
                return OrderStatus::getString($model->status);
            })
            ->escapeColumns([])
            ->orderColumn('id', '-id $1')
            ->make(true);
    }

    public function update(Request $request, Order $order)
    {
        $messageType = $order->id ? 'updated' : 'created';

        $rules = [
            'boarding_pass_depart' => 'required|array',
            'boarding_pass_depart.*' => 'required|alpha_dash',
            'boarding_pass_return.*' => 'required_with:boarding_pass_return|alpha_dash',
        ];
        $messages = [
            'boarding_pass_depart.*.required' => 'The Boarding Pass field is required.',
            'boarding_pass_depart.*.alpha_dash' => 'The Boarding Pass may only contain letters, numbers, and dashes.',
            'boarding_pass_return.*.required_with' => 'The Boarding Pass field is required.',
            'boarding_pass_return.*.alpha_dash' => 'The Boarding Pass may only contain letters, numbers, and dashes.',
        ];
        
        $validator = \Validator::make($request->all(), $rules, $messages);
        $validator->sometimes('boarding_pass_return', 'required|array', function($data) use ($order) {
            return $order->options->two_way;
        });
        $validator->validate();

        try {
            DB::beginTransaction();
            $orderManager = new OrderManager;
            $updatedOrder = $orderManager->update($order, $request->only('boarding_pass_depart', 'boarding_pass_return'));
            DB::commit();

            $sendMail = $orderManager->sendMailWithReceipt($order);

            return redirect()->route('admin.order.index')->with('success', 'Order successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage)->withInput();
        }
    }

    public function cancel(Request $request, Order $order)
    {
        try {
            DB::beginTransaction();
            $orderManager = new OrderManager;
            $updatedOrder = $orderManager->cancelOrder($order);
            DB::commit();
            return response()->json(['message' => 'Order Cancelled'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }

    public function detail(Request $request, Order $order)
    {
        $dTickets = $order->details()->ticket()->departure()->get();
        $rTickets = $order->details()->ticket()->return()->get();
        $wifiRental = $order->details()->wifiRental()->first();
        $carRental = $order->details()->carRental()->first();
        $contactPerson = $order->contactPerson;

        return view('admin.order.detail')
            ->with('order', $order)
            ->with('dTickets', $dTickets)
            ->with('rTickets', $rTickets)
            ->with('contactPerson', $contactPerson)
            ->with('wifiRental', $wifiRental)
            ->with('carRental', $carRental);
    }

    public function exportExcel(Request $request)
    {
        $a = $request->start_date ?? 'Beginning';
        $b = $request->end_date ?? 'End';

        if ($request->has('wifi') && $request->wifi == 2) {
            $filename = 'Wifi Orders '.$a.' - '.$b.'.xlsx';
            return Excel::download(new WifiOrderExport($request->start_date, $request->end_date, $request->status), $filename);
        } else {
            $filename = 'Orders '.$a.' - '.$b.'.xlsx';
            return Excel::download(new OrdersExport($request->start_date, $request->end_date, $request->status), $filename);
        }

    }
}