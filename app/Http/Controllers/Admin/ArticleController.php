<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Validator;
use App\Models\Article;
use Yajra\Datatables\Datatables;
use App\Helpers\Managers\ArticleManager;
use App\Helpers\Enums\ActiveStatus;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.articles.index')
            ->with('statusList', $statusList);
    }

    public function edit(Request $request, Article $article)
    {
        $statusList = ActiveStatus::getArray();
        return view('admin.articles.form')
            ->with('article', $article)
            ->with('statusList', $statusList);
    }

    public function ajax(Request $request)
    {
    	$data = Article::with('publisher');
    	return Datatables::of($data)
            ->filter(function($model) use ($request) {
                if ($request->has('status_enum') && $request->status_enum !== null) {
                    $model->where('status', $request->status_enum);
                }
            }, true)
            ->addColumn('action', function ($model) {
                $str = '<div class="btn-group btn-group-circle">';
                $str .= '<a href='. route('admin.article.edit', ['article' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                $str .= '<a data-url="'. route('admin.article.delete', ['article' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table"><span class="far fa-trash-alt"></span></a>';
                $str .= '</div>';
                return $str;
            })
            ->addColumn('cover', function ($model) {
                $str = $model->cover_url;
                return $str;
            })
            ->addColumn('status', function ($model) {
                return ActiveStatus::getString($model->status);
            })
            ->addColumn('tags', function ($model) {
                return $model->tags ? $model->tags : '' ;
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function update(Request $request, Article $article)
    {
        $messageType = $article->id ? 'updated' : 'created';

        $rules = [
            'title' => 'required',
            'tags' => 'nullable',
            'intro' => 'nullable',
            'status' => 'required|integer',
            'desc' => 'nullable',
            'cover' => 'file',
        ];
        $validator = Validator::make($request->all(), $rules);
        $validator->sometimes('cover', 'required', function($data) use ($article) {
            return !$article->id;
        });
        $validator->validate();
        
        try {
            DB::beginTransaction();
            $articleManager = new ArticleManager($request->user());
            $updatedArticle = $articleManager->update($article, $request->all());

            DB::commit();
            return redirect()->route('admin.article.index')->with('success', 'Article successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->withInput()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Article $article)
    {
        try {
            DB::beginTransaction();
            $articleManager = new ArticleManager($request->user());
            $updatedArticle = $articleManager->delete($article);
            DB::commit();
            return response()->json(['message' => 'Article Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}