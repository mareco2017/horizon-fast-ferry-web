<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\RedirectsUsers;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\Admin;
use App\Models\Token;

class AuthController extends Controller
{
    use RedirectsUsers, ThrottlesLogins;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    public function loginPage(Request $request)
    {
        return view('admin.auth.login');
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
            'first_name' => 'required|string|alpha_spaces',
            'last_name' => 'nullable|string|alpha_spaces',
            'gender' => 'nullable',
            'dob' => 'nullable|date_format:Y-m-d'
        ]);

        try {
            DB::beginTransaction();
            $admin = new Admin;
            $admin->phone_number = $request->phone_number;
            $admin->email = $request->email;
            $admin->password = Hash::make($request->password);
            $admin->first_name = $request->first_name;
            $admin->last_name = $request->last_name;
            $admin->gender = $request->gender;
            $admin->dob = $request->dob;

            if (!$admin->save()) {
                throw new Exception("Failed while registering admin!", 500);
            }
            DB::commit();

            $this->guard()->login($admin, true);
            $loggedInAdmin = $this->guard()->user();

            return redirect()->route('admin.dashboard')->with('success', 'Successfully registering admin!');
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        try {
            $field = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
            $request->merge([$field => $request->input('username')]);
            
            $credentials = $request->only($field, 'password');

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if (!$this->guard()->attempt($credentials)) {
                $this->incrementLoginAttempts($request);
                throw new Exception("Wrong username or password!", 400);
            }

            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            // $this->checkRedirectPath($request);

            $loggedInAdmin = $this->guard()->user();
            $token = $this->createToken($request, $loggedInAdmin, $request->session()->getId());
            
            return redirect()->route('admin.dashboard')->with('success', 'Successfully logged in!');
        } catch (Exception $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        }
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        return redirect()->route('admin.login')->with('success', 'Successfully logged out!');
    }

    public function username()
    {
        return 'email';
    }

    protected function createToken(Request $request, $admin, $tokenString = null)
    {
        try {
            DB::beginTransaction();
            $payload = $this->guard()->payload();
            $exp = $payload->get('exp');
            $expiredAt = date("Y-m-d H:i:s", $exp);
            $token = new Token;
            $token->token = $tokenString;
            $token->type = 'web-admin';
            $token->device_type = $request->header('Device-Type') ?: $request->header('User-Agent');
            $token->ip_address = $request->ip();
            $token->device_token = $request->header('Device-Token');
            $token->ownable()->associate($admin);
            $token->expired_at = $expiredAt;

            if (!$token->save()) {
                throw new Exception("Failed while saving token!", 500);
            }
            DB::commit();

            return $token;
        } catch (Exception $e) {
            DB::rollBack();
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            throw new Exception($e->getMessage(), $code);
        }
    }

    protected function checkEmailAvailability($email)
    {
        return Admin::where('email', $email)->exists();
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('web-admin');
    }
}