<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Managers\PassengerManager;
use App\Enums\Gender;
use App\Enums\PassengerType;
use App\Models\Passenger;
use Yajra\Datatables\Datatables;
use DB;
use Exception;
use File;
use Illuminate\Support\Collection;

class PassengerController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.passenger.index');
    }

    public function edit(Request $request, Passenger $passenger)
    {
        $countries = json_decode(File::get(storage_path('app/public/country.json')));
        $issuingCountries = json_decode(File::get(storage_path('app/public/issuing-country.json')));
        Collection::macro('toAssoc', function () {
            return $this->reduce(function ($assoc, $keyValuePair) {
                list($key, $value) = $keyValuePair;
                $assoc[$key] = $value;
                return $assoc;
            }, new static);
        });

        Collection::macro('mapToAssoc', function ($callback) {
            return $this->map($callback)->toAssoc();
        });

        $nationalities = collect($countries)->mapToAssoc(function ($country) {
            return [$country->value, $country->text];
        });

        $issuingNationalities = collect($issuingCountries)->mapToAssoc(function ($country) {
            return [$country->value, $country->text];
        });
        $genders = Gender::getArray();
        $types = PassengerType::getArray();
        return view('admin.passenger.form')
            ->with('nationalities', $nationalities)
            ->with('issuingNationalities', $issuingNationalities)
            ->with('genders', $genders)
            ->with('types', $types)
            ->with('passenger', $passenger);
    }

    public function ajax(Request $request)
    {
    	$data = Passenger::query();
    	return Datatables::of($data)
                ->addColumn('action', function ($model) {
                    $str = '<div class="btn-group btn-group-circle">';
                    $str .= '<a href='. route('admin.passenger.edit', ['user' => $model]) .' class="action"><span class="far fa-edit"></span></a>';
                    $str .= '<a data-url="'. route('admin.passenger.delete', ['user' => $model]) .'" class="action" data-toggle="modal" data-target="#modalConfirm" data-table-name="#table" data-message="Are you sure want to delete this data? This may cause a system <b>ERROR</b> or <b>FAILURE</b> towards this app"><span class="far fa-trash-alt"></span></a>';
                    $str .= '</div>';
                    return $str;
                })
                ->editColumn('dob', function ($model) {
                    return $model->dob->format('Y-m-d');
                })
                ->editColumn('passport_issue', function ($model) {
                    return $model->passport_issue->format('Y-m-d');
                })
                ->editColumn('passport_expiry', function ($model) {
                    return $model->passport_expiry->format('Y-m-d');
                })
                ->editColumn('gender', function ($model) {
                    return Gender::getString($model->gender);
                })
                ->editColumn('type', function ($model) {
                    return PassengerType::getString($model->type);
                })
                ->escapeColumns([])
                ->orderColumn('id', '-id $1')
				->make(true);
    }

    public function update(Request $request, Passenger $passenger)
    {
        $messageType = $passenger->id ? 'updated' : 'created';

        $validatedData = $request->validate([
            'passport_number' => 'required|unique:passengers,passport_number,'.$passenger->id.',id',
            'name' => 'required|string|alpha_spaces',
            'nationality' => 'required|string',
            'gender' => 'required|in:0,1,2',
            'dob' => 'required|date_format:Y-m-d',
            'passport_expiry' => 'required|date_format:Y-m-d',
            'issuing_country' => 'required|string',
            'passport_issue' => 'required|date_format:Y-m-d',
            'type' => 'required|in:0,1,2'
        ]);
        
        try {
            DB::beginTransaction();
            $passengerManager = new PassengerManager;
            $updatedPassenger = $passengerManager->update($passenger, $request->except('_token'));
            DB::commit();
            return redirect()->route('admin.passenger.index')->with('success', 'Passenger successfully '.$messageType);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return redirect()->back()->with('error', $errorMessage);
        }
    }

    public function delete(Request $request, Passenger $passenger)
    {
        try {
            DB::beginTransaction();
            $passengerManager = new PassengerManager($request->user());
            $updatedPassenger = $passengerManager->delete($passenger);
            DB::commit();
            return response()->json(['message' => 'Passenger Deleted'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            $errorMessage = $e->getMessage();
            return response()->json(['message' => $errorMessage], 500);
        }
    }
}
