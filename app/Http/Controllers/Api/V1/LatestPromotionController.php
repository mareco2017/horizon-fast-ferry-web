<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Exception;
use App\Models\Promo;

class LatestPromotionController extends Controller
{
    use ApiResponse;
    
    public function get(Request $request)
    {
        try {
            $promos = Promo::active()->orderBy('created_at', 'desc')->get();
            return $this->jsonResponse("Success", ['promos' => $promos]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}