<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Exception;
use App\Models\Faq;

class FaqController extends Controller
{
    use ApiResponse;

    public function get(Request $request)
    {
        try {
            $faqs = Faq::active()->sort()->get();
            return $this->jsonResponse("Success", ['faqs' => $faqs]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}