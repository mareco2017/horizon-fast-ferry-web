<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Exception;
use App\Models\Article;
use App\Models\Comment;
use App\Helpers\Managers\CommentManager;

class ArticleController extends Controller
{
    use ApiResponse;

    public function get(Request $request)
    {
        try {
            $articles = Article::active()->orderBy('created_at', 'desc')->paginate(6);
            return $this->jsonResponse("Success", ['articles' => $articles]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function getArticleDetail(Request $request, Article $article)
    {
        $prevArticle = Article::where('id', '<', $article->id)->orderBy('id', 'desc')->select('title', 'slug')->first();
        $nextArticle = Article::where('id', '>', $article->id)->orderBy('id', 'asc')->select('title', 'slug')->first();
        return $this->jsonResponse("Success", ['article' => $article->load('comments'), 'prevArticle' => $prevArticle, 'nextArticle' => $nextArticle]);
    }

    public function getBatamHighlight(Request $request)
    {
        try {
            $articles = Article::active()->orderBy('created_at', 'desc')->take(6)->get();
            return $this->jsonResponse("Success", ['articles' => $articles]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function postComment(Request $request, Article $article)
    {
        $validatedData = $request->validate([
            'desc' => 'required',
            'name' => 'required|string|alpha_spaces',
            'email' => 'required|email',
        ]);

        try {
            if (!$article) {
                throw new Exception('Article Not Found!', 404);
            }

            $commentManager = new CommentManager;
            $updatedComment = $commentManager->update(new Comment, $request->all(), $article);

            return $this->jsonResponse("Success");
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}