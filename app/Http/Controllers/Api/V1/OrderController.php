<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Carbon\Carbon;
use Exception;
use App\Models\User;
use App\Models\ContactPerson;
use App\Models\Passenger;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Config as AppConfig;
use App\Enums\TicketType;
use App\Enums\Gender;
use App\Enums\OrderStatus;
use App\Enums\PassengerType;
use App\Enums\OrderDetailType;
use App\Enums\OrderDetailStatus;
use App\Enums\TripType;
use Midtrans;
use Stripe;
use SoapClient;
use Illuminate\Support\Facades\Mail;
use App\Mail\CheckedIn;
use DateTime;
use App\Helpers\Managers\OrderManager;
use Stripe\Stripe as StripePHP;
use Stripe\PaymentIntent as StripePaymentIntent;
use Stripe\Customer as StripeCustomer;
use Stripe\PaymentMethod as StripePaymentMethod;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class OrderController extends Controller
{
    use ApiResponse;

    protected $url;
    protected $username;
    protected $password;
    protected $checkedIn;
    protected $checkInErrors;
    protected $client;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        StripePHP::setApiKey(config('services.stripe.secret'));
        $this->url = 'http://43.225.185.198:6300/API_HorizonFastFerryBatam/HFFBatam.asmx';
        $this->username = config('horizon.username');
        $this->password = config('horizon.password');
        $this->checkedIn = [];
        $this->checkInErrors = [];
        $this->client = new Client();
    }

    public function owned(Request $request)
    {
        $user = $this->guard()->user();

        $orders = $user->orders()->with('contactPerson', 'details.passenger')->latest()->get();

        return $this->jsonResponse("Success", array('orders' => $orders));
    }

    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'reference_number' => 'required|string',
            'email' => 'nullable|email'
        ]);

        try {
            $user = $this->guard()->user();

            $order = null;

            if ($user) {
                $order = $user->orders()->where('reference_number', $request->reference_number)->latest()->first();
            } else {
                $email = $request->email;
                $order = Order::where('reference_number', $request->reference_number)->whereHas('contactPerson', function($query) use ($email) {
                        $query->where('email', $email);
                })->latest()->first();
            }

            if ($order instanceof Order) {
                $order->load('contactPerson', 'ownable', 'details.passenger');
            }

            $departDetails = $order->details()->ticket()->departure()->with('passenger')->get();
            $returnDetails = $order->details()->ticket()->return()->with('passenger')->get();
            $wifiDetail = $order->details()->typeTicket(OrderDetailType::WIFI_RENTAL)->first();
            $carDetail = $order->details()->typeTicket(OrderDetailType::CAR_RENTAL)->first();

            $bookingSummary = [
                'total_adult' => $order->options->two_way ? $order->details()->typeTicket(OrderDetailType::TICKET_ADULT)->count() / 2 : $order->details()->typeTicket(OrderDetailType::TICKET_ADULT)->count(),
                'total_infant' => $order->options->two_way ? $order->details()->typeTicket(OrderDetailType::TICKET_CHILD)->count() / 2 : $order->details()->typeTicket(OrderDetailType::TICKET_CHILD)->count(),
                'total_ticket_price' => $order->total_ticket_price,
                'total_adult_ticket_price' => $order->total_adult_ticket_price,
                'total_infant_ticket_price' => $order->total_infant_ticket_price,
                'total_surcharge' => $order->total_surcharge,
                'total_terminal_fee' => $order->total_terminal_fee,
                'refundable_deposit' => AppConfig::where('key', 'wifi_refundable_deposit')->value('value'),
            ];

            return $this->jsonResponse("Success", array(
                'order' => $order,
                'departDetails' => $departDetails,
                'returnDetails' => $returnDetails,
                'bookingSummary' => $bookingSummary,
                'wifiDetail' => $wifiDetail,
                'carDetail' => $carDetail,
                'wifiPenaltyPrice' => $order->options->departure_from == 'singapore' ? AppConfig::where('key', 'wifi_penalty_price_batam')->value('value') : AppConfig::where('key', 'wifi_penalty_price_singapore')->value('value'),
            ));
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function reserve(Request $request)
    {
        $validatedData = $request->validate([
            'two_way' => 'required|boolean',
            'departure_schedule_id' => 'required',
            'departure_date' => 'required|date_format:Y-m-d',
            'departure_time' => 'required|date_format:H:i',
            'departure_from' => 'required|string',
            'departure_route' => 'required|string',
            'return_schedule_id' => 'required_if:two_way,1',
            'return_date' => 'required_if:two_way,1|date_format:Y-m-d',
            'return_time' => 'required_if:two_way,1|date_format:H:i',
            'return_from' => 'required_if:two_way,1|string',
            'return_route' => 'required_if:two_way,1|string',
            'type' => 'required|in:0,1,2',
            'quantity' => 'required|numeric|min:1',
            'wifi_start_date' => 'nullable|date_format:Y-m-d',
            'wifi_end_date' => 'nullable|date_format:Y-m-d',
            'wifi_devices' => 'required|numeric',
            'selected_car' => 'nullable|string|in:car_5_seater,car_10_seater',
            'car_start_date' => 'nullable|date_format:Y-m-d',
            'car_end_date' => 'nullable|date_format:Y-m-d',
            'car_pick_up_at' => 'nullable|string',
            'car_checkpoint' => 'nullable|string',

            'contact_person' => 'required',
            'contact_person.email' => 'required|email|confirmed',
            'contact_person.email_confirmation' => 'required',
            'contact_person.phone_number' => 'required',
            'contact_person.first_name' => 'nullable',
            'contact_person.last_name' => 'nullable',

            'passenger' => 'required|array',
            'passenger.*.passport_number' => 'required',
            'passenger.*.name' => 'required|string|alpha_spaces',
            'passenger.*.nationality' => 'required|string',
            'passenger.*.gender' => 'required|in:0,1,2',
            'passenger.*.dob' => 'required|date_format:Y-m-d',
            'passenger.*.passport_expiry' => 'required|date_format:Y-m-d',
            'passenger.*.issuing_country' => 'required|string',
            'passenger.*.passport_issue' => 'required|date_format:Y-m-d',
            'passenger.*.type' => 'required|in:0,1,2'
        ]);

        $departureSeatResult = null;
        $returnSeatResult = null;

        try {
            $user = $this->guard()->user();

            if (count($request->passenger) !== $request->quantity) {
                throw new Exception("Quantity and passengers data not equal.", 400);
            }

            $this->checkPassportExpiry($request->departure_date, $request->passenger);

            DB::beginTransaction();

            $departureDate = Carbon::createFromFormat('Y-m-d', $request->departure_date);
            $returnDate = Carbon::createFromFormat('Y-m-d', $request->return_date);

            $departureSeatResult = $this->checkSeatAvailability($request->departure_from, $departureDate->format('Ymd'), $request->departure_schedule_id, $request->type, $request->quantity);

            if ($request->two_way) {
                $returnSeatResult = $this->checkSeatAvailability($request->return_from, $returnDate->format('Ymd'), $request->return_schedule_id, $request->type, $request->quantity);
            }

            $contactPerson = $this->createContactPerson($request->contact_person,  $user);

            $data = [
                'contact_person' => $contactPerson,
                'options' => (object)[
                    'two_way' => $request->two_way,
                    'departure_schedule_id' => $request->departure_schedule_id,
                    'departure_date' => $request->departure_date,
                    'departure_time' => $request->departure_time,
                    'departure_from' => $request->departure_from,
                    'departure_route' => $request->departure_route,
                    'departure_reservation_key' => $departureSeatResult ? $departureSeatResult->reservationKey : null,
                    'return_schedule_id' => $request->return_schedule_id,
                    'return_date' => $request->return_date,
                    'return_time' => $request->return_time,
                    'return_from' => $request->return_from,
                    'return_route' => $request->return_route,
                    'return_reservation_key' => $returnSeatResult ? $returnSeatResult->reservationKey : null,
                    'wifi_devices' => $request->wifi_devices,
                    'wifi_start_date' => $request->wifi_start_date,
                    'wifi_end_date' => $request->wifi_end_date,
                    'selected_car' => $request->selected_car,
                    'car_start_date' => $request->car_start_date,
                    'car_end_date' => $request->car_end_date,
                    'car_pick_up_at' => $request->car_pick_up_at,
                    'car_checkpoint' => $request->car_checkpoint,
                    'type' => $request->type,
                    'quantity' => $request->quantity
                ],
                'type' => $request->type,
                'quantity' => $request->quantity,
                'passengers' => $request->passenger
            ];

            $order = $this->createOrder($data, $user);

            $paymentDesc = $order->reference_number;
            if ($order->details()->wifiRental()->first()) {
                $wifiStartDate = Carbon::parse($data['options']->wifi_start_date);
                $wifiEndDate = Carbon::parse($data['options']->wifi_end_date);
                $totalDays = $wifiStartDate->diffInDays($wifiEndDate) + 1;

                $paymentDesc .= " - Included ".$data['options']->wifi_devices." units of ";
                $paymentDesc .= $data['options']->departure_from == 'batam' ? 'SINGAPORE' : 'BATAM';
                $paymentDesc .= " Wifi Rental for ";
                $paymentDesc .= $wifiStartDate->format('j M Y').' - '.$wifiEndDate->format('j M Y');
                $paymentDesc .= " | $totalDays day(s)";
            }
            if ($order->details()->carRental()->first()) {
                $carStartDate = Carbon::parse($data['options']->car_start_date);
                $carEndDate = Carbon::parse($data['options']->car_end_date);
                $totalDays = $carStartDate->diffInDays($carEndDate) + 1;
                $selectedCar = $order->options->selected_car;

                $paymentDesc .= " - Included 1 unit(s) of ";
                $paymentDesc .=  $selectedCar == 'car_5_seater' ? " 5 Seater Car " : " 10 Seater Car ";
                $paymentDesc .= $carStartDate->format('j M Y').' - '.$carEndDate->format('j M Y');
                $paymentDesc .= " | $totalDays day(s)";
            }
            
            $intent = StripePaymentIntent::create([
                'amount' => $order->total * 100,
                'currency' => 'sgd',
                'capture_method' => 'manual',
                'setup_future_usage' => 'off_session',
                'description' => $paymentDesc,
                'metadata' => ['order_id' => $order->id]
            ]);

            $paymentData = ['payment' => $intent];

            // if ($order->options->wifi_devices) {
            //     $wifi = $order->details()->wifiRental()->first();
            //     if ($wifi) {
            //         $wifiIntent = StripePaymentIntent::create([
            //             'amount' => $order->total_wifi * 100,
            //             'currency' => 'sgd',
            //             'capture_method' => 'manual',
            //             'description' => $order->reference_number . ' Wifi',
            //             'metadata' => ['order_id' => $order->id]
            //         ]);
            //         $paymentData['wifi_payment'] = $wifiIntent;


            //         $wifiRefundableIntent = StripePaymentIntent::create([
            //             'amount' => ($wifi->options->wifi_refund_price * $wifi->quantity) * 100,
            //             'currency' => 'sgd',
            //             'capture_method' => 'manual',
            //             'description' => $order->reference_number . ' Wifi refundable',
            //             'metadata' => ['order_id' => $order->id]
            //         ]);
            //         $paymentData['wifi_refundable_payment'] = $wifiRefundableIntent;
            //     }
            // }
            
            $updatedOrder = $this->updateOrderPayment($order, $paymentData);

            DB::commit();

            return $this->jsonResponse("Success", array('order' => $updatedOrder));
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error("Error reserving");
            \Log::error($e);
            // Cancel reservation
            if ($departureSeatResult && isset($departureSeatResult->reservationKey)) {
                $this->cancelReservation($request->departure_schedule_id, $departureSeatResult->reservationKey);
            }
            // if ($returnSeatResult && isset($returnSeatResult->reservationKey)) {
            //     $this->cancelReservation($request->return_schedule_id, $returnSeatResult->reservationKey);
            // }

            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function reserveWifi(Request $request)
    {
        $validatedData = $request->validate([
            'departure_from' => 'required|string',

            'wifi_only' => 'required|boolean',
            'wifi_start_date' => 'nullable|date_format:Y-m-d',
            'wifi_end_date' => 'nullable|date_format:Y-m-d',
            'wifi_devices' => 'required|numeric',

            'contact_person' => 'required',
            'contact_person.email' => 'required|email|confirmed',
            'contact_person.email_confirmation' => 'required',
            'contact_person.phone_number' => 'required',
            'contact_person.first_name' => 'nullable',
            'contact_person.last_name' => 'nullable',

            'passenger' => 'required',
            'passenger.passport_number' => 'nullable',
            'passenger.name' => 'required|string|alpha_spaces',
            'passenger.nationality' => 'required|string',
            'passenger.gender' => 'required|in:0,1,2',
            'passenger.dob' => 'required|date_format:Y-m-d',
            'passenger.passport_expiry' => 'nullable|date_format:Y-m-d',
            'passenger.issuing_country' => 'required|string',
            'passenger.passport_issue' => 'nullable|date_format:Y-m-d',
            'passenger.type' => 'required|in:0,1,2'
        ]);
        try {
            $user = $this->guard()->user();

            DB::beginTransaction();
            $contactPerson = $this->createContactPerson($request->contact_person,  $user);
            $data = [
                'contact_person' => $contactPerson,
                'options' => (object)[
                    'selected_car' => null,
                    'wifi_only' => $request->wifi_only,
                    'two_way' => false,
                    // 'departure_schedule_id' => $request->departure_schedule_id,
                    // 'departure_date' => $request->departure_date,
                    // 'departure_time' => $request->departure_time,
                    'departure_from' => $request->departure_from,
                    // 'departure_route' => $request->departure_route,
                    // 'departure_reservation_key' => $departureSeatResult ? $departureSeatResult->reservationKey : null,
                    // 'return_schedule_id' => $request->return_schedule_id,
                    // 'return_date' => $request->return_date,
                    // 'return_time' => $request->return_time,
                    // 'return_from' => $request->return_from,
                    // 'return_route' => $request->return_route,
                    // 'return_reservation_key' => $returnSeatResult ? $returnSeatResult->reservationKey : null,
                    'wifi_devices' => $request->wifi_devices,
                    'wifi_start_date' => $request->wifi_start_date,
                    'wifi_end_date' => $request->wifi_end_date,
                    'type' => null,
                    'quantity' => 0,
                ],
                // 'type' => $request->type,
                // 'quantity' => $request->quantity,
                'passengers' => [$request->passenger]
            ];
            $order = $this->createOrder($data, $user);

            $paymentDesc = $order->reference_number;
            if ($order->details()->wifiRental()->first()) {
                $wifiStartDate = Carbon::parse($data['options']->wifi_start_date);
                $wifiEndDate = Carbon::parse($data['options']->wifi_end_date);
                $totalDays = $wifiStartDate->diffInDays($wifiEndDate) + 1;

                $paymentDesc .= " - ".$data['options']->wifi_devices." units of ";
                $paymentDesc .= $data['options']->departure_from == 'batam' ? 'SINGAPORE' : 'BATAM';
                $paymentDesc .= " Wifi Rental for ";
                $paymentDesc .= $wifiStartDate->format('j M Y').' - '.$wifiEndDate->format('j M Y');
                $paymentDesc .= " | $totalDays day(s)";
            }
            
            $intent = StripePaymentIntent::create([
                'amount' => $order->total_wifi * 100,
                'currency' => 'sgd',
                'capture_method' => 'manual',
                'setup_future_usage' => 'off_session',
                'description' => $paymentDesc,
                'metadata' => ['order_id' => $order->id]
            ]);

            $paymentData = ['payment' => $intent];
            
            $updatedOrder = $this->updateOrderPayment($order, $paymentData);
            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function reserveCar(Request $request)
    {
        $validatedData = $request->validate([
            'departure_from' => 'required|string',

            'car_only' => 'required|boolean',
            'selected_car' => 'required|string',
            'car_start_date' => 'required|date_format:Y-m-d',
            'car_end_date' => 'required|date_format:Y-m-d',
            'car_pick_up_at' => 'required|string',
            'car_checkpoint' => 'required|string',

            'contact_person' => 'required',
            'contact_person.email' => 'required|email|confirmed',
            'contact_person.email_confirmation' => 'required',
            'contact_person.phone_number' => 'required',
            'contact_person.first_name' => 'nullable',
            'contact_person.last_name' => 'nullable',

            'passenger' => 'required',
            'passenger.passport_number' => 'nullable',
            'passenger.name' => 'required|string|alpha_spaces',
            'passenger.nationality' => 'required|string',
            'passenger.gender' => 'required|in:0,1,2',
            'passenger.dob' => 'required|date_format:Y-m-d',
            'passenger.passport_expiry' => 'nullable|date_format:Y-m-d',
            'passenger.issuing_country' => 'required|string',
            'passenger.passport_issue' => 'nullable|date_format:Y-m-d',
            'passenger.type' => 'required|in:0,1,2'
        ]);
        try {
            $user = $this->guard()->user();

            DB::beginTransaction();
            $contactPerson = $this->createContactPerson($request->contact_person,  $user);
            $data = [
                'contact_person' => $contactPerson,
                'options' => (object)[
                    'car_only' => $request->car_only,
                    'two_way' => false,
                    'departure_from' => $request->departure_from,
                    'selected_car' => $request->selected_car,
                    'car_start_date' => $request->car_start_date,
                    'car_end_date' => $request->car_end_date,
                    'car_pick_up_at' => $request->car_pick_up_at,
                    'car_checkpoint' => $request->car_checkpoint,
                    'type' => null,
                    'quantity' => 0,
                    'wifi_devices' => 0,
                ],
                'passengers' => [$request->passenger]
            ];
            $order = $this->createOrder($data, $user);

            $paymentDesc = $order->reference_number;
            if ($order->details()->carRental()->first()) {
                $carStartDate = Carbon::parse($data['options']->car_start_date);
                $carEndDate = Carbon::parse($data['options']->car_end_date);
                $totalDays = $carStartDate->diffInDays($carEndDate) + 1;
                $selectedCar = $order->options->selected_car;

                $paymentDesc .= " - Included 1 unit(s) of ";
                $paymentDesc .=  $selectedCar == 'car_5_seater' ? " 5 Seater Car " : " 10 Seater Car ";
                $paymentDesc .= $carStartDate->format('j M Y').' - '.$carEndDate->format('j M Y');
                $paymentDesc .= " | $totalDays day(s)";
            }
            
            $intent = StripePaymentIntent::create([
                'amount' => $order->total_car * 100,
                'currency' => 'sgd',
                'capture_method' => 'manual',
                'setup_future_usage' => 'off_session',
                'description' => $paymentDesc,
                'metadata' => ['order_id' => $order->id]
            ]);

            $paymentData = ['payment' => $intent];
            
            $updatedOrder = $this->updateOrderPayment($order, $paymentData);
            DB::commit();
            return $this->jsonResponse("Success", array('order' => $order));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function cancel(Request $request)
    {
        $validatedData = $request->validate([
            'order_id' => 'required|string',
            'reference_number' => 'required|string',
            'email' => 'nullable|email'
        ]);

        try {
            $user = $this->guard()->user();

            $order = Order::where('id', $request->order_id)->where('reference_number', $request->reference_number)->latest()->first();
            if (!$order) throw new Exception("Order not found!", 400);
            \Log::info('cancelling order');
            DB::beginTransaction();

            // TODO set payment status in merchant's database to 'expire'
            $cancelledOrder = $this->cancelOrder($order, OrderStatus::CANCELLED);
            if ($cancelledOrder instanceof Order) {
                $cancelledOrder->load('contactPerson', 'ownable', 'details.passenger');
            }
            DB::commit();
            \Log::info('success');

            return $this->jsonResponse("Success", array('order' => $cancelledOrder));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }

    }

    // public function midtransHandler(Request $request)
    // {
    //     \Log::info('==========================midtrans request body=================================');
    //     \Log::info($request->all());
    //     $validatedData = $request->validate([
    //         'order_id' => 'required|string',
    //         'signature_key' => 'required|string',
    //         'status_code' => 'required|string',
    //     ]);

    //     try {
    //         $user = $this->guard()->user();

    //         $transaction = $request->transaction_status;
    //         $type = $request->payment_type;
    //         $orderId = $request->order_id;
    //         $fraud = $request->fraud_status;

    //         $order = Order::where('reference_number', $orderId)->latest()->first();
    //         if (!$order) throw new Exception("Order not found!", 400);

    //         // Check status & signature from Midtrans
    //         $verified = $this->verifySignature($order, $request->signature_key, $request->status_code);
    //         if (!$verified) throw new Exception("Signature key not genuine", 400);

    //         DB::beginTransaction();

    //         if ($transaction == 'capture') {
    //             // For credit card transaction, we need to check whether transaction is challenge by FDS or not
    //             if ($type == 'credit_card') {
    //                 if ($fraud == 'challenge') {
    //                     // TODO set payment status in merchant's database to 'Challenge by FDS'
    //                     // TODO merchant should decide whether this transaction is authorized or not in MAP
    //                     // echo "Transaction order_id: " . $orderId ." is challenged by FDS";
    //                 }
    //                 else {
    //                     // TODO set payment status in merchant's database to 'Success'
    //                     $data = [
    //                         'midtrans' => $request->all()
    //                     ];
    //                     $order = $this->processToCheckIn($order, $data);
    //                     $contactPerson = $order->contactPerson;
    //                     if ($contactPerson instanceof ContactPerson) {
    //                         Mail::to($contactPerson->email)->send(new CheckedIn($order->ownable, $order));
    //                     }
    //                 }
    //             }
    //         }
    //         else if ($transaction == 'settlement') {
    //             // TODO set payment status in merchant's database to 'Settlement'
    //             // echo "Transaction order_id: " . $orderId ." successfully transfered using " . $type;
    //         }
    //         else if ($transaction == 'pending') {
    //             // TODO set payment status in merchant's database to 'Pending'
    //             // echo "Waiting customer to finish transaction order_id: " . $orderId . " using " . $type;
    //         }
    //         else if ($transaction == 'deny') {
    //             // TODO set payment status in merchant's database to 'Denied'
    //             // echo "Payment using " . $type . " for transaction order_id: " . $orderId . " is denied.";
    //         }
    //         else if ($transaction == 'expire') {
    //             // TODO set payment status in merchant's database to 'expire'
    //             $this->cancelOrder($order, OrderStatus::EXPIRED);
    //         }
    //         else if ($transaction == 'cancel') {
    //             // TODO set payment status in merchant's database to 'Denied'
    //             // echo "Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.";
    //         }

    //         DB::commit();

    //         return $this->jsonResponse("Success with status " . $transaction, array());
    //     } catch (Exception $e) {
    //         DB::rollBack();
    //         return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
    //     }
    // }

    public function stripeCharge(Request $request)
    {
        $validatedData = $request->validate([
            'order_id' => 'required',
            'token' => 'required|string',
            'email' => 'required|email',
            'card' => 'nullable'
        ]);
        $order = Order::find($request->order_id);
        $ticketCharge = null;

        try {
            $user = $this->guard()->user();

            if (!$order) throw new Exception("Order not found!", 400);

            if (!$order->isActive()) throw new Exception("Order is not active anymore or already completed", 400);

            $customer = Stripe::customers()->create([
                'email' => $request->email,
                'description' => "Customer for " . $request->email,
                'source' => $request->token
            ]);
            
            DB::beginTransaction();
            
            $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
            if ($wifiOnly) {
                $data = [
                    'stripe' => $ticketCharge,
                    'stripe_customer' => $customer,
                ];
                $order = $this->processToCheckIn($order, $data);

                $wifi = $order->details()->wifiRental()->first();
                $wifiCharge = Stripe::charges()->create([
                    'currency' => 'sgd',
                    'description' => $order->reference_number . ' Wifi',
                    'amount'   => $order->total_wifi,
                    'customer'   => $customer['id'],
                    'source' => $customer['default_source'],
                    'capture' => false
                ]);
                
                if ($wifiCharge['failure_code'] || empty($wifiCharge)) {
                    $this->updateWifiStatus($order, OrderDetailStatus::ERROR, $wifiCharge['failure_message']);
                    throw new Exception($wifiCharge['failure_message'], (int)$wifiCharge['failure_code']);
                } else {
                    $wifiRefundableCharge = Stripe::charges()->create([
                        'currency' => 'sgd',
                        'description' => $order->reference_number . ' Wifi refundable',
                        'amount'   => $wifi->options->wifi_refund_price * $wifi->quantity,
                        'customer' => $customer['id'],
                        'source' => $customer['default_source'],
                        'capture' => false
                    ]);
                    
                    if ($wifiRefundableCharge['failure_code'] || empty($wifiRefundableCharge)) {
                        $errorMessage = empty($wifiRefundableCharge) ? "" : isset($wifiRefundableCharge['failure_message']) ?: "";
                        $wifiRefund = Stripe::refunds()->create($wifiCharge['id']);
                        // $wifiRefundableRefund = Stripe::refunds()->create($wifiRefundableCharge['id']);
                        $this->updateWifiStatus($order, OrderDetailStatus::ERROR, $errorMessage);
                        throw new Exception($wifiRefundableCharge['failure_message'], (int)$wifiRefundableCharge['failure_code']);
                    } else {
                        $this->updateWifiStatus($order, OrderDetailStatus::CHECKED_IN);
                        $wifiCapture = Stripe::charges()->capture($wifiCharge['id']);
                    }
                }
            } else {
                $ticketCharge = Stripe::charges()->create([
                    'currency' => 'sgd',
                    'description' => $order->reference_number,
                    'amount'   => $order->total_ticket,
                    'customer'   => $customer['id'],
                    'source' => $customer['default_source'],
                    'capture' => false
                ]);
    
    
                if ($ticketCharge['failure_code']) {
                    throw new Exception($ticketCharge['failure_message'], (int)$ticketCharge['failure_code']);
                }
    
                // Update order to COMPLETE
                if ($ticketCharge['paid'] == true) {
                    // $order->status = OrderStatus::COMPLETED;
                    // $payment = array_merge($ticketCharge, ['type' => 'stripe']);
                    // $order->payment = $payment;
    
                    // if (!$order->save()) {
                    //     throw new Exception("Failed while saving order.", 500);
                    // }
    
                    $data = [
                        'stripe' => $ticketCharge,
                        'stripe_customer' => $customer,
                    ];
                    $order = $this->processToCheckIn($order, $data);
    
                    if (!$ticketCharge['review'] && !$ticketCharge['captured']) {
                        $secondCustomer = Stripe::customers()->find($order->options->stripe_customer->id);
                        $wifi = $order->details()->wifiRental()->first();
                        if ($wifi) {
                            $wifiCharge = null;
                            try {
                                $wifiCharge = Stripe::charges()->create([
                                    'currency' => 'sgd',
                                    'description' => $order->reference_number . ' Wifi',
                                    'amount'   => $order->total_wifi,
                                    'customer'   => $secondCustomer['id'],
                                    'source' => $ticketCharge['source']['id'],
                                    'capture' => false
                                ]);
                            } catch (Exception $e) {
                                \Log::info($e->getMessage());
                            }
                           
                            if ($wifiCharge['failure_code'] || empty($wifiCharge)) {
                                // $wifiRefund = Stripe::refunds()->create($wifiCharge['id']);
                                $this->updateWifiStatus($order, OrderDetailStatus::ERROR, $wifiCharge['failure_message']);
                            } else {
                                $wifiRefundableCharge = null;
                                try {
                                    $wifiRefundableCharge = Stripe::charges()->create([
                                        'currency' => 'sgd',
                                        'description' => $order->reference_number . ' Wifi refundable',
                                        'amount'   => $wifi->options->wifi_refund_price * $wifi->quantity,
                                        'customer' => $secondCustomer['id'],
                                        'source' => $ticketCharge['source']['id'],
                                        'capture' => false
                                    ]);
                                } catch (Exception $e) {
                                    \Log::info($e->getMessage());
                                }
                                
                                if ($wifiRefundableCharge['failure_code'] || empty($wifiRefundableCharge)) {
                                    $errorMessage = empty($wifiRefundableCharge) ? "" : isset($wifiRefundableCharge['failure_message']) ?: "";
                                    $wifiRefund = Stripe::refunds()->create($wifiCharge['id']);
                                    // $wifiRefundableRefund = Stripe::refunds()->create($wifiRefundableCharge['id']);
                                    $this->updateWifiStatus($order, OrderDetailStatus::ERROR, $errorMessage);
                                } else {
                                    $this->updateWifiStatus($order, OrderDetailStatus::CHECKED_IN);
                                    $wifiCapture = Stripe::charges()->capture($wifiCharge['id']);
                                }
                            }
                        }
                        $ticketCapture = Stripe::charges()->capture($ticketCharge['id']);
                    }
                }
            }

            DB::commit();
            $contactPerson = $order->contactPerson;
            if ($contactPerson instanceof ContactPerson) {
                $orderManager = new OrderManager;
                $sendMail = $orderManager->sendMailWithReceipt($order);
            }
            return $this->jsonResponse("Success", array('order' => $order));
        } catch (Exception $e) {
            DB::rollBack();

            $message = $e->getMessage();
            \Log::info($message);
            $order = Order::find($request->order_id);
            if ($order && $order->isActive()) {
                $this->cancelOrder($order, OrderStatus::CANCELLED, $message, $ticketCharge);
            }
            
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    // Use this for 3D secure payment
    public function stripePaymentIntent(Request $request)
    {
        $validatedData = $request->validate([
            'order_id' => 'required',
            'response' => 'required|json',
            'email' => 'required|email',
            'name' => 'nullable|string',
            'card' => 'nullable'
        ]);

        $order = Order::find($request->order_id);
        $ticketCharge = null;

        try {
            $user = $this->guard()->user();

            $response = json_decode($request->response);

            if (!$order) throw new Exception("Order not found!", 400);

            if (!$order->isActive()) throw new Exception("Order is not active anymore or already completed", 400);

            // if (isset($order->options->stripe_customer) && isset($order->options->stripe_customer->id)) {
            //     $customer = StripeCustomer::retrieve($order->options->stripe_customer->id);
            // }

            if (!is_object($response) || !isset($response->paymentIntent) || !isset($response->paymentIntent->payment_method)) {
                throw new Exception("Payment method is required!", 400);

                $paymentMethod = StripePaymentMethod::retrieve($response->paymentIntent->payment_method);
                $paymentMethod->attach(['customer' => $customer->id]);
            }

            $paymentMethod = StripePaymentMethod::retrieve($response->paymentIntent->payment_method);
            if (!$paymentMethod) throw new Exception("Payment method not found!", 400);
            
            if ($paymentMethod->customer != null) {
                $customer = StripeCustomer::retrieve($paymentMethod->customer);
            }

            if (!isset($customer) || !$customer) {
                $customer = StripeCustomer::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'description' => "Customer for " . $request->email,
                    // 'payment_method' => $response->paymentIntent->payment_method
                ]);

                try {
                    $paymentMethod->attach(['customer' => $customer->id]);                    
                } catch (Exception $e) {
                    \Log::error("Attaching payment method to customer failed");
                    \Log::error($e);
                }
            }
            
            DB::beginTransaction();
            
            // $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
            
            // $ticketPaymentIntent = StripePaymentIntent::update($order->payment->id, [
            //     'setup_future_usage' => 'off_session',
            //     'customer' => $customer->id,
            //     'payment_method' => $paymentMethod->id
            // ]);
            // $ticketPaymentIntent = StripePaymentIntent::update($order->payment->id, [
            //     'customer' => $customer->id
            // ]);
            $ticketPaymentIntent = StripePaymentIntent::retrieve($order->payment->id);
            // Update order to COMPLETE
            $data = [
                'stripe_customer' => $customer,
                'charge' => $ticketPaymentIntent['charges']->data[0]['id'],
                'charge_customer' => $ticketPaymentIntent['charges']->data[0]['customer'] ? $ticketPaymentIntent['charges']->data[0]['customer'] : "-"
            ];
            $order = $this->processToCheckIn($order, $data);

            $ticketPaymentIntent->capture();

            if ($ticketPaymentIntent['status'] !== "succeeded") throw new Exception($this->generateStripeErrorMessage($ticketPaymentIntent['status']), 400);

            $wifi = $order->details()->wifiRental()->first();
            if ($wifi) {
                $this->updateWifiStatus($order, OrderDetailStatus::CHECKED_IN);
            }

            $car = $order->details()->carRental()->first();
            if ($car) {
                $this->updateCarStatus($order, OrderDetailStatus::CHECKED_IN);
            }

            DB::commit();
            try {
                $contactPerson = $order->contactPerson;
                if ($contactPerson instanceof ContactPerson) {
                    $orderManager = new OrderManager;
                    $sendMail = $orderManager->sendMailWithReceipt($order);
                }
            } catch (Exception $e) {
                \Log::error("Failed to send receipt");
                \Log::error($e);
            }
            return $this->jsonResponse("Success", array('order' => $order));
        } catch (Exception $e) {
            DB::rollBack();

            $message = $e->getMessage();
            \Log::error("Stripe payment intent error");
            \Log::error($message);
            \Log::error($e);
            $order = Order::find($request->order_id);
            if ($order && $order->isActive()) {
                $this->cancelOrder($order, OrderStatus::CANCELLED, $message, $ticketCharge);
            }
            
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function generateStripeErrorMessage($str) {
        switch ($str) {
            case 'requires_payment_method':
                $msg = "Payment method required!";
                break;
            case 'requires_confirmation':
                $msg = "Confirmation required!";
                break;
            case 'requires_action':
                $msg = "Action required!";
                break;
            case 'processing':
                $msg = "Processing!";
                break;
            case 'requires_capture':
                $msg = "Capture required!";
                break;
            case 'canceled':
                $msg = "Payment cancelled!";
                break;
            default:
                $msg = "Unknown error, please contact administrator!";
                break;
        }
    }

    protected function processToCheckIn(Order $order, $data = [])
    {
        try {
            if (!$order->isActive()) throw new Exception("Order is not active anymore or already completed", 400);

            $order = $this->completeOrder($order, $data);

            return $order;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function createOrder($data = array(), User $user = null)
    {
        $order = new Order;
        $order->reference_number = $this->generateReferenceNumber($data['options']);
        $order->options = $data['options'];
        if ($user) {
            $order->ownable()->associate($user);
        }
        if (isset($data['contact_person'])) {
            $order->contactPerson()->associate($data['contact_person']);
        }

        if (!$order->save()) {
            throw new Exception("Failed while saving order.", 500);
        }

        $twoWay = $order->options->two_way;
        $ticketType = $order->options->type;
        $wifiDevices = $order->options->wifi_devices;
        $selectedCar = $order->options->selected_car;

        // $checkPrice = $this->checkPrice(boolval($order->options->two_way), $data['type']);

        // if (empty((array)$checkPrice)) {
        //     throw new Exception("Something went wrong.", 500);
        // } else {
        //     $price = $checkPrice->ticketPrice;
        // }

        // Create ticket order detail
        for ($i=0; $i < $order->options->quantity; $i++) {
            $passenger = $this->createPassenger($data['passengers'][$i], $user);
            $terminalFee = AppConfig::where('key', 'terminal_fee')->value('value');
            $surcharge = AppConfig::where('key', 'surcharge')->value('value');
            $price = $this->checkLocalPrice(boolval($twoWay), $data['passengers'][$i]['type'], $data['type'], $passenger->nationality, $order->options->departure_from == 'batam' ? 'IDR' : 'SGD');
            $upgradeBusinessPrice = AppConfig::where('key', 'upgrade_business_class')->value('value');
            $d = [
                'passenger' => $passenger,
                'price' => $twoWay ? $price / 2 : $price,
                'terminal_fee' => $terminalFee,
                'surcharge' => $surcharge,
                'upgrade_business' => $ticketType == TicketType::BUSINESS ? $upgradeBusinessPrice : 0,
                'discount' => 0,
                'start_date' => $order->options->departure_date,
                'end_date' => null,
                'expiry_date' => null,
                'type' => $data['passengers'][$i]['type'],
                'status' => OrderDetailStatus::RESERVED,
                'options' => [
                    'schedule_id' => $order->options->departure_schedule_id,
                    'reservation_key' => $order->options->departure_reservation_key,
                    'trip_type' => TripType::DEPARTURE
                ]
            ];
            $this->createOrderDetail($d, $order);

            if ($twoWay) {
                $d['start_date'] = $order->options->return_date;
                $d['options'] = [
                    'schedule_id' => $order->options->return_schedule_id,
                    'reservation_key' => $order->options->return_reservation_key,
                    'trip_type' => TripType::RETURN
                ];
                $this->createOrderDetail($d, $order);
            }
        }

        // TODO: Create car rental and wifi rental order detail
        if ($wifiDevices > 0) {
            $passenger = $this->createPassenger($data['passengers'][0], $user);
            
            if ($order->options->departure_from == 'batam') {
                $wifiPrice = AppConfig::where('key', 'wifi_price_singapore')->value('value');
            } else {
                $wifiPrice = AppConfig::where('key', 'wifi_price_batam')->value('value');
            }
            $wifiRefundPrice = AppConfig::where('key', 'wifi_refundable_deposit')->value('value');
            $numberOfDays = $this->countDaysBetweenDates($order->options->wifi_start_date, $order->options->wifi_end_date);

            $wifiData = [
                'passenger' => $passenger,
                'price' => $numberOfDays * $wifiPrice,
                'terminal_fee' => 0,
                'surcharge' => 0,
                'upgrade_business' => 0,
                'discount' => 0,
                'start_date' => $order->options->wifi_start_date,
                'end_date' => $order->options->wifi_end_date,
                'quantity' => $order->options->wifi_devices,
                'expiry_date' => null,
                'type' => OrderDetailType::WIFI_RENTAL,
                'status' => OrderDetailStatus::RESERVED,
                'options' => [
                    'number_of_days' => $numberOfDays,
                    'wifi_refund_price' => $wifiRefundPrice
                    // 'trip_type' => TripType::DEPARTURE
                ]
            ];

            $this->createOrderDetail($wifiData, $order);
        }

        if ($selectedCar) {
            $passenger = $this->createPassenger($data['passengers'][0], $user);

            $carPrice = AppConfig::where('key', $selectedCar)->value('value');
            if (!$carPrice) throw new Exception("Invalid selected car type.", 400);

            $numberOfDays = $this->countDaysBetweenDates($order->options->car_start_date, $order->options->car_end_date);

            $carData = [
                'passenger' => $passenger,
                'price' => $numberOfDays * $carPrice,
                'terminal_fee' => 0,
                'surcharge' => 0,
                'upgrade_business' => 0,
                'discount' => 0,
                'start_date' => $order->options->car_start_date,
                'end_date' => $order->options->car_end_date,
                'quantity' => 1,
                'expiry_date' => null,
                'type' => OrderDetailType::CAR_RENTAL,
                'status' => OrderDetailStatus::RESERVED,
                'options' => [
                    'number_of_days' => $numberOfDays,
                    'car_pick_up_at' => $order->options->car_pick_up_at,
                ]
            ];

            $this->createOrderDetail($carData, $order);
        }

        // $snapToken = $this->generateSnapToken($order);

        // $options = $order->options;
        // $options->snap_token = $snapToken;

        // $order->options = $options;
        $order->status = OrderStatus::WAITING_FOR_PAYMENT;

        if (!$order->save()) {
            throw new Exception("Failed while saving order.", 500);
        }

        return $order;
    }

    protected function updateOrderPayment(Order $order, $data = [])
    {
        if (isset($data['payment'])) $order->payment = $data['payment'];
        if (isset($data['wifi_payment'])) $order->wifi_payment = $data['wifi_payment'];
        if (isset($data['wifi_refundable_payment'])) $order->wifi_refundable_payment = $data['wifi_refundable_payment'];
        if (isset($data['charge'])) $order->charge = $data['charge'];
        if (isset($data['charge_customer'])) $order->charge_customer = $data['charge_customer'];
        // $order->status = OrderStatus::WAITING_FOR_PAYMENT;

        if (!$order->save()) {
            throw new Exception("Failed while saving order.", 500);
        }

        return $order;
    }

    protected function completeOrder(Order $order, $data = [])
    {
        $order->charge = $data['charge'];
        $order->charge_customer = $data['charge_customer'];
        $departureCheckInResult = null;
        $returnCheckInResult = null;
        try {
            $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
            $carOnly = isset($order->options->car_only) && $order->options->car_only;
            if ($wifiOnly || $carOnly) {
                $departureCheckInResult = (object) [
                    "bookingReference" => "",
                ];
    
                if ($order->options->two_way) {
                    $returnCheckInResult = (object) [
                        "bookingReference" => "",
                    ];
                }
            } else {
                // Check-in to Horizon API
                $departureCheckInResult = $this->multipleCheckIn($order, TripType::DEPARTURE, $order->options->departure_from);
                \Log::info('oi Depart');
                \Log::info($departureCheckInResult->bookingReference);
                \Log::info(json_encode($departureCheckInResult));
    
                $this->checkInOrderDetails($order, TripType::DEPARTURE, $departureCheckInResult);
    
                if ($order->options->two_way) {
                    $returnCheckInResult = $this->multipleCheckIn($order, TripType::RETURN, $order->options->return_from);
                    \Log::info('oi Return');
                    \Log::info($returnCheckInResult->bookingReference);
                    \Log::info(json_encode($returnCheckInResult));
                    
                    $this->checkInOrderDetails($order, TripType::RETURN, $returnCheckInResult);
                }
            }

            // checkin single
            // if (!$departureDetails->isEmpty()) {
            //     $orderDetails->each(function($detail) use ($order) {
            //         $passenger = $detail->passenger;
            //         if ($detail instanceof OrderDetail && $passenger instanceof Passenger) {
            //             $checkIn = $this->checkIn($order, $detail, $passenger);

            //             if (!$checkIn->errorNo) {
            //                 $data = [
            //                     'boarding_pass_number' => $checkIn->boardingpassNo,
            //                     'vip_key' => $checkIn->VIPKey
            //                 ];
            //                 $this->checkedIn[] = $this->completeOrderDetail($detail, $data);
            //             } else {
            //                 \Log::info('=====================checkin error==============================');
            //                 \Log::info(json_encode($checkIn));
            //                 // {"boardingpassNo":"","errorNo":-1,"errorDesc":"This passenger already checked-in at 16:40 ! Can not check-in more than 1 time in the same day."}
            //                 $this->checkInErrors[] = $checkIn;
            //             }
            //         }
            //     });
            // }

            // Update order to COMPLETE
            $order->options = array_merge((array) $order->options, ['departure_booking_reference' => $departureCheckInResult->bookingReference]);
            if ($order->options->two_way && $returnCheckInResult) {
                $order->options = array_merge((array) $order->options, ['return_booking_reference' => $returnCheckInResult->bookingReference]);
            }
            $order->status = OrderStatus::COMPLETED;

            // Update midtrans data to order / use array_merge?
            // if (isset($data['midtrans'])) {
            //     $payment = array_merge($data['midtrans'], ['type' => 'midtrans']);
            //     $order->payment = $payment;
            // }
            // Update stripe data to order / use array_merge?
            if (isset($data['stripe'])) {
                $payment = array_merge($data['stripe'], ['type' => 'stripe']);
                $order->payment = $payment;
            }
            // Update stripe data to order / use array_merge?
            if (isset($data['stripe_customer'])) {
                $order->options = array_merge((array) $order->options, ['stripe_customer' => $data['stripe_customer']]);
            }

            if (!$order->save()) {
                throw new Exception("Failed while saving order.", 500);
            }

            return $order;
        } catch (Exception $e) {
            // Something to do with $checkIn
            \Log::info('completeOrderFailed with error:');
            \Log::info($e->getMessage());
            if (!empty($departureCheckInResult)) {
                $this->cancelMultipleCheckIn($order, TripType::DEPARTURE, $order->options->departure_from, $departureCheckInResult);
            }
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function cancelOrder(Order $order, $status = OrderStatus::CANCELLED, $message = "", $charge = null)
    {
        try {
            if (!$order->isActive()) throw new Exception("Order is not active anymore!", 400);

            $orderDetails = $order->details()->ticket()->reserved()->get();
            if (!$orderDetails->isEmpty()) {
                $orderDetails->each(function($detail) use ($order) {
                    if ($detail instanceof OrderDetail) {
                        $checkIn = $this->cancelOrderDetail($detail);
                    }
                });
            }

            $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
            if (!$wifiOnly) {
                $this->cancelReservation($order->options->departure_schedule_id, $order->options->departure_reservation_key);
            }

            // if ($order->options->two_way) {
            //     $this->cancelReservation($order->options->return_schedule_id, $order->options->return_reservation_key);
            // }

            $order->status = $status;
            $order->options = array_merge((array) $order->options, ['cancel_reason' => $message]);

            if (!empty($charge) && isset($charge['id'])) {
                $refund = Stripe::refunds()->create($charge['id']);
                $updatedCharge = Stripe::charges()->find($refund['charge']);
                $order->payment = array_merge($updatedCharge, ['type' => 'stripe']);
            }

            if (!$order->save()) {
                throw new Exception("Failed while saving order.", 500);
            }
            
            try {
                if (isset($order->payment->id)) {
                    $paymentIntent = StripePaymentIntent::retrieve($order->payment->id);
                    $paymentIntent->cancel();
                }
                \Log::error("Order payment intent id ".$order->payment->id);
            } catch (Exception $e) {
                \Log::error("Failed cancelling stripe payment intent");    
                \Log::error($e->getMessage());    
            }

            return $order;
        } catch (Exception $e) {
            \Log::error("Failed cancelling order");
            \Log::error($e->getMessage());
            throw new Exception($e->getMessage(), $e->getCode());
        }

    }

    protected function createOrderDetail($data = array(), Order $order)
    {
        $orderDetail = new OrderDetail;
        $orderDetail->price = $data['price'];
        $orderDetail->terminal_fee = $data['terminal_fee'];
        $orderDetail->surcharge = $data['surcharge'];
        $orderDetail->upgrade_business = $data['upgrade_business'];
        $orderDetail->discount = $data['discount'];
        $orderDetail->start_date = $data['start_date'];
        $orderDetail->end_date = $data['end_date'];
        $orderDetail->expiry_date = $data['expiry_date'];
        if (isset($data['quantity'])) {
            $orderDetail->quantity = $data['quantity'];
        }
        $orderDetail->type = $data['type'];
        if (isset($data['status'])) {
            $orderDetail->status = $data['status'];
        }
        $orderDetail->options = $data['options'];
        $orderDetail->passenger()->associate($data['passenger']);
        $orderDetail->order()->associate($order);

        if (!$orderDetail->save()) {
            throw new Exception("Failed while saving order detail.", 500);
        }

        return $orderDetail;
    }

    protected function checkInOrderDetails(Order $order, $tripType, $result)
    {
        \Log::info('masukCheckinOrderDetails 1281');
        if ($tripType == TripType::DEPARTURE) {
            $details = $order->details()->ticket()->departure()->reserved()->with('passenger')->get();
            \Log::info('Masuk Depart');
            \Log::info($details);
        } else if ($tripType == TripType::RETURN) {
            $details = $order->details()->ticket()->return()->reserved()->with('passenger')->get();
            \Log::info('Masuk Return');
            \Log::info($details);
        } else {
            \Log::error('checkInOrderDetails error - $tripType invalid.');
            throw new Exception("Something went wrong, please check error logs.", 500);
        }

        if (!isset($result->boardingPassNoList) || !isset($result->boardingPassNoList->boardingPassNo) || !is_array($result->boardingPassNoList->boardingPassNo)) {
            \Log::error('checkInOrderDetails error - $result->boardingPassNo invalid');
            throw new Exception("Something went wrong, please check error logs.", 500);
        }

        foreach ($result->boardingPassNoList->boardingPassNo as $r) {
            $detail = $details->filter(function($value, $key) use ($r) {
                return strtoupper($value->passenger->passport_number) == strtoupper($r->passportNo);
            })->first();

            \Log::info('filter passport no');
            \Log::info($r->passportNo);
            \Log::info($detail);
            \Log::info($r->boardingPassNo);

            if ($detail instanceof OrderDetail) {
                if ($r->checkInStatus != 0) {
                    // New
                    \Log::info('orderCon1313');
                    \Log::info($r->checkInStatus);


                    throw new Exception($r->description, 400);
                    $detail->status = OrderDetailStatus::ERROR;
                    $detail->options = array_merge((array) $detail->options, ['error_description' => $r->description]);
                } else {
                    \Log::info('orderCon1317');
                    \Log::info($r->boardingPassNo);
                    $detail->status = OrderDetailStatus::CHECKED_IN;
                    $detail->options = array_merge((array) $detail->options, ['boarding_pass_number' => $r->boardingPassNo]);
                    $detail->options = array_merge((array) $detail->options, ['e_ticket_number' => $r->eTicketNo]);
                    $detail->options = array_merge((array) $detail->options, ['e_boarding_pass' => $r->eBoardingPass]);
                }

                if (!$detail->save()) {
                    \Log::error('checkInOrderDetails error - failed to save order detail with id ' . $detail->id);
                    throw new Exception("Failed while saving order detail.", 500);
                }
            }
        }

        return true;
    }

    protected function updateWifiStatus(Order $order, $status, $message = null)
    {
        $wifi = $order->details()->wifiRental()->first();
        if (!$wifi) {
            \Log::error('Error while updating wifi status, wifi not found!');
            throw new Exception("Something went wrong, please contact admin.", 500);
        }
        $wifi->status = $status;
        if ($message) {
            $wifi->options = array_merge((array) $wifi->options, ['message' => $message]);
        }
        if (!$wifi->save()) {
            \Log::error('updateWifiStatus error - failed to save order detail with id ' . $wifi->id);
            throw new Exception("Failed while saving order detail.", 500);
        }

        return $order;
    }

    protected function updateCarStatus(Order $order, $status, $message = null)
    {
        $car = $order->details()->carRental()->first();
        if (!$car) {
            \Log::error('Error while updating car status, car not found!');
            throw new Exception("Something went wrong, please contact admin.", 500);
        }
        $car->status = $status;
        if ($message) {
            $car->options = array_merge((array) $car->options, ['message' => $message]);
        }
        if (!$car->save()) {
            \Log::error('updateCarStatus error - failed to save order detail with id ' . $car->id);
            throw new Exception("Failed while saving order detail.", 500);
        }

        $this->rentCar($order);

        return $order;
    }

    protected function completeOrderDetail(OrderDetail $orderDetail, $data = [])
    {
        $orderDetail->status = OrderDetailStatus::CHECKED_IN;
        $options = $orderDetail->options;
        $options->boarding_pass_number = isset($data['boarding_pass_number']) ? $data['boarding_pass_number'] : null;
        $options->vip_key = $data['vip_key'] ? $data['vip_key'] : null;

        $orderDetail->options = $options;

        if (!$orderDetail->save()) {
            throw new Exception("Failed while saving order detail.", 500);
        }

        return $orderDetail;
    }

    protected function cancelOrderDetail(OrderDetail $orderDetail, $data = [])
    {
        $orderDetail->status = OrderDetailStatus::RELEASED;

        if (!$orderDetail->save()) {
            throw new Exception("Failed while saving order detail.", 500);
        }

        if ($orderDetail->isTicket()) {
            // $this->cancelReservation($orderDetail->options->schedule_id, $orderDetail->options->reservation_key);
        }

        return $orderDetail;
    }

    protected function createContactPerson($data = [], User $user = null)
    {
        if ($user) {
            if (isset($data['id'])) {
                $contactPerson = $user->contactPersons()->updateOrCreate(['id' => $data['id']], $data);
            } else {
                $contactPerson = $user->contactPersons()->create($data);
            }
        } else {
            if (isset($data['id'])) {
                $contactPerson = ContactPerson::updateOrCreate(['id' => $data['id']], $data);
            } else {
                $contactPerson = ContactPerson::create($data);
            }
        }

        return $contactPerson;
    }

    protected function createPassenger($data = [], User $user = null)
    {
        if ($user) {
            $passenger = $user->passengers()->updateOrCreate(['passport_number' => $data['passport_number']], $data);

            // if (isset($data['id'])) {
            //     $passenger = $user->passengers()->updateOrCreate(['id' => $data['id']], $data);
            // } else {
            //     $passenger = $user->passengers()->create($data);
            // }
        } else {
            $passenger = Passenger::updateOrCreate(['passport_number' => $data['passport_number']], $data);

            // if (isset($data['id'])) {
            //     $passenger = Passenger::updateOrCreate(['id' => $data['id']], $data);
            // } else {
            //     $passenger = Passenger::create($data);
            // }
        }

        return $passenger;
    }

    protected function checkSeatAvailability($from = 'batam', $scheduleDate = '', $scheduleId = '', $type = TicketType::ECONOMY, $quantity = 1)
    {
        try {
            if ($from == 'batam') {
                $result = $this->batamCheckSeatAvailability($scheduleDate, $scheduleId, $type, $quantity);
            } else if ($from == 'singapore') {
                $result = $this->singaporeCheckSeatAvailability($scheduleDate, $scheduleId, $type, $quantity);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function batamCheckSeatAvailability($scheduleDate, $scheduleId, $type, $quantity) {
        $options = [
            'location' => $this->url,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE
        ];

        $client = new SoapClient($this->url . "?wsdl", $options);

        $res = $client->BATAM_2_getSeatAvailability([
            '_scheduleDate' => $scheduleDate,
            '_scheduleID' => $scheduleId,
            '_seatType' => TicketType::getCode($type),
            '_seatQty' => $quantity,
            '_userName' => $this->username,
            '_userPassword' => $this->password
        ]);

        $result = !empty((array)$res) ? $res->BATAM_2_getSeatAvailabilityResult : $res;

        if (isset($result->errorNo) && $result->errorNo != 0) {
            throw new Exception($result->errorDesc, 400);
        }

        return $result;
    }

    protected function singaporeCheckSeatAvailability($scheduleDate, $scheduleId, $type, $quantity) {
        $options = [
            'location' => $this->url,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE
        ];

        $client = new SoapClient($this->url . "?wsdl", $options);

        $res = $client->SINGAPORE_2_getSeatAvailability([
            '_scheduleDate' => $scheduleDate,
            '_scheduleID' => $scheduleId,
            '_seatType' => TicketType::getCode($type),
            '_seatQty' => $quantity,
            '_userName' => $this->username,
            '_userPassword' => $this->password
        ]);

        $result = !empty((array)$res) ? $res->SINGAPORE_2_getSeatAvailabilityResult : $res;

        if (isset($result->errorNo) && $result->errorNo != 0) {
            throw new Exception($result->errorDesc, 400);
        }

        return $result;
    }

    protected function multipleCheckIn(Order $order, $tripType = TripType::DEPARTURE, $from = 'batam')
    {
        try {
            if ($tripType == TripType::DEPARTURE) {
                $scheduleId = $order->options->departure_schedule_id;
                $scheduleDate = Carbon::createFromFormat('Y-m-d', $order->options->departure_date)->format('Ymd');
                $reservationKey = $order->options->departure_reservation_key;
                $details = $order->details()->ticket()->departure()->reserved()->get();
            } else if ($tripType == TripType::RETURN) {
                $scheduleId = $order->options->return_schedule_id;
                $scheduleDate = Carbon::createFromFormat('Y-m-d', $order->options->return_date)->format('Ymd');
                $reservationKey = $order->options->return_reservation_key;
                $details = $order->details()->ticket()->return()->reserved()->get();
            } else {
                \Log::error('multipleCheckIn error - $tripType invalid.');
                throw new Exception("Something went wrong, please check error logs.", 500);
            }

            $passengerJson = $this->generatePassengerJson($details);

            if (!$passengerJson) throw new Exception("Something went wrong, please try again later.", 400);

            \Log::info('wtf');
            \Log::info($passengerJson);

            if ($from == 'batam') {
                $result = $this->batamMultipleCheckIn($order, $scheduleId, $scheduleDate, $reservationKey, $passengerJson);
            } else if ($from == 'singapore') {
                $result = $this->singaporeMultipleCheckIn($order, $scheduleId, $scheduleDate, $reservationKey, $passengerJson);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function batamMultipleCheckIn(Order $order, $scheduleId, $scheduleDate, $reservationKey, $passengerJson = '') {
        try {
            \Log::info('batamMultipleCheckIn');

            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->BATAM_6_newCheckInData_Multiple([
                '_scheduleDate' => $scheduleDate,
                '_paxDetailsJSON' => $passengerJson,
                '_scheduleID' => $scheduleId,
                '_seatType' => TicketType::getCode($order->options->type),
                // '_VIPKey' => '',
                '_reservationKey' => $reservationKey,
                '_bookingReference' => $order->reference_number,
                '_userName' => $this->username,
                '_userPassword' => $this->password,
                '_departureFrom' => $order->options->departure_from,
                '_twoWay' => $order->options->two_way,
                '_chargeID' => $order->charge,
                '_customerID' => $order->charge_customer
            ]);
            \Log::info("========================================");
            \Log::info($order->charge);
            \Log::info($order->charge_customer);
            \Log::info($scheduleDate);
            \Log::info($scheduleId);
            \Log::info($passengerJson);
            \Log::info(TicketType::getCode($order->options->type));
            \Log::info($reservationKey);
            \Log::info($order->reference_number);
            \Log::info($this->username);
            \Log::info($this->password);

            $result = !empty((array)$res) ? $res->BATAM_6_newCheckInData_MultipleResult : $res;

            if (isset($result->errorNo) && $result->errorNo != 0) {
                throw new Exception($result->errorDesc, 400);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function singaporeMultipleCheckIn(Order $order, $scheduleId, $scheduleDate, $reservationKey, $passengerJson = '') {
        try {
            \Log::info('singaporeMultipleCheckIn');

            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->SINGAPORE_4_newCheckInData_Multiple([
                '_scheduleDate' => $scheduleDate,
                '_paxDetailsJSON' => $passengerJson,
                '_scheduleID' => $scheduleId,
                '_seatType' => TicketType::getCode($order->options->type),
                // '_VIPKey' => '',
                '_reservationKey' => $reservationKey,
                '_bookingReference' => $order->reference_number,
                '_userName' => $this->username,
                '_userPassword' => $this->password,
                '_departureFrom' => $order->options->departure_from,
                '_twoWay' => $order->options->two_way,
                '_chargeID' => $order->charge,
                '_customerID' => $order->charge_customer
            ]);
            \Log::info("====================SG================");
            \Log::info($order->charge);
            \Log::info($order->charge_customer);

            \Log::info('singapore multiple');
            \Log::info($scheduleDate);
            \Log::info($scheduleId);
            \Log::info($passengerJson);
            \Log::info(TicketType::getCode($order->options->type));
            \Log::info($reservationKey);
            \Log::info($order->reference_number);
            \Log::info($this->username);
            \Log::info($this->password);

            $result = !empty((array)$res) ? $res->SINGAPORE_4_newCheckInData_MultipleResult : $res;

            if (isset($result->errorNo) && $result->errorNo != 0) {
                throw new Exception($result->errorDesc, 400);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function checkIn(Order $order = null, OrderDetail $orderDetail, Passenger $passenger)
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $passengerName1 = '';
            $passengerName2 = '';
            $passengerName3 = '';

            if (strlen($passenger->name) > 15) {
                if (strpos($passenger->name, ' ')) {
                    $names = explode(' ', $passenger->name);
                    $passengerName1 = $names[0];
                    if (isset($name[1])) {
                        $passengerName2 = $names[1];
                    }
                    if (isset($name[2])) {
                        $passengerName3 = $names[2];
                    }
                } else {
                    // Split name?
                }
            } else {
                $passengerName1 = $passenger->name;
            }

            $res = $client->newCheckInData([
                '_passportNo' => $passenger->passport_number,
                '_passengerName1' => $passengerName1,
                '_passengerName2' => $passengerName2,
                '_passengerName3' => $passengerName3,
                '_dateOfBirth' => $passenger->dob ? $passenger->dob->format('Ymd') : '',
                '_placeOfBirth' => $passenger->issuing_country,
                '_gender' => Gender::getCode($passenger->gender),
                '_nationalCode' => 'ID',
                '_placeOfIssue' => $passenger->issuing_country,
                '_dateOfIssue' => $passenger->passport_expiry ? $passenger->passport_expiry->subYears(5)->format('Ymd') : Carbon::now()->format('Ymd'),
                '_expiryDate' => $passenger->passport_expiry ? $passenger->passport_expiry->format('Ymd') : Carbon::now()->format('Ymd'),
                '_scheduleID' => $orderDetail->options->schedule_id,
                '_seatType' => TicketType::getCode($order->options->type),
                '_VIPKey' => '',
                '_reservationKey' => $orderDetail->options->reservation_key,
                '_bookingReference' => $orderDetail->order->reference_number,
                '_userName' => $this->username,
                '_userPassword' => $this->password,
                '_chargeID' => '-',
                '_customerID' => '-'
            ]);

            $result = !empty((array)$res) ? $res->newCheckInDataResult : $res;

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function cancelMultipleCheckIn(Order $order, $tripType = TripType::DEPARTURE, $from = 'batam', $r)
    {
        try {
            $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
            if ($wifiOnly) {
                return true;
            }

            if ($tripType == TripType::DEPARTURE) {
                $scheduleId = $order->options->departure_schedule_id;
                $bookingReference = $r->bookingReference;
                // $details = $order->details()->ticket()->departure()->reserved()->get();
            } else if ($tripType == TripType::RETURN) {
                $scheduleId = $order->options->return_schedule_id;
                $bookingReference = $r->bookingReference;
                // $details = $order->details()->ticket()->return()->reserved()->get();
            } else {
                \Log::error('cancelMultipleCheckIn error - $tripType invalid.');
                throw new Exception("Something went wrong, please check error logs.", 500);
            }

            $boardingPassJson = $this->generateBoardingPassJson($r->boardingPassNoList->boardingPassNo);

            if (!$boardingPassJson) throw new Exception("Something went wrong, please try again later.", 400);

            \Log::info('cancelling');
            \Log::info($bookingReference);

            if ($from == 'batam') {
                $result = $this->batamCancelMultipleCheckIn($order, $scheduleId, $bookingReference, $boardingPassJson);
            } else if ($from == 'singapore') {
                $result = $this->singaporeCancelMultipleCheckIn($order, $scheduleId, $bookingReference, $boardingPassJson);
            }
            \Log::info('cancel with result');
            \Log::info(json_encode($result));

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function batamCancelMultipleCheckIn(Order $order, $scheduleId, $bookingReference = '', $boardingPassJson = '')
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->BATAM_7_cancelCheckInData_Multiple([
                '_scheduleID' => $scheduleId,
                '_transactionReference' => $bookingReference,
                '_boardingPassNoJSON' => $boardingPassJson,
                '_userName' => $this->username,
                '_userPassword' => $this->password
            ]);

            $result = !empty((array)$res) ? $res->BATAM_7_cancelCheckInData_MultipleResult : $res;

            if (isset($result->errorNo) && $result->errorNo != 0) {
                \Log::info('batamCancelMultipleCheckIn error');
                \Log::info($result->errorDesc);
                throw new Exception($result->errorDesc, 400);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function singaporeCancelMultipleCheckIn(Order $order, $scheduleId, $bookingReference = '', $boardingPassJson = '')
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'features' => SOAP_SINGLE_ELEMENT_ARRAYS
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->SINGAPORE_5_cancelCheckInData_Multiple([
                '_scheduleID' => $scheduleId,
                '_transactionReference' => $bookingReference,
                '_boardingPassNoJSON' => $boardingPassJson,
                '_userName' => $this->username,
                '_userPassword' => $this->password
            ]);

            $result = !empty((array)$res) ? $res->SINGAPORE_5_cancelCheckInData_MultipleResult : $res;

            if (isset($result->errorNo) && $result->errorNo != 0) {
                \Log::info('singaporeCancelMultipleCheckIn error');
                \Log::info($result->errorDesc);
                throw new Exception($result->errorDesc, 400);
            }

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function cancelReservation($scheduleId = '', $reservationKey = '')
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->BATAM_3_releaseSeats([
                '_scheduleID' => $scheduleId,
                '_reservationKey' => $reservationKey,
                '_userName' => $this->username,
                '_userPassword' => $this->password
            ]);

            $result = !empty((array)$res) ? $res->BATAM_3_releaseSeatsResult : $res;
            \Log::info('seat released with response');
            \Log::info(json_encode($result));
            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function checkLocalPrice($twoWay = false, $ageType = 0, $type = TicketType::ECONOMY, $nationality = "ID", $preferredCurrency = 'IDR')
    {
        $price = null;
        $key = null;
        if ($twoWay) {
            if ($ageType == 0) {
                if ($nationality == "ID") {
                    if ($preferredCurrency == 'IDR') {
                        $key = 'btm_sg_adult_local_two_ways';
                    } else if ($preferredCurrency == 'SGD') {
                        $key = 'sg_btm_adult_two_ways';
                    }
                } else {
                    if ($preferredCurrency == 'IDR') {
                        $key = 'btm_sg_adult_foreign_two_ways';
                    } else if ($preferredCurrency == 'SGD') {
                        $key = 'sg_btm_adult_two_ways';
                    }
                }
            } else if ($ageType == 1) {
                if ($preferredCurrency == 'IDR') {
                    $key = 'btm_sg_infant_two_ways';
                } else if ($preferredCurrency == 'SGD') {
                    $key = 'sg_btm_infant_two_ways';
                }
            }
        } else {
            if ($ageType == 0) {
                if ($nationality == "ID") {
                    if ($preferredCurrency == 'IDR') {
                        $key = 'btm_sg_adult_local_one_way';
                    } else if ($preferredCurrency == 'SGD') {
                        $key = 'sg_btm_adult_one_way';
                    }
                } else {
                    if ($preferredCurrency == 'IDR') {
                        $key = 'btm_sg_adult_foreign_one_way';
                    } else if ($preferredCurrency == 'SGD') {
                        $key = 'sg_btm_adult_one_way';
                    }
                }
            } else if ($ageType == 1) {
                if ($preferredCurrency == 'IDR') {
                    $key = 'btm_sg_infant_one_way';
                } else if ($preferredCurrency == 'SGD') {
                    $key = 'sg_btm_infant_one_way';
                }
            }
        }

        if (!$key) {
            \Log::info('invalid config key');
            throw new Exception("Something went wrong, please try again later.", 500);
        }

        $price = AppConfig::where('key', $key)->value('value');

        return $price;
    }

    protected function checkPrice($twoWay = false, $type = TicketType::ECONOMY)
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->getTicketPrice([
                '_oneWayEconomy' => $twoWay ? 0 : 1,
                '_twoWayEconomy' => $twoWay ? 1 : 0,
                '_upgradeBusiness' => $type == TicketType::BUSINESS ? 1 : 0,
                '_upgradeVIP' => $type == TicketType::VIP ? 1 : 0,
                '_userName' => $this->username,
                '_userPassword' => $this->password
            ]);

            $result = !empty((array)$res) ? $res->getTicketPriceResult : $res;

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function generatePassengerJson($details = \Illuminate\Database\Eloquent\Collection)
    {
        $arr = [];

        if ($details->isEmpty()) return false;

        $details->each(function($detail) use (&$arr) {
            $passenger = $detail->passenger;
      
            $passengerName1 = '';
            $passengerName2 = '';
            $passengerName3 = '';

            if (strlen($passenger->name) > 15) {
                if (strpos($passenger->name, ' ')) {
                    $names = explode(' ', $passenger->name, 3);
                    $passengerName1 = $names[0];
                    if (isset($names[1])) {
                        $passengerName2 = $names[1];
                    }
                    if (isset($names[2])) {
                        $passengerName3 = $names[2];
                    }
                } else {
                    // Split name?
                }
            } else {
                $passengerName1 = $passenger->name;
            }


            $arr[] = (object) [
                'passportNo' => $passenger->passport_number,
                'passengerName1' => $passengerName1,
                'passengerName2' => $passengerName2,
                'passengerName3' => $passengerName3,
                'dateOfBirth' => $passenger->dob ? $passenger->dob->format('Ymd') : '',
                'placeOfBirth' => $passenger->issuing_country,
                'gender' => Gender::getCode($passenger->gender),
                'nationalCode' => $passenger->nationality,
                'placeOfIssue' => $passenger->issuing_country,
                'dateOfIssue' => $passenger->passport_issue ? $passenger->passport_issue->format('Ymd') : Carbon::now()->format('Ymd'),
                'expiryDate' => $passenger->passport_expiry ? $passenger->passport_expiry->format('Ymd') : Carbon::now()->format('Ymd'),
                'paxType' => PassengerType::getString($passenger->type),
            ];
        });
        $json = json_encode($arr);

        return $json;
    }

    protected function generateBoardingPassJson($list = [])
    {
        $arr = [];

        foreach ($list as $l) {
            $arr[] = (object) [
                'BoardingPass' => $l->boardingPassNo
            ];
        }

        $json = json_encode($arr);

        return $json;
    }

    protected function checkPassportExpiry($departureDateString, $passengers = [])
    {
        $departureDate = Carbon::createFromFormat('Y-m-d', $departureDateString);
        foreach ($passengers as $p) {
            $passportExpiry = Carbon::createFromFormat('Y-m-d', $p['passport_expiry']);
            // \Log::info($departureDate->diffInMonths($passportExpiry, false));
            if ($departureDate->diffInMonths($passportExpiry, false) < 6) {
                throw new Exception("Passport Expiry date must be at-least 6 months from date of travel.", 400);
            }
        }

        return true;
    }

    protected function generateSnapToken(Order $order)
    {
        $customerDetails = [];
        $itemDetails = [];
        $transactionDetails = [
            'order_id' => $order->reference_number,
            'gross_amount' => $order->total
        ];

        $contactPerson = $order->contactPerson;

        if ($contactPerson) {
            $customerDetails = [
                'first_name' => $contactPerson->first_name,
                'last_name' => $contactPerson->last_name,
                'email' => $contactPerson->email,
                'phone' => $contactPerson->phone_number
            ];
        }


        $customExpiry = [
            'start_time' => date("Y-m-d H:i:s O", time()),
            'unit' => 'day',
            'duration' => 2
        ];

        $order->details->each(function($detail) {
            $itemDetails[] = [
                'id' => 'OD-' . $detail->id,
                'quantity' => $detail->quantity,
                'name' => OrderDetailType::getString($detail->type),
                'price' => $detail->total
            ];
        });

        // Send this options if you use 3Ds in credit card request
        $creditCardOption = [
            'secure' => true,
            'channel' => 'migs'
        ];

        $transactionData = [
            'transaction_details' => $transactionDetails,
            'item_details' => $itemDetails,
            'customer_details' => $customerDetails,
            // 'expiry' => $customExpiry,
            // 'credit_card' => $creditCardOption,
        ];

        try {
            $token = Midtrans::getSnapToken($transactionData);
            return $token;
        } catch (Exception $e) {
            \Log::info($e->getMessage());
            $code = is_numeric($e->getCode()) ? $e->getCode() : 500;
            // Note: Change error message
            throw new Exception($e->getMessage(), $code);
        }

    }

    protected function rentCar(Order $order) {
        $carDetail = $order->details()->typeTicket(OrderDetailType::CAR_RENTAL)->first();
        $rentalName = $order->contactPerson->fullname;
        if($rentalName == '') {
            $rentalName = $order->details[0]->passenger->name;
        }
        $detail = (object) [
            'nama' => $rentalName,
            'hp' => $order->contactPerson->phone_number,
            'email' => $order->contactPerson->email
        ];

        $rentStart = Carbon::parse($order->options->car_start_date);
        $numberOfDays = $this->countDaysBetweenDates($order->options->car_start_date, $order->options->car_end_date);
        $data = [
            'form_params' => [
                'token_key' => '8jCEIiemzc8t6cdq44Dflcqo4p3EbfHfl8JhxF5ul4wv9ljc',
                'booking_code' => $order->reference_number,
                'detail' => json_encode($detail),
                'rent_start' => $rentStart->format('Ymd'),
                'rent_duration' => $numberOfDays,
                'seat' => $order->options->selected_car == 'car_5_seater' ? 5 : 10,
                'price' => $carDetail ? $carDetail->total : 0,
                'checkpoint' => $order->options->car_checkpoint,
                'timepoint' => $order->options->car_pick_up_at, 
            ]
        ];
        \Log::info($data);
        try {
            $request = $this->client->post('http://103.246.0.8:9876/api_one/cbp_rent/rent_order/reg_rent.php', $data);

            $body = $request->getBody();
            \Log::info('cbp_rent response body');
            \Log::info($body);

            // Success = {"code":"200","note":"Success"}
            // Failed = {"code":"500","note":"Error"}
            // Bad Token / Unregistered Token = {"code":"401","note":"Bad Token"}
            // Bad Request = {"code":"440","note":"Bad Request"}
        } catch (Exception $e) {
            \Log::error('cbp_rent error');
            \Log::error($e->getMessage());
        }

        return $order;
    }

    // protected function verifySignature(Order $order, $signature, $statusCode)
    // {
    //     try {
    //         $orderId = $order->reference_number;
    //         $grossAmount = number_format($order->total, 2, ".", "");
    //         $serverKey = config('midtrans.server_key');
    //         $input = $orderId.$statusCode.$grossAmount.$serverKey;

    //         $localSignature = openssl_digest($input, 'sha512');

    //         return $signature == $localSignature;
    //     } catch (Exception $e) {
    //         throw new Exception($e->getMessage(), $code);
    //     }
    // }

    protected function generateReferenceNumber($options)
    {
        if (isset($options->wifi_only) && $options->wifi_only) {
            if (isset($options->departure_from) && $options->departure_from == 'batam') {
                $code = 'WS';
            } else if (isset($options->departure_from) && $options->departure_from == 'singapore') {
                $code = 'WB';
            } else {
                $code = 'W';
            }
            $wifi = true;
        } else if (isset($options->car_only) && $options->car_only) {
            $code = 'CR';
        } else if (isset($options->departure_from) && $options->departure_from == 'batam') {
            $code = 'B';
        } else if (isset($options->departure_from) && $options->departure_from == 'singapore') {
            $code = 'H';
        }

        $todayDate = date('ymd');

        $reference = $code.$todayDate;
        $minLength = 3;
        
        $query = Order::query();
        if (isset($wifi) && $wifi) {
            // $latest = $query->where('reference_number', 'like', 'W%')->whereDate('created_at', Carbon::today())->latest()->value('reference_number');
            $latest = $query->select('reference_number', DB::raw('RIGHT(reference_number, 9) AS sub_reference_number'))->where('reference_number', 'like', '%'.$todayDate.'%')->orderBy('sub_reference_number', 'desc')->value('reference_number');
        } else {
            $latest = $query->select('reference_number', DB::raw('RIGHT(reference_number, 9) AS sub_reference_number'))->where('reference_number', 'like', '%'.$todayDate.'%')->orderBy('sub_reference_number', 'desc')->value('reference_number');
            // $latest = $query->where('reference_number', 'not like', 'W%')->whereDate('created_at', Carbon::today())->latest()->value('reference_number');
        }

        if (!$latest) {
            $referenceNumber = $reference.str_pad('1', $minLength, "0", STR_PAD_LEFT);
        } else {
            $number = (int) substr($latest, -1 * $minLength);
            $number++;
            if (strlen((string) $number) > $minLength) {
                $minLength = strlen((string) $number);
            }
            $referenceNumber = $reference.str_pad($number, $minLength, "0", STR_PAD_LEFT);
        }

        return $referenceNumber;
    }

    protected function countDaysBetweenDates($startDate = null, $endDate = null)
    {
        $wifiStartDate = $startDate ? new DateTime($startDate) : null;
        $wifiEndDate = $endDate ? new DateTime($endDate) : null;

        if ($wifiStartDate && $wifiStartDate) {
            $wifiStartDate->modify('-1 day');
            $numberOfDays = (int) $wifiEndDate->diff($wifiStartDate)->format("%a");
        } else {
            $numberOfDays = 1;
        }

        return $numberOfDays;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}
