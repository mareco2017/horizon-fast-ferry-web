<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use App\Models\Config as AppConfig;
use Exception;
use GuzzleHttp\Client;
use Mail;
use App\Mail\Enquiry;

class ContactUsController extends Controller
{
    use ApiResponse;

    public function send(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'enquiry' => 'required',
            'token' => 'required',
        ]);

        try {
            $client = new Client();
            
            $response = $client->post(
                'https://www.google.com/recaptcha/api/siteverify',
                [
                    'form_params'=> [
                        'secret' => config('social.google.recaptcha_secret'),
                        'response' => $request->token
                    ]
                ]
            );
            $body = json_decode((string)$response->getBody());
            if (!$body->success) throw new Exception('Something went wrong, please try again!', 400);

            $fromEmail = 'noreply@horizonfastferry.com';
            $fromName = $request->name;
            $enquiry = $request->email.' - '.$request->enquiry;

            Mail::to(AppConfig::where('key', 'hff_contact_us')->value('value'))
                ->cc('hffbatam@gmail.com')
                ->send(new Enquiry($fromEmail, $fromName, $enquiry));
            return $this->jsonResponse("Success");
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }
}