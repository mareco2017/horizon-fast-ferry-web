<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Models\ContactPerson;

class ContactPersonController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    public function getOwnedContactPersons(Request $request)
    {
        try {
            $loggedInUser = $this->guard()->user();

            $contactPersons = $loggedInUser->contactPersons;
            return $this->jsonResponse("Success", ['contact_persons' => $contactPersons]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'nullable'
        ]);

        try {
            $query = ContactPerson::query();
            $query->where('email', $request->email);

            $contactPerson = $query->first();

            return $this->jsonResponse("Success", ['contact_person' => $contactPerson]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|confirmed',
            'email_confirmation' => 'required',
            'phone_number' => 'required',
            'first_name' => 'nullable',
            'last_name' => 'nullable'
        ]);

        try {
            DB::beginTransaction();

            $contactPerson = $this->create($request->all(), $this->guard()->user());

            DB::commit();

            return $this->jsonResponse("Successfully created!", ['contact_person' => $contactPerson]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function create($data = [], User $user)
    {
        if ($user) {
            return $user->contactPersons()->create($data);
        } else {
            return Passenger::create($data);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}