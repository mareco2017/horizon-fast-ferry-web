<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Models\Token;

class UserController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me(Request $request)
    {
        return $this->jsonResponse("Success", ['user' => $this->guard()->user()]);
    }

    public function updateProfile(Request $request)
    {
        $user = $this->guard()->user();

        $validatedData = $request->validate([
            'phone_number' => 'required|unique:users,phone_number,' . $user->id,
            'first_name' => 'required|string|alpha_spaces',
            'last_name' => 'nullable|string|alpha_spaces',
            'gender' => 'nullable',
            'dob' => 'nullable|date_format:Y-m-d'
        ]);

        try {
            DB::beginTransaction();
            
            $user->phone_number = $request->phone_number;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->gender = $request->gender;
            $user->dob = $request->dob;

            if (!$user->save()) {
                throw new Exception("Failed while updating profile!", 500);
            }

            DB::commit();
            return $this->jsonResponse("Successfully updated profile!", array('user' => $user));
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function changePassword(Request $request)
    {
        $validatedData = $request->validate([
            'password' => 'required',
            'new_password' => 'required|min:6',
        ]);

        try {
            $user = $this->guard()->user();

            if (!Hash::check($request->password, $user->password)) {
                throw new Exception("Wrong password!", 400);
            }
            
            DB::beginTransaction();
            
            $user->password = bcrypt($request->new_password);

            if (!$user->save()) {
                throw new Exception("Failed while changing password!", 500);
            }

            DB::commit();

            // note: need logout?

            return $this->jsonResponse("Successfully change password!");
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}