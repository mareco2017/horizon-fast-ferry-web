<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use GuzzleHttp\Client;
use SoapClient;

class ScheduleController extends Controller
{
    use ApiResponse;

    protected $url;
    protected $username;
    protected $password;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->url = 'http://202.154.185.170:6300/API_HorizonFastFerryBatam/HFFBatam.asmx';
        $this->url = 'http://43.225.185.198:6300/API_HorizonFastFerryBatam/HFFBatam.asmx';
        $this->username = config('horizon.username');
        $this->password = config('horizon.password');
        $options = [
            'location' => $this->url,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS
        ];

        $this->client = new SoapClient($this->url . "?wsdl", $options);
    }

    public function daily(Request $request)
    {
        $validatedData = $request->validate([
            'date' => 'required|date_format:Ymd',
            'type' => 'required|string',
            'from' => 'required|string'
        ]);

        try {
            
            if ($request->from == 'batam') {
                $result = $this->batamSchedule($request->date, $request->type);
            } else {
                $result = $this->singaporeSchedule($request->date, $request->type);
            }

            return $this->jsonResponse("Success", $result->tripDetails);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function batamSchedule($date, $seatType = "")
    {
        $res = $this->client->BATAM_1_getDailyScheduleList([
            '_date' => $date,
            '_seatType' => $seatType,
            '_userName' => $this->username,
            '_userPassword' => $this->password
        ]);

        if (!empty((array)$res)) {
            $result = $res->BATAM_1_getDailyScheduleListResult;
            if (isset($result->errorNo) && $result->errorNo != 0) {
                throw new Exception($result->errorDesc, 400);
            }
        } else {
            $result = $res;
        }

        return $result;
    }

    protected function singaporeSchedule($date, $seatType = "")
    {
        $res = $this->client->SINGAPORE_1_getDailyScheduleList([
            '_date' => $date,
            '_seatType' => $seatType,
            '_userName' => $this->username,
            '_userPassword' => $this->password
        ]);

        if (!empty((array)$res)) {
            $result = $res->SINGAPORE_1_getDailyScheduleListResult;
            if (isset($result->errorNo) && $result->errorNo != 0) {
                throw new Exception($result->errorDesc, 400);
            }
        } else {
            $result = $res;
        }

        return $result;
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}
