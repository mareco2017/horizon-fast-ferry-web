<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Models\Config as AppConfig;
use App\Enums\TicketType;
use SoapClient;

class TicketController extends Controller
{
    use ApiResponse;

    protected $url;
    protected $username;
    protected $password;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->url = 'http://43.225.185.198:6300/API_HorizonFastFerryBatam/HFFBatam.asmx';
        $this->username = config('horizon.username');
        $this->password = config('horizon.password');
    }

    public function price(Request $request)
    {
        $validatedData = $request->validate([
            'two_way' => 'required|boolean',
            'type' => 'nullable|numeric|in:0,1,2'
        ]);

        try {
            if ($request->two_way) {
                $result = [
                    'local_infant' => AppConfig::where('key', 'btm_sg_infant_two_ways')->value('value'),
                    'local_adult' => AppConfig::where('key', 'btm_sg_adult_local_two_ways')->value('value'),
                    'foreign_adult' => AppConfig::where('key', 'btm_sg_adult_foreign_two_ways')->value('value'),
                    'infant_sg' => AppConfig::where('key', 'sg_btm_infant_two_ways')->value('value'),
                    'adult_sg' => AppConfig::where('key', 'sg_btm_adult_two_ways')->value('value'),
                    'terminal_fee' => AppConfig::where('key', 'terminal_fee')->value('value'),
                    'surcharge' => AppConfig::where('key', 'surcharge')->value('value'),
                    'upgrade_business_class' => AppConfig::where('key', 'upgrade_business_class')->value('value'),
                    'wifi_price_batam' => AppConfig::where('key', 'wifi_price_batam')->value('value'),
                    'wifi_price_sg' => AppConfig::where('key', 'wifi_price_singapore')->value('value'),
                    'wifi_refundable_deposit' => AppConfig::where('key', 'wifi_refundable_deposit')->value('value'),
                    'car_5_seater' => AppConfig::where('key', 'car_5_seater')->value('value'),
                    'car_10_seater' => AppConfig::where('key', 'car_10_seater')->value('value'),
                    'wifi_enabled' => AppConfig::where('key', 'wifi_enabled')->value('value'),
                ];
            } else {
                $result = [
                    'local_infant' => AppConfig::where('key', 'btm_sg_infant_one_way')->value('value'),
                    'local_adult' => AppConfig::where('key', 'btm_sg_adult_local_one_way')->value('value'),
                    'foreign_adult' => AppConfig::where('key', 'btm_sg_adult_foreign_one_way')->value('value'),
                    'infant_sg' => AppConfig::where('key', 'sg_btm_infant_one_way')->value('value'),
                    'adult_sg' => AppConfig::where('key', 'sg_btm_adult_one_way')->value('value'),
                    'terminal_fee' => AppConfig::where('key', 'terminal_fee')->value('value'),
                    'surcharge' => AppConfig::where('key', 'surcharge')->value('value'),
                    'upgrade_business_class' => AppConfig::where('key', 'upgrade_business_class')->value('value'),
                    'wifi_price_batam' => AppConfig::where('key', 'wifi_price_batam')->value('value'),
                    'wifi_price_sg' => AppConfig::where('key', 'wifi_price_singapore')->value('value'),
                    'wifi_refundable_deposit' => AppConfig::where('key', 'wifi_refundable_deposit')->value('value'),
                    'car_5_seater' => AppConfig::where('key', 'car_5_seater')->value('value'),
                    'car_10_seater' => AppConfig::where('key', 'car_10_seater')->value('value'),
                    'wifi_enabled' => AppConfig::where('key', 'wifi_enabled')->value('value'),
                ];
            }

            // $result = $this->checkPrice($request->two_way, $request->type);

            return $this->jsonResponse("Success", $result);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function wifiPrice()
    {
        try {
            $result = [
                'wifi_price_batam' => AppConfig::where('key', 'wifi_price_batam')->value('value'),
                'wifi_price_sg' => AppConfig::where('key', 'wifi_price_singapore')->value('value'),
                'wifi_refundable_deposit' => AppConfig::where('key', 'wifi_refundable_deposit')->value('value'),
            ];
            return $this->jsonResponse("Success", $result);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function checkPrice($twoWay = false, $type = TicketType::ECONOMY)
    {
        try {
            $options = [
                'location' => $this->url,
                'soap_version' => SOAP_1_1,
                'exceptions' => true,
                'trace' => 1,
                'cache_wsdl' => WSDL_CACHE_NONE
            ];

            $client = new SoapClient($this->url . "?wsdl", $options);

            $res = $client->getTicketPrice([
                '_oneWayEconomy' => $twoWay ? 0 : 1,
                '_twoWayEconomy' => $twoWay ? 1 : 0,
                '_upgradeBusiness' => $type == TicketType::BUSINESS ? 1 : 0,
                '_upgradeVIP' => $type == TicketType::VIP ? 1 : 0,
                '_userName' => $this->username,
                '_userPassword' => $this->password
            ]);

            $result = !empty((array)$res) ? $res->getTicketPriceResult : $res;

            return $result;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}
