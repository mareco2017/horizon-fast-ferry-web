<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use DB;
use Exception;
use App\Models\User;
use App\Models\Passenger;

class PassengerController extends Controller
{
    use ApiResponse;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        // 
    }

    public function getOwnedPassengers(Request $request)
    {
        try {
            $loggedInUser = $this->guard()->user();

            $passengers = $loggedInUser->availablePassengers;
            return $this->jsonResponse("Success", ['passengers' => $passengers]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function search(Request $request)
    {
        $validatedData = $request->validate([
            'passport_number' => 'nullable'
        ]);

        try {
            $query = Passenger::query();
            $query->where('passport_number', $request->passport_number);

            $passenger = $query->first();

            return $this->jsonResponse("Success", ['passenger' => $passenger]);
        } catch (Exception $e) {
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'passport_number' => 'required',
            'name' => 'required|string|alpha_spaces',
            'nationality' => 'required|string',
            'gender' => 'required|in:0,1,2',
            'dob' => 'required|date_format:Y-m-d',
            'passport_expiry' => 'required|date_format:Y-m-d',
            'issuing_country' => 'required|string',
            'type' => 'required|in:0,1,2'
        ]);

        try {
            DB::beginTransaction();

            $passenger = $this->create($request->all(), $this->guard()->user());

            DB::commit();

            return $this->jsonResponse("Successfully created!", ['passenger' => $passenger]);
        } catch (Exception $e) {
            DB::rollBack();
            return $this->jsonResponse($e->getMessage(), array(), $e->getCode());
        }
    }

    protected function create($data = [], User $user)
    {
        if ($user) {
            return $user->passengers()->create($data);
        } else {
            return Passenger::create($data);
        }
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('api-user');
    }
}