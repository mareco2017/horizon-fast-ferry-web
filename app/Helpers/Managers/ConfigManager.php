<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Config as AppConfig;

class ConfigManager
{
    public function update(AppConfig $config, $data)
    {
        $config->key = $data['key'];
        $config->value = $data['value'];
        if (!$config->save()) {
            throw new Exception('Failed to update config!', 500);
        }

        return $config;
    }

    public function delete(AppConfig $config)
    {
        $config->delete();
        return $config;
    }
}
