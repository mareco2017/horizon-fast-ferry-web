<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Admin;
use App\Models\User;
use App\Models\Promo;
use App\Helpers\Managers\AttachmentManager;

class PromoManager
{
    protected $disk = 'public';

    public function update(Promo $promo, $data)
    {
        $promo->status = isset($data['status']) ? $data['status'] : 0;
        $promo->start_date = $data['start_date'];
        $promo->end_date = $data['end_date'];
        if (!$promo->save()) {
            throw new Exception('Failed to update promo!', 500);
        }

        $this->updateCover($promo, $data);

        return $promo;
    }

    public function updateCover($promo, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $promo->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->create($data['cover']);
            $promo->cover()->associate($attachment);

            if (!$promo->save()) {
                throw new Exception('Failed to update cover promo!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(Promo $promo)
    {
        $attachmentManager = new AttachmentManager($this->disk);
        if ($promo->cover) {
            $attachment = $attachmentManager->delete($promo->cover);
        }
        $promo->delete();
    }
}
