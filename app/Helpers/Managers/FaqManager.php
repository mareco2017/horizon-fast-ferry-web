<?php

namespace App\Helpers\Managers;

use DB;
use Exception;
use Carbon\Carbon;
use App\Models\Faq;

class FaqManager
{
    public function update(Faq $faq, $data)
    {
        $faq->question = $data['question'];
        $faq->answer = $data['answer'];
        $faq->status = $data['status'];

        if (!$faq->save()) throw new Exception('Failed to save FAQ!', 500);
        return $faq;
    }

    public function updateValues(array $values, array $nullifiers) 
    {
        if (count($values) > 0) {
            $table = Faq::getModel()->getTable();
    
            $cases = [];
            $ids = [];
            $params = [];
    
            foreach ($values as $value => $id) {
                $id = (int) $id;
                $cases[] = "WHEN {$id} then ?";
                $params[] = $value;
                $ids[] = $id;
            }
    
            $ids = implode(',', $ids);
            $cases = implode(' ', $cases);
            $params[] = Carbon::now();
    
            \DB::update("UPDATE `{$table}` SET `urut` = CASE `id` {$cases} END, `updated_at` = ? WHERE `id` in ({$ids})", $params);
        }

        $bool = Faq::whereIn('id', $nullifiers)->update([
            'urut' => null
        ]);

        return $bool;
    }

    public function delete(Faq $faq)
    {
        $faq->delete();
        return $faq;
    }
}