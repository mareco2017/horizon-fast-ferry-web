<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Passenger;

class PassengerManager
{
    public function update(Passenger $passenger, $data = [])
    {
        $passenger = $passenger->update($data);

        return $passenger;
    }

    public function delete(Passenger $passenger)
    {
        $passenger->delete();
        return $passenger;
    }
}
