<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Comment;
use App\Models\Article;

class CommentManager
{
    public function update(Comment $comment, $data, Article $article)
    {
        $comment->name = $data['name'];
        $comment->email = $data['email'];
        $comment->desc = $data['desc'];
        if (array_key_exists('status', $data)) $comment->status = $data['status'];
        $comment->article()->associate($article);

        if (!$comment->save()) {
            throw new Exception('Failed to update comment!', 500);
        }

        return $comment;
    }

    public function delete(Comment $comment)
    {
        $comment->delete();
    }
}
