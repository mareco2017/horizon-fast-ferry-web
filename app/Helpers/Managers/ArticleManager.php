<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\Admin;
use App\Models\Article;
use App\Helpers\Managers\AttachmentManager;

class ArticleManager
{
    protected $disk = 'public';
    protected $admin;

    public function __construct(Admin $admin = null)
    {
        $this->admin = $admin;
    }

    public function update(Article $article, $data)
    {
        $article->title = $data['title'];
        $article->desc = $data['desc'];
        $article->intro = $data['intro'];
        $article->status = $data['status'];
        $article->tags = $data['tags'];
        if (!$article->id) $article->slug = $this->createSlug($data['title']);
        $article->publisher()->associate($this->admin);

        if (!$article->save()) {
            throw new Exception('Failed to update article!', 500);
        }

        $this->updateCover($article, $data);

        return $article;
    }

    public function createSlug($title, $id = 0)
    {
        // Normalize the title
        $slug = str_slug($title);
        // Get any that could possibly be related.
        // This cuts the queries down by doing it once.
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }
        // Just append numbers like a savage until we find not used.
        for ($i = 1; $i <= 10; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains('slug', $newSlug)) {
                return $newSlug;
            }
        }
        throw new Exception('Can not create a unique slug url');
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Article::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }

    public function updateCover($article, $data)
    {
        if (array_key_exists('cover', $data) && isset($data['cover'])) {
            $oldCover = $article->cover;

            $attachmentManager = new AttachmentManager($this->disk);
            $attachment = $attachmentManager->create($data['cover']);
            $article->cover()->associate($attachment);

            if (!$article->save()) {
                throw new Exception('Failed to update cover article!', 500);
            }

            if ($oldCover) $attachmentManager->delete($oldCover);
        }
    }

    public function delete(Article $article)
    {
        $attachmentManager = new AttachmentManager($this->disk);
        if ($article->cover) {
            $attachment = $attachmentManager->delete($article->cover);
        }
        $article->delete();
    }
}
