<?php

namespace App\Helpers\Managers;

use Exception;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\Config as AppConfig;
use App\Enums\OrderStatus;
use App\Enums\OrderDetailStatus;
use App\Enums\PassengerType;
use App\Enums\TicketType;
use SoapClient;
use PDF;
use Mail;
use App\Mail\CheckedIn;
use Carbon\Carbon;

class OrderManager
{
    protected $url;
    protected $username;
    protected $password;

    public function __construct()
    {
        $this->url = 'http://43.225.185.198:6300/API_HorizonFastFerryBatam/HFFBatam.asmx';
        $this->username = config('horizon.username');
        $this->password = config('horizon.password');
    }

    public function update(Order $order, $data)
    {
        $dTickets = $order->details()->ticket()->departure()->get();
        $rTickets = $order->details()->ticket()->return()->get();

        foreach ($data['boarding_pass_depart'] as $key => $boardingKey) {
            $detail = $dTickets[$key];
            $this->inputData($detail, $boardingKey);
        }
        
        if ($order->options->two_way && isset($data['boarding_pass_return'])) {
            foreach ($data['boarding_pass_return'] as $key => $boardingKey) {
                $detail = $rTickets[$key];
                $this->inputData($detail, $boardingKey);
            }
        }

        $order->options = array_merge((array) $order->options, ['departure_booking_reference' => '']);
        if ($order->options->two_way) {
            $order->options = array_merge((array) $order->options, ['return_booking_reference' => '']);
        }
        $order->status = OrderStatus::COMPLETED;

        if (!$order->save()) {
            throw new Exception("Failed while saving order.", 500);
        }
        return $order;
    }

    public function inputData(OrderDetail $detail, $boardingKey)
    {
        $detail->status = OrderDetailStatus::CHECKED_IN;
        $detail->options = array_merge((array) $detail->options, ['boarding_pass_number' => $boardingKey]);

        if (!$detail->save()) {
            \Log::error('checkInOrderDetails error - failed to save order detail with id ' . $detail->id);
            throw new Exception("Failed while saving order detail.", 500);
        }
    }

    public function cancelOrder(Order $order, $status = OrderStatus::CANCELLED)
    {
        if (!$order->isActive()) throw new Exception("Order is not active anymore!", 400);

        $orderDetails = $order->details()->ticket()->reserved()->get();
        if (!$orderDetails->isEmpty()) {
            $orderDetails->each(function($detail) use ($order) {
                if ($detail instanceof OrderDetail) {
                    $checkIn = $this->cancelOrderDetail($detail);
                }
            });
        }

        if (isset($order->options->departure_schedule_id) && isset($order->options->departure_reservation_key)) $this->cancelReservation($order->options->departure_schedule_id, $order->options->departure_reservation_key);

        $order->status = $status;

        if (!$order->save()) {
            throw new Exception("Failed while saving order.", 500);
        }

        return $order;
    }

    protected function cancelOrderDetail(OrderDetail $orderDetail, $data = [])
    {
        $orderDetail->status = OrderDetailStatus::RELEASED;

        if (!$orderDetail->save()) {
            throw new Exception("Failed while saving order detail.", 500);
        }

        return $orderDetail;
    }

    protected function cancelReservation($scheduleId = '', $reservationKey = '')
    {
        $options = [
            'location' => $this->url,
            'soap_version' => SOAP_1_1,
            'exceptions' => true,
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE
        ];

        $client = new SoapClient($this->url . "?wsdl", $options);

        $res = $client->BATAM_3_releaseSeats([
            '_scheduleID' => $scheduleId,
            '_reservationKey' => $reservationKey,
            '_userName' => $this->username,
            '_userPassword' => $this->password
        ]);

        $result = !empty((array)$res) ? $res->BATAM_3_releaseSeatsResult : $res;
        return $result;
    }

    public function sendMailWithReceipt(Order $order)
    {
        $dTickets = $order->details()->ticket()->departure()->get();
        $rTickets = $order->details()->ticket()->return()->get();
        $wifiRental = $order->details()->wifiRental()->first();
        $carRental = $order->details()->carRental()->first();
        $contactPerson = $order->contactPerson;
        $twoWay = $order->options->two_way;

        $adultLocalTicket = [];
        $adultForeignTicket = [];
        $infantLocalTicket = [];
        $infantForeignTicket = [];
        $eBoardingPass = [];

        foreach ($dTickets as $key => $ticket) {
            if ($order->options->departure_from == 'singapore') {
                if ($ticket->passenger->type == PassengerType::ADULT) {
                    array_push($adultForeignTicket, $ticket);
                } else if ($ticket->passenger->type == PassengerType::INFANT) {
                    array_push($infantForeignTicket, $ticket);
                }
            } else {
                if ($ticket->passenger->nationality == 'ID' && $ticket->passenger->type == PassengerType::ADULT) {
                    array_push($adultLocalTicket, $ticket);
                } else if ($ticket->passenger->nationality != 'ID' && $ticket->passenger->type == PassengerType::ADULT) {
                    array_push($adultForeignTicket, $ticket);
                } else if ($ticket->passenger->nationality == 'ID' && $ticket->passenger->type == PassengerType::INFANT) {
                    array_push($infantLocalTicket, $ticket);
                } else if ($ticket->passenger->nationality != 'ID' && $ticket->passenger->type == PassengerType::INFANT) {
                    array_push($infantForeignTicket, $ticket);
                }
            }

            $departureEBPObject = isset($ticket->options->e_boarding_pass) ? $ticket->options->e_boarding_pass : null;
            $dEBP = ($departureEBPObject && ($departureEBPObject->eBPStatus == "True" || $departureEBPObject->eBPStatus == "true" || $departureEBPObject->eBPStatus === true)) ? (object) [
                'from' => $order->options->departure_from,
                'date' => Carbon::parse($order->options->departure_date)->format('d-M-Y'),
                'time' => $order->options->departure_time,
                'seatClass' => TicketType::getString($order->options->type),
                'eBP' => $departureEBPObject,
                'ticketNo' => isset($ticket->options->e_ticket_number) ? $ticket->options->e_ticket_number : '-',
                'bpNo' => $ticket->options->boarding_pass_number,
            ] : null;

            $returnEBPObject = $twoWay && isset($rTickets[$key]->options->e_boarding_pass) ? $rTickets[$key]->options->e_boarding_pass : null;
            $rEBP = ($returnEBPObject && ($returnEBPObject->eBPStatus == "True" || $returnEBPObject->eBPStatus == "true" || $returnEBPObject->eBPStatus === true)) ? (object) [
                'from' => $order->options->return_from,
                'date' => Carbon::parse($order->options->return_date)->format('d-M-Y'),
                'time' => $order->options->return_time,
                'seatClass' => TicketType::getString($order->options->type),
                'eBP' => $returnEBPObject,
                'ticketNo' => isset($rTickets[$key]->options->e_ticket_number) ? $rTickets[$key]->options->e_ticket_number : '-',
                'bpNo' => $rTickets[$key]->options->boarding_pass_number,
            ] : null;

            $filename = 'E-BP';
            if (isset($dEBP) && $dEBP) {
                $filename = $dEBP->eBP->passengerName.' ('.$dEBP->eBP->passportNo.') E-BP';
            } else if (isset($rEBP) && $rEBP) {
                $filename = $rEBP->eBP->passengerName.' ('.$rEBP->eBP->passportNo.') E-BP';
            }

            if ($dEBP || $rEBP) {
                $eBPpdf = PDF::loadView('templates.pdf.e-boarding-pass', [
                    'refNumber' => $order->reference_number,
                    'dEBP' => $dEBP,
                    'rEBP' => $rEBP,
                    'twoWay' => $twoWay
                ])->setPaper($twoWay ? 'a4' : 'a5', $twoWay ? 'landscape' : 'portrait');
    
                $boardingPass = (object) [
                    'filename' => strtoupper($filename),
                    'pdf' => base64_encode($eBPpdf->output())
                ];
                array_push($eBoardingPass, $boardingPass);
                \Log::info("E-BP PDF generated");
                \Log::info("dEBP: ".isset($dEBP));
                \Log::info("rEBP: ".isset($rEBP));
            }
        }

        $pdf = PDF::loadView('templates.pdf.invoice', [
            'order' => $order,
            'dTickets' => $dTickets,
            'rTickets' => $rTickets,
            'adultLocalTicket' => $adultLocalTicket,
            'adultForeignTicket' => $adultForeignTicket,
            'infantLocalTicket' => $infantLocalTicket,
            'infantForeignTicket' => $infantForeignTicket,
            'wifiRental' => $wifiRental,
            'carRental' => $carRental,
            'contactPerson' => $contactPerson,
        ]);
        \Log::info("Receipt PDF generated");

        $list = [
            AppConfig::where('key','hff_contact_us')->value('value'),
            'batam@horizonfastferry.com',
            'hff@horizonfastferry.com',
            'hffbatam@gmail.com'
        ];
        
        if ($wifiRental) $list[] = AppConfig::where('key','wifi_rental_email')->value('value') ?? 'wifi@horizonfastferry.com';
        if ($carRental) $list[] = AppConfig::where('key','car_rental_email')->value('value') ?? 'carcharter@horizonfastferry.com';

        \Log::info("Log before sending mail code called");
        Mail::to($contactPerson->email)
        ->bcc($list)
        ->send(new CheckedIn($order->ownable, $order, $pdf->output(), $eBoardingPass));
        \Log::info("Log after sending mail code called");

        return;
    }
}
