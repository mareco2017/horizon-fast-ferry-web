<?php

namespace App\Helpers\Enums;

final class VerificationStatus {

	const PENDING = 0;
	const VERIFIED = 1;
	const DECLINED = 2;

	public static function getList() {
		return [
			VerificationStatus::PENDING,
			VerificationStatus::VERIFIED,
			VerificationStatus::DECLINED
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
				// return __('enums/messages.pending');
			case 1:
				return "Verified";
				// return __('enums/messages.verified');
			case 2:
				return "Declined";
				// return __('enums/messages.declined');
		}
	}

}

?>
