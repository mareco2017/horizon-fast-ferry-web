<?php

namespace App\Helpers\Enums;

final class ActiveStatus {

	const INACTIVE = 0;
	const ACTIVE = 1;

	public static function getList() {
		return [
			ActiveStatus::INACTIVE,
			ActiveStatus::ACTIVE
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return 'Inactive';
			case 1:
				return 'Active';
		}
	}

}

?>
