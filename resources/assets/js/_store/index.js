import Vue from 'vue';
import Vuex from 'vuex';

import { account } from './account.module';
import { loading } from './loading.module';

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        account,
        loading
    }
});