const state = {
    isLoading: false,
};

const actions = {
    setLoading({ dispatch, commit }, { act }) {
        // console.log('module', act);
        commit('setLoadingState', act);
        return new Promise((resolve, reject) => {
            resolve(true);
        });
        
    },
};

const mutations = {
    setLoadingState(state, act) {
        state.isLoading = act;
    },
};

export const loading = {
    namespaced: true,
    state,
    actions,
    mutations
};