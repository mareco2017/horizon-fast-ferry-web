import API from '../api';

const token = localStorage.getItem('token');
const state = {
    isLoggedIn: false,
    isLoggingIn: false,
    isRefreshing: false,
    isReloading: false,
    user: null,
    token: token
};

const actions = {
    login({ dispatch, commit }, { username, password }) {
        let data = {
            'username': username,
            'password': password
        };

        commit('loginRequest');
        return new Promise((resolve, reject) => {
            window.axios.post(API.login, data)
                .then((response)  =>  {
                    let res = response.data;
                    commit('loginSuccess', res.data.user, res.data.token);
                    let token = res.data.token;
                    dispatch('setToken', { token });
                    resolve(res);
                }, (error) => {
                    let response = error.response;
                    commit('loginFailure');
                    // dispatch('alert/error', error, { root: true });
                    reject(error);
                });
        });
        
    },
    register({ dispatch, commit }, { data }) {
        commit('loginRequest');
        return new Promise((resolve, reject) => {
            window.axios.post(API.quickRegister, data)
                .then((response)  =>  {
                    let res = response.data;
                    commit('loginSuccess', res.data.user, res.data.token);
                    let token = res.data.token;
                    dispatch('setToken', { token });
                    resolve(res);
                }, (error) => {
                    let response = error.response;
                    commit('loginFailure');
                    reject(error);
                });
        });
        
    },
    updateProfile({ dispatch, commit }, { data }) {
        return new Promise((resolve, reject) => {
            window.axios.post(API.updateProfile, data)
                .then((response)  =>  {
                    let res = response.data;
                    commit('updateUser', res.data.user);
                    resolve(res);
                }, (error) => {
                    let response = error.response;
                    reject(error);
                });
        });
        
    },
    socialLogin({ dispatch, commit }, { type, accessToken }) {
        let data = {
            'access_token': accessToken
        };

        commit('loginRequest');
        return new Promise((resolve, reject) => {
            window.axios.post(API.socialLogin + '/' + type, data)
                .then((response)  =>  {
                    let res = response.data;
                    commit('loginSuccess', res.data.user, res.data.token);
                    let token = res.data.token;
                    dispatch('setToken', { token });
                    resolve(res);
                }, (error) => {
                    let response = error.response;
                    commit('loginFailure');
                    // dispatch('alert/error', error, { root: true });
                    reject(error);
                });
        });
    },
    refresh({ dispatch, commit }) {
        commit('refreshRequest');
        return new Promise((resolve, reject) => {
            window.axios.post(API.refresh)
                .then((response)  =>  {
                    let res = response.data;
                    commit('refreshSuccess', res.data.user, res.data.token);
                    let token = res.data.token;
                    dispatch('setToken', { token });
                    resolve(res);
                }, (error) => {
                    commit('refreshFailure');
                    dispatch('removeToken');
                    reject(error);
                });
        });
        
    },
    reload({ dispatch, commit }, { token }) {
        dispatch('setToken', { token });
        commit('reloadRequest');
        return new Promise((resolve, reject) => {
            window.axios.get(API.userProfile)
                .then((response)  =>  {
                    let res = response.data;
                    commit('reloadSuccess', res.data.user, token);
                    dispatch('setToken', { token });
                    resolve(res);
                }, (error) => {
                    commit('reloadFailure');
                    reject(error);
                });
        });
        
    },
    logout({ dispatch, commit }) {
        return new Promise((resolve, reject) => {
            window.axios.post(API.logout)
                .then((response)  =>  {
                    let res = response.data;
                    commit('logout');
                    dispatch('removeToken');
                    resolve(res);
                }, (error) => {
                    let response = error.response;
                    reject(error);
                });
        });
    },
    setToken({ dispatch, commit }, { token }) {
        localStorage.setItem('token', token);
        commit('updateToken', token);
        let bearerToken = token ? "Bearer " + token : "";
        window.axios.defaults.headers.common['Authorization'] = bearerToken;
    },
    removeToken({ dispatch, commit }) {
        localStorage.removeItem('token');
        commit('updateToken', "");
        let bearerToken = "Bearer ";
        window.axios.defaults.headers.common['Authorization'] = bearerToken;
    }
};

const mutations = {
    loginRequest(state) {
        state.isLoggingIn = true;
    },
    loginSuccess(state, user, token) {
        state.isLoggingIn = false;
        state.isLoggedIn = true;
        state.user = user;
    },
    loginFailure(state) {
        state.isLoggingIn = false;
        state.user = null;
    },
    refreshRequest(state) {
        state.isRefreshing = true;
    },
    refreshSuccess(state, user, token) {
        state.isRefreshing = false;
        state.isLoggedIn = true;
        state.user = user;
    },
    refreshFailure(state) {
        state.isRefreshing = false;
        state.isLoggedIn = false;
        state.user = null;
    },
    reloadRequest(state) {
        state.isReloading = true;
    },
    reloadSuccess(state, user, token) {
        state.isReloading = false;
        state.isLoggedIn = true;
        state.user = user;
    },
    reloadFailure(state) {
        state.isReloading = false;
        state.isLoggedIn = false;
        state.user = null;
    },
    logout(state) {
        state.isLoggedIn = false;
        state.user = null;
    },
    updateToken(state, token) {
        state.token = token;
    },
    updateUser(state, user) {
        state.user = user;
    }
};

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
};