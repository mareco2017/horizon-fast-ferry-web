import { Model } from 'vue-mc'
import { required } from 'vue-mc/validation'

export default class CustomDate extends Model {

    defaults() {
        return {
            date: '',
            month: '',
            year: '',
        }
    }

    options() {
        return {
            // useFirstErrorOnly: true,
            validateOnChange: true,
        }
    }

    validation() {
        return {
            date: required,
            month: required,
            year: required,
        }
    }

    mutations() {
        return {
            // date: Number,
            // month: Number,
            // year: Number,
        }
    }
}