import { Model } from 'vue-mc'
import { string, numeric, required, alphanumeric, match, messages } from 'vue-mc/validation'

messages.set('match', "May only contain letters and spaces.");

export default class Passenger extends Model {

    defaults() {
        return {
            id: null,
            passportNumber: '',
            name: '',
            nationality: '',
            gender: 0,
            dobDay: '',
            dobMonth: '',
            dobYear: '',
            passportExpiryDay: '',
            passportExpiryMonth: '',
            passportExpiryYear: '',
            issuingCountry: '',
            passportIssueDay: '',
            passportIssueMonth: '',
            passportIssueYear: '',
            type: 0,
        }
    }

    options() {
        return {
            useFirstErrorOnly: true,
            validateOnChange: true,
        }
    }

    validation() {
        return {
            name: required.and(match(/^[A-Za-z\s]*$/)),
            nationality: required.and(string),
            gender: required.and(numeric),
            dobDay: required,
            dobMonth: required,
            dobYear: (value) => {
                if (!value) {
                    return 'Required';
                }

                if (this._attributes.type == 1) {
                    if (!this._attributes.dobDay || !this._attributes.dobMonth) {
                        return 'Pick Date and Month';
                    } else {
                        let twoYearAgo = moment().startOf('day').subtract(2, 'year').format('YYYY-MM-DD');
                        let passengerDob = moment(value+'-'+this._attributes.dobMonth+'-'+this._attributes.dobDay).startOf('day');
                        let isAfter = moment(twoYearAgo).isAfter(passengerDob.format('YYYY-MM-DD'));
                        
                        if (isAfter) return 'Infant Age need to be 0-2 years old'
                    }
                }

            },
            issuingCountry: required.and(string),
            type: required.and(numeric),
        }
    }

    mutations() {
        return {
            id: (id) => Number(id) || null,
            passportNumber: String,
            name: String,
            nationality: String,
            gender: Number,
            // dob: function(dob) {
            //     let arr = dob.split("-");
            //     let object = {
            //         date: arr[2],
            //         month: arr[1],
            //         year: arr[0],
            //     }
            //     return object;
            // },
            // passportExpiry: {
            //     date: Number,
            //     month: Number,
            //     year: Number,
            // },
            issuingCountry: String,
            type: (type) => Number(type)
        }
    }
}