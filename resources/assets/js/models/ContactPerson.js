import { Model } from 'vue-mc'
import { email, required, same, numeric } from 'vue-mc/validation'

export default class ContactPerson extends Model {

    defaults() {
        return {
            id: null,
            emailAddress: '',
            confirmEmailAddress: '',
            mobile: '',
        }
    }

    options() {
        return {
            useFirstErrorOnly: true,
            validateOnChange: true,
        }
    }

    validation() {
        return {
            emailAddress: required.and(email),
            confirmEmailAddress: required.and(email).and(same('emailAddress')),
            mobile: required.and(numeric),
        }
    }

    mutations() {
        return {
            id: (id) => Number(id) || null,
            emailAddress: String,
            confirmEmailAddress: String,
            mobile: String,
        }
    }
}