export default class PassengerType {
    static get ECONOMY() {
        return 0;
    }
    static get BUSINESS() {
        return 1;
    }
    static get VIP() {
        return 1;
    }

    static getList() {
        return [
            this.ECONOMY,
            this.BUSINESS,
            this.VIP,
        ]
    }

	static getArray() {
		let result = [];
		$.each(this.getList(), function(key, value) {
			result[key] = this.getString(value);
        }) 
		return result;
	}

	static getString(val) {
		switch (Number(val)) {
			case 0:
				return "Economy";
			case 1:
				return "Business";
			case 2:
				return "VIP";
		}
	}
}