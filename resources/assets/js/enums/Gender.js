export default class PassengerType {
    static get MALE() {
        return 0;
	}
	
    static get FEMALE() {
        return 1;
	}
	
    static get OTHER() {
        return 2;
    }

    static getList() {
        return [
            this.MALE,
            this.FEMALE,
            this.OTHER,
        ]
    }

	static getArray() {
		let result = [];
		let self = this;
		$.each(this.getList(), function(key, value) {
			result[key] = self.getString(value);
        }) 
		return result;
	}

	static getString(val) {
		switch (val) {
			case 0:
				return "Male";
			case 1:
				return "Female";
			case 2:
				return "Other";
		}
	}
}