export default class PassengerType {
    static get ADULT() {
        return 0;
    }
    static get INFANT() {
        return 1;
    }

    static getList() {
        return [
            this.ADULT,
            this.INFANT,
        ]
    }

	static getArray() {
		let result = [];
		$.each(this.getList(), function(key, value) {
			result[key] = this.getString(value);
        }) 
		return result;
	}

	static getString(val) {
		switch (val) {
			case 0:
				return "Adult";
			case 1:
				return "Infant";
		}
	}
}