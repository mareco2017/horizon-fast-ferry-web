require('./bootstrap');
window.Vue = require('vue');

import VueInternationalization from 'vue-i18n'
import Locale from './vue-i18n-locales.generated'
import VueRouter from 'vue-router'
import router from './routes.js'
import { store } from './_store'
import HomeComponent from './components/HomeComponent.vue'
import API from './api.js'
import FerryRoutes from './ferry_routes.js'
import VueScrollReveal from 'vue-scroll-reveal'
import VueMoment from 'vue-moment'
import Vuelidate from 'vuelidate'
import Vue2Filters from 'vue2-filters'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import * as VueGoogleMaps from 'vue2-google-maps'
import Toasted from 'vue-toasted';

library.add(fab, fas, faSpinner);

Vue.use(VueInternationalization);
Vue.use(VueScrollReveal, {
    duration: 800,
    scale: 1,
    distance: '10px',
    origin: 'top',
    mobile: false,
    easing: 'cubic-bezier(0.23,1,0.32,1)'
});
Vue.use(VueMoment);
Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.use(Vue2Filters);
Vue.use(Toasted, {
    duration: 4000,
    iconPack : 'material',
    // action: {
    //     text: 'CLOSE',
    //     onClick: (e, toastObject) => {
    //         toastObject.goAway(0);
    //     }
    // }
})
Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyAmlvZe7w0ZjQfw2j4UYasTVZrcjenzkXM',
      libraries: 'places', // This is required if you use the Autocomplete plugin
      // OR: libraries: 'places,drawing'
      // OR: libraries: 'places,drawing,visualization'
      // (as you require)
   
      //// If you want to set the version, you can do so:
      // v: '3.26',
    },
   
    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,
   
    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
  })

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.directive('tooltip', function(el, binding) {
    $(el).tooltip({
         title: binding.value,
         placement: binding.arg,
         trigger: 'hover',
         html: true
    })
})

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);
  
    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  
    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;
  
    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));
  
    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();
  
    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
      const tag = document.createElement('meta');
  
      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
      });
  
      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute('data-vue-router-controlled', '');
  
      return tag;
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag));
  
    next();
});

const lang = document.documentElement.lang.substr(0, 2); 
const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});
const app = new Vue({
    el: '#app',
    i18n,
    router,
    store,
    data: {
        api: API,
        ferryRoutes: FerryRoutes
    },
    component: {
        HomeComponent,
    },
    render: h => h(HomeComponent)
});