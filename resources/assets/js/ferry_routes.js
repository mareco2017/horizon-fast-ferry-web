export default [
    {
        id: 1,
        name: "Batam Harbour Bay",
        portName: "Harbour Bay",
        placeCode: "BTM",
        place: "batam"
    },
    {
        id: 2,
        name: "Singapore HarbourFront",
        portName: "HarbourFront",
        placeCode: "SG",
        place: "singapore"
    }
];