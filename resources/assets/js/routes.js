import VueRouter from 'vue-router'
import DashboardComponent from './components/Dashboard/DashboardComponent.vue';
import LoginComponent from './components/Auth/LoginComponent.vue';
// import RegisterComponent from './components/Auth/RegisterComponent.vue';
import BookingComponent from './components/Booking/BookingComponent.vue';
import MyAccountComponent from './components/Account/MyAccountComponent.vue';
import AccountComponent from './components/Account/Sidebar/AccountComponent.vue';
import MyBookingComponent from './components/Account/Sidebar/MyBookingComponent.vue';
import ResetPasswordComponent from './components/Password/ResetPasswordComponent.vue';
import ForgotPasswordComponent from './components/Password/ForgotPasswordComponent.vue';
import BatamHighlightsComponent from './components/Dashboard/BatamHighlightsComponent.vue';
import NotFoundComponent from './components/NotFoundComponent.vue';
import OrderComponent from './components/Order/OrderComponent.vue';
import TravelInformationComponent from './components/Dashboard/TravelInformationComponent.vue';
import TermsComponent from './components/Dashboard/TermsComponent.vue';
import PrivacyComponent from './components/Dashboard/PrivacyComponent.vue';
import FaqComponent from './components/Dashboard/FaqComponent.vue';
import ContactUsComponent from './components/Dashboard/ContactUsComponent.vue';
import AboutUsComponent from './components/Dashboard/AboutUsComponent.vue';
import ArticleComponent from './components/Article/ArticleComponent.vue';
import BookingWifiComponent from './components/BookingWifi/BookingWifiComponent.vue';
import BookingCarComponent from './components/BookingCar/BookingCarComponent.vue';

var defaultMeta = {
  title: 'Horizon Ferry | Official Batam Ferry | 35 mins to Batam.',
  metaTags: [
    {
      name: 'description',
      content: 'Economic and Business class. Priority queue in Batam Custom Clearance. Auto gate Custom Clearance for foreigner. Wifi rental. Car Charter service. 10 minutes drive from Nagoya City Centre. Travel Content and info for first time traveller.'
    }
  ]
};

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    { 
      path: '/',
      name: 'dashboard',
      component: DashboardComponent,
      meta: defaultMeta
    },
    {
      path: '/chinese-new-year-2017-for-walk-in-purchase-only-2',
      redirect: to => {
        window.location = "https://horizonfastferry.com.sg/blog/how-to-skip-queue-during-holiday-season-in-batam/";
      },
    },
    {
      path: '/batam-highlights',
      name: 'batam-highlights',
      redirect: to => {
        window.location = "https://horizonfastferry.com.sg/blog";
      },
      // component: BatamHighlightsComponent,
      // meta: defaultMeta
    },
    {
      path: '/article',
      redirect: to => {
        window.location = "https://horizonfastferry.com.sg/blog";
      },
      children: [
        {
          path: '9-must-try-seafood-restaurants-in-batam-that-is-super-cheap',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/batam-seafood";
          },
        },
        {
          path: 'top-tasting-kueh-lapis-review-in-batam',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/kueh-lapis-review-batam";
          },
        },
        {
          path: 'major-shopping-places-in-batam',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/batam-shopping-malls";
          },
        },
        {
          path: 'get-an-insight-into-the-specialized-spas-and-resorts-in-batam',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/batam-spas";
          },
        },
        {
          path: 'top-10-cafes-in-batam-that-are-instagram-worthy-plus-batam-brewery-craft-beer-places',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/batam-cafes";
          },
        },
        {
          path: 'harbour-bay-downtown',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/harbour-bay-downtown-batam";
          },
        },
        {
          path: 'ultimate-batam-destination-guide',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/batam-destination-guide";
          },
        },
        {
          path: '2019-top-family-activities-in-batam-that-makes-you-go-wow',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/family-activities-batam";
          },
        },
        {
          path: '2019-top-7-outdoor-action-activities-in-batam-that-is-not-massage-or-spa',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/outdoor-activities-batam";
          },
        },
        {
          path: 'official-batam-ferry-terminal',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/official-batam-ferry-terminal";
          },
        },
        {
          path: 'singapore-public-holidays-for-2019',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/singapore-public-holidays";
          },
        },
        {
          path: 'chinese-new-year-2017-for-walk-in-purchase-only-2',
          redirect: to => {
            window.location = "https://horizonfastferry.com.sg/blog/how-to-skip-queue-during-holiday-season-in-batam/";
          },
        },
      ]
    },
    // {
    //   path: '/article/:slug',
    //   name: 'article-detail',
    //   component: ArticleComponent,
    //   meta: defaultMeta
    // },
    {
      path: '/travel-information',
      name: 'travel-information',
      component: TravelInformationComponent,
      meta: {
        title: 'Important Information Travelling to Batam | Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: "Travelling to Batam? Here are some important info like Passports & Visas, how to get Visa on Arrival (VOA), where the Indonesian Embassy in Singapore is located, etc."
          }
        ]
      }
    },
    {
      path: '/terms',
      name: 'terms',
      component: TermsComponent,
      meta: {
        title: 'Terms & Conditions | Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: "Horizon Ferry's terms & conditions' page. We listed 24 T&Cs on this web page. Do read them to find out more."
          }
        ]
      }
    },
    {
      path: '/privacy',
      name: 'privacy',
      component: PrivacyComponent,
      meta: {
        title: 'Privacy Policy | Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: "Horizon Ferry's privacy policys' page. We listed 10 on this web page. Do read them to find out more."
          }
        ]
      }
    },
    {
      path: '/faq',
      name: 'faq',
      component: FaqComponent,
      meta: {
        title: 'FAQs on Ferry Travelling to Batam Using Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: 'We listed 15 FAQs that you might ask before you travel to Batam using using Horizon Ferry. Question not found? Email to singapore@horizonfastferry.com.'
          }
        ]
      }
    },
    {
      path: '/contact-us',
      name: 'contact-us',
      component: ContactUsComponent,
      meta: {
        title: 'Contact Us | Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: 'Here are our contact detail and addresses for our Singapore and Batam office.'
          }
        ]
      }
    },
    {
      path: '/about-us',
      name: 'about-us',
      component: AboutUsComponent,
      meta: {
        title: 'About Us | Horizon Ferry',
        metaTags: [
          {
            name: 'description',
            content: 'Find out more about Horizon Ferry, our history and our vision for our services.'
          }
        ]
      }
    },
    { 
      path: '/login',
      name: 'login',
      component: LoginComponent,
      meta: defaultMeta
    },
    // { 
    //     path: '/register',
    //     name: 'register',
    //     component: RegisterComponent,
    // },
    { 
      path: '/booking',
      name: 'booking',
      component: BookingComponent,
      meta: defaultMeta
    },
    { 
      path: '/booking-wifi',
      name: 'booking-wifi',
      component: BookingWifiComponent,
      meta: defaultMeta
    },
    { 
      path: '/booking-car',
      name: 'booking-car',
      component: BookingCarComponent,
      meta: defaultMeta
    },
    { 
      path: '/order',
      name: 'order',
      component: OrderComponent,
      meta: defaultMeta
    },
    { 
      path: '/my-account',
      name: 'my-account',
      component: MyAccountComponent,
      meta: defaultMeta,
      children: [
        {
          path: 'account',
          name: 'my-account.account',
          component: AccountComponent,
        },
        {
          path: 'my-booking',
          name: 'my-account.my-booking',
          component: MyBookingComponent,
        },
      ]
    },
    // Password
    { 
      path: '/password/forgot',
      name: 'forgot-password',
      component: ForgotPasswordComponent,
      meta: defaultMeta
    },
    { 
      path: '/password/reset/:token',
      name: 'reset-password',
      component: ResetPasswordComponent,
      meta: defaultMeta
    },
    {
      path: '*',
      name: 'not-found',
      component: NotFoundComponent,
      meta: defaultMeta,
      props: { hideHeader: true, hideFooter: true }
    }
  ]
});