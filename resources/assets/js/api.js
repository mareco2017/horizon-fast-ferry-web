var baseURL = `/api/v1`;

export default {
    baseUrl: baseURL,
    // Auth
    login: baseURL + `/login`,
    logout: baseURL + `/logout`,
    register: baseURL + `/register`,
    quickRegister: baseURL + `/quick-register`,
    refresh: baseURL + `/refresh`,
    socialLogin: baseURL + `/social-login`,

    // Usr
    userProfile: baseURL + `/user/me`,
    updateProfile: baseURL + `/user/update-profile`,
    changePassword: baseURL + `/user/change-password`,
    getOwnedOrder: baseURL + `/order/owned`,


    // Password
    sendResetPasswordMail: baseURL + `/password/email`,
    resetPassword: baseURL + `/password/reset`,

    // Validation
    checkExists: baseURL + `/check-exists`,

    // Dashboard Section
    getLatestPromotions: baseURL + `/get-latest-promotions`,
    getArticles: baseURL + `/get-articles`,
    getArticle: baseURL + `/get-article`,
    getBatamHighlights: baseURL + `/get-batam-highlights`,
    getFaqs: baseURL + `/get-faqs`,

    // Booking Section
    getPassengers: baseURL + `/passenger/owned`,
    getContactPerson: baseURL + `/contact-person/owned`,
    getTicketPrice: baseURL + `/ticket/price`,
    reserveBooking: baseURL + `/order/reserve`,
    getWifiPrice: baseURL + `/ticket/wifi-price`,
    reserveWifiBooking: baseURL + `/order/reserve-wifi`,
    reserveCarBooking: baseURL + `/order/reserve-car`,
    
    // Booking Step One
    getSchedule: baseURL + `/schedule/daily`,

    // Order
    searchOrder: baseURL + `/order/search`,
    cancelOrder: baseURL + `/order/cancel`,

    // Payment
    stripeCharge: baseURL + `/order/stripe-charge`,
    stripePaymentIntent: baseURL + `/order/stripe-payment-intent`,

    // Contact Us
    sendEnquiry: baseURL + `/send-enquiry`
}