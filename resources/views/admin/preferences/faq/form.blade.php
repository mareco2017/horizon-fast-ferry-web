@extends('templates.admin.master')

@section('title')
    {{ $faq->id ? 'Edit' : 'Create' }} FAQ @parent
@endsection

@section('groupName', 'Preferences')

@section('content')
@if (!$faq->id)
{!! Form::open(['route' => 'admin.preferences.faq.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($faq, ['route' => ['admin.preferences.faq.update', $faq], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $faq->id ? 'Edit' : 'Create' }} FAQ</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="question" class="col-2 col-form-label">Question</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('question', old('question'), array('class' => 'form-control', 'id' => 'question', 'placeholder' => 'Question')) }}
                        @if($errors->has('question'))
                        <p class="status-text">{{ $errors->first('question') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="answer" class="col-2 col-form-label">Answer</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('answer', old('answer'), array('class' => 'form-control v-hidden', 'id' => 'answer', 'placeholder' => 'Answer')) }}
                        @if($errors->has('answer'))
                        <p class="status-text">{{ $errors->first('answer') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
CKEDITOR.replace('answer', {
    height: 500
});
</script>
@endpush