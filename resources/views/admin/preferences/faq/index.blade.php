@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    FAQ @parent
@endsection

@section('groupName', 'Preferences')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">FAQ</h4>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.preferences.faq.create') }}">Create new FAQ</a>
                <a class="dropdown-item" href="{{ route('admin.preferences.faq.customize-urut') }}">Rearrange</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Show All Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Urut</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.preferences.faq.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'question', name: 'question', className: 'text-center' },
                { data: 'answer', name: 'answer', className: 'text-center' },
                { data: 'urut', name: 'urut', className: 'text-center' },
                { data: 'status', name: 'status', className: 'text-center', sortable: false, searchable: false },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
			]
		});
        $(table.table().container()).removeClass('form-inline');
        
        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush