@extends('templates.admin.master')

@section('title')
    Rearrange FAQ @parent
@endsection

@section('groupName', 'Preferences')

@section('content')
{!! Form::open(['url' => route('admin.preferences.faq.update-urut'), 'method' => 'POST', 'id' => 'urutForm']) !!}
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">FAQ</h4>
        </div>
        <div class="card-body">
            {{ Form::hidden('order', null, array('id' => 'order')) }}
            {{ Form::hidden('nullifier', null, array('id' => 'nullifier')) }}
            <div class="row form-group">
                <div class="col-12 col-lg-6">
                    <div class="border bg-light-grey p-3">
                        <p class="fs-3">Active</p>
                        <div id="shown" class="my-min-1 connectedSortable sortable">
                        @foreach ($displayedFaqs as $key => $displayedFaq)
                            <div id="{{ $displayedFaq->id }}" class="py-1">
                                <div class="p-3 bg-white border pointer ui-state-default">{{ $displayedFaq->question }}</div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="border bg-light-grey p-3">
                        <p class="fs-3">Not showed</p>
                        <div id="notShown" class="my-min-1 connectedSortable sortable">
                        @foreach ($undisplayedFaqs as $key => $undisplayedFaq)
                            <div id="{{ $undisplayedFaq->id }}" class="py-1">
                                <div class="p-3 bg-white border pointer ui-state-default">{{ $undisplayedFaq->question }}</div>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <p class="fs-0 mb-0"><b>Note: </b>Some question is missing? please make sure the status is <b>Active</b></p>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-save"></i> Save</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var urutForm = $('#urutForm');
        var hiddenOrder = $('#order');
        var hiddenNullifier = $('#nullifier');
        var sortable = $('#shown, #notShown');
        var shown = $('#shown');
        var nullifiers = [];

        sortable.sortable({
            connectWith: ".connectedSortable",
            receive: function(event, ui) {
                let container = $(event.target);
                let dropped = $(ui.item);
                let id = dropped.attr('id');
    
                if (container.attr('id') == 'shown') {
                    nullifiers.splice($.inArray(id, nullifiers), 1);
                } else if (container.attr('id') == 'notShown') {
                    nullifiers.push(id);
                }
            },
        });
        
        urutForm.on('submit', function(e) {
            e.preventDefault();
            let order = shown.sortable("toArray");
            hiddenOrder.val(order);
            hiddenNullifier.val(nullifiers);
            this.submit();
        });
    });
</script>
@endpush