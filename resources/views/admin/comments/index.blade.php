@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Comment List @parent
@endsection
@section('groupName', 'Comments')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Comment List</h3>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.comment.create') }}">Create new comment</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-12 col-md-4">
                <div class="form-group">
                    <label for="filterStatus">Filter Status</label>
                    {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Show All Status']) }}
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Article</th>
                    <th class="w-30">Comment</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
        var filterStatusEnum = null;

		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.comment.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'name', name: 'name', className: 'text-center' },
                { data: 'email', name: 'email', className: 'text-center' },
                { data: 'article.title', name: 'article.title', className: 'text-center' },
                { data: 'desc', name: 'desc', className: 'text-center', sortable: false },
                { data: 'status', name: 'status', className: 'text-center', sortable: false, searchable: false},
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false},
			]
		});
		$(table.table().container()).removeClass('form-inline');
        
        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        }); 
	});
</script>
@endpush