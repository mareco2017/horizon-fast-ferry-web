@extends('templates.admin.master')

@section('title')
    {{ $comment->id ? 'Edit' : 'Create'}} Comment @parent
@endsection
@section('groupName', 'Comments')

@section('content')
@if (!$comment->id)
{!! Form::open(['route' => 'admin.comment.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($comment, ['route' => ['admin.comment.update', $comment], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $comment->id ? 'Edit' : 'Create' }} Comment</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="article_id" class="col-2 col-form-label">Article ID</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::number('article_id', old('article_id'), array('class' => 'form-control', 'id' => 'article_id', 'placeholder' => 'Article ID')) }}
                        @if($errors->has('article_id'))
                        <p class="status-text">{{ $errors->first('article_id') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="name" class="col-2 col-form-label">Name</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('name', old('name'), array('class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name', 'required')) }}
                        @if($errors->has('name'))
                        <p class="status-text">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="email" class="col-2 col-form-label">Email</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('email', old('email'), array('class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email')) }}
                        @if($errors->has('email'))
                        <p class="status-text">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="desc" class="col-2 col-form-label">Comment</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('desc', old('desc'), array('class' => 'form-control', 'id' => 'desc', 'placeholder' => 'Comment')) }}
                        @if($errors->has('desc'))
                        <p class="status-text">{{ $errors->first('desc') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection