@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Order List @parent
@endsection

@section('groupName', 'Order')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Order List</h3>
    </div>
    <div class="card-body">
        {!! Form::open(['route' => 'admin.order.export-excel', 'method' => 'post']) !!}
            <div class="row mb-3">
                <div class="col">
                    <div class="form-group">
                        <label for="filterStatus">Filter Status</label>
                        {{ Form::select('status', $statusList, '', ['class' => 'form-control nullable', 'id' => 'filterStatus', 'placeholder' => 'Show All Status']) }}
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="startDate">Filter Start Date</label>
                        {{ Form::text('start_date', old('start_date'), array('class' => 'form-control datepickers', 'id' => 'startDate', 'placeholder' => 'Start Date')) }}
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="endDate">Filter End Date</label>
                        {{ Form::text('end_date', old('end_date'), array('class' => 'form-control datepickers', 'id' => 'endDate', 'placeholder' => 'End Date')) }}
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="filterWifi">Filter Wifi</label>
                        {{ Form::select('wifi', $wifiStatus, old('wifi'), array('class' => 'form-control nullable', 'id' => 'filterWifi', 'placeholder' => 'No Filter')) }}
                    </div>
                </div>
                <div class="px-3">
                    <div class="form-group">
                        <label for="endDate">&nbsp;</label><br>
                        <button type="submit" class="btn btn-round btn-excel m-0"><i class="far fa-file-excel"></i> Export Excel</button>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th rowspan="2">Id</th>
                    <th rowspan="2">Reference Number</th>
                    <th colspan="2" class="text-center">Contact Person</th>
                    <th rowspan="2">Departure From</th>
                    <th rowspan="2">Wifi Rental</th>
                    <th rowspan="2">Status</th>
                    <th rowspan="2">Created At</th>
                    <th rowspan="2">Action</th>
                </tr>
                <tr>
                    <th>Email</th>
                    <th>Phone No.</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
@endpush

@push('pageRelatedJs')
<script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
<script>
	$(document).ready(function() {
        var inputStart = $('input[name="start_date"]');
        var inputEnd = $('input[name="end_date"]');

        inputStart.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true
        });

        inputEnd.datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            clearBtn: true,
            endDate: new Date()
        });

        var filterStatusEnum = null;
        var filterStartDate = null;
        var filterEndDate = null;
        var filterWifi = null;

		var table = $('#table').DataTable(
		{
            language: {
                search: `
                <a href="#" data-toggle="tooltip" data-html="true" data-placement="left" 
                   title='
                   <p class="mb-1">Searchable Field:</p>
                   <span class="tooltip-label">Id</span>
                   <span class="tooltip-label">Reference Number</span>
                   <span class="tooltip-label">Contact Person</span>
                   '>
                    <i class="fas fa-question-circle search-tooltip"></i>
                </a> Search`
            },
            "initComplete": function(settings, json) {
                $('[data-toggle="tooltip"]').tooltip(); 
            },
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.order.ajax') !!}',
                data: function(d) { 
                    d.status_enum = filterStatusEnum;
                    d.start_date = filterStartDate;
                    d.end_date = filterEndDate;
                    d.wifi = filterWifi;
                },
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'reference_number', name: 'reference_number', className: 'text-center' },
                { data: 'contact_person.email', name: 'contactPerson.email', className: 'text-center' },
                { data: 'contact_person.phone_number', name: 'contactPerson.phone_number', className: 'text-center' },
                { data: 'departure', name: 'departure', className: 'text-center', sortable: false, searchable: false },
                { data: 'wifi', name: 'wifi', className: 'text-center', sortable: false, searchable: false },
                { data: 'status', name: 'status', className: 'text-center', searchable: false },
                { data: 'created_at', name: 'created_at', className: 'text-center',
                    render: function (data, type, full, meta) {
                        var date = moment.tz(data, 'UTC').tz(moment.tz.guess());
                        return date.format('LLLL');
                    } 
                },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
			]
		});
		$(table.table().container()).removeClass('form-inline');
        
        $('#filterStatus').on('change', function(e) { 
            filterStatusEnum = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#startDate').on('change', function(e) { 
            filterStartDate = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#endDate').on('change', function(e) { 
            filterEndDate = e.currentTarget.value; 
            table.draw(); 
        });
        
        $('#filterWifi').on('change', function(e) { 
            filterWifi = e.currentTarget.value; 
            table.draw(); 
        });
	});
</script>
@endpush