@extends('templates.admin.master')

@section('title')
    {{ $order->id ? 'Edit' : 'Create'}} Order @parent
@endsection
@section('groupName', 'Order')

@section('content')
<form action="" class="form-horizontal">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Transaction Details</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label class="col-2 col-form-label">Reference Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $order->reference_number, array('class' => 'form-control', 'readonly', 'title' => $order->reference_number.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Pax Quantity</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $order->options->quantity, array('class' => 'form-control', 'readonly', 'title' => $order->options->quantity.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Seat Type</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', TicketType::getString($order->options->type), array('class' => 'form-control', 'readonly', 'title' => TicketType::getString($order->options->type).' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Departure Trip</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $order->options->departure_route.' | '.\Carbon\Carbon::parse($order->options->departure_date)->format('D, d M Y').' | '.$order->options->departure_time, array('class' => 'form-control', 'readonly', 'title' => $order->options->departure_route.' | '.\Carbon\Carbon::parse($order->options->departure_date)->format('D, d M Y').' | '.$order->options->departure_time.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Return Trip</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $order->options->two_way ? $order->options->return_route.' | '.\Carbon\Carbon::parse($order->options->return_date)->format('D, d M Y').' | '.$order->options->return_time : '-', array('class' => 'form-control', 'readonly', 'title' => ($order->options->two_way ? $order->options->return_route.' | '.\Carbon\Carbon::parse($order->options->return_date)->format('D, d M Y').' | '.$order->options->return_time : '-').' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Total Payment</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', '$'.$order->total, array('class' => 'form-control', 'readonly', 'title' => '$'.$order->total.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Email</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $contactPerson->email, array('class' => 'form-control', 'readonly', 'title' => $contactPerson->email.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Phone Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $contactPerson->phone_number, array('class' => 'form-control', 'readonly', 'title' => $contactPerson->phone_number.' | Readonly')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Created At</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('', $order->created_at->setTimezone('Asia/Jakarta'), array('class' => 'form-control', 'readonly', 'title' => $order->created_at->setTimezone('Asia/Jakarta').' | Readonly')) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@if (!$order->id)
{!! Form::open(['route' => 'admin.order.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($order, ['route' => ['admin.order.update', $order], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Pax Details</h3>
        </div>
        <div class="card-body">
            @foreach($dTickets as $t)
                <div class="{{ $loop->last ? '' : 'mb-4'}}">
                    <h6 class="text-center mb-3">Pax {{ $loop->index + 1 }} ({{ $order->options->two_way ? 'Round Trip' : 'One Way' }})</h6>
                    <div class="row">
                        <div class="col-12 col-lg-6">
                            <div class="row">
                                <label class="col-3 col-form-label">Pax Type</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', $t->options->trip_type.' | '.$t->order->options->departure_route, array('class' => 'form-control', 'readonly', 'title' => $t->options->trip_type.' | '.$t->order->options->departure_route.' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-3 col-form-label">Age Group</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', \App\Enums\OrderDetailType::getType($t->type), array('class' => 'form-control', 'readonly', 'title' => \App\Enums\OrderDetailType::getType($t->type).' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-3 col-form-label">Name</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', $t->passenger->name, array('class' => 'form-control', 'readonly', 'title' => $t->passenger->name.' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-3 col-form-label">Passport Number</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('', $t->passenger->passport_number, array('class' => 'form-control', 'readonly', 'title' => $t->passenger->passport_number.' | Readonly')) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label for="boarding_pass_depart{{ $loop->index }}" class="col-3 col-form-label">Boarding Pass</label>
                                <div class="col-9">
                                    <div class="form-group">
                                        {{ Form::text('boarding_pass_depart[]', old('boarding_pass_depart.'.$loop->index), array('class' => 'form-control', 'id' => 'boarding_pass_depart'.$loop->index, 'placeholder' => 'Boarding Pass')) }}
                                        @if($errors->has('boarding_pass_depart.'.$loop->index))
                                        <p class="status-text">{{ $errors->first('boarding_pass_depart.'.$loop->index) }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-6">
                            @if ($order->options->two_way && $rTickets && count($rTickets) > 0)
                                <div class="row">
                                    <label class="col-3 col-form-label">Pax Type</label>
                                    <div class="col-9">
                                        <div class="form-group">
                                            {{ Form::text('', $rTickets[$loop->index]->options->trip_type.' | '.$rTickets[$loop->index]->order->options->return_route, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->options->trip_type.' | '.$rTickets[$loop->index]->order->options->return_route.' | Readonly')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-3 col-form-label">Age Group</label>
                                    <div class="col-9">
                                        <div class="form-group">
                                            {{ Form::text('', \App\Enums\OrderDetailType::getType($rTickets[$loop->index]->type), array('class' => 'form-control', 'readonly', 'title' => \App\Enums\OrderDetailType::getType($rTickets[$loop->index]->type).' | Readonly')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-3 col-form-label">Name</label>
                                    <div class="col-9">
                                        <div class="form-group">
                                            {{ Form::text('', $rTickets[$loop->index]->passenger->name, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->passenger->name.' | Readonly')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-3 col-form-label">Passport Number</label>
                                    <div class="col-9">
                                        <div class="form-group">
                                            {{ Form::text('', $rTickets[$loop->index]->passenger->passport_number, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->passenger->passport_number.' | Readonly')) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label for="boarding_pass_return{{ $loop->index }}" class="col-3 col-form-label">Boarding Pass</label>
                                    <div class="col-9">
                                        <div class="form-group">
                                            {{ Form::text('boarding_pass_return[]', old('boarding_pass_return.'.$loop->index), array('class' => 'form-control', 'id' => 'boarding_pass_return'.$loop->index, 'placeholder' => 'Boarding Pass')) }}
                                            @if($errors->has('boarding_pass_return.'.$loop->index))
                                            <p class="status-text">{{ $errors->first('boarding_pass_return.'.$loop->index) }}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
