@extends('templates.admin.master')

@section('title')
    Order Detail @parent
@endsection

@section('groupName', 'Order')

@section('content')
@php
    $wifiOnly = isset($order->options->wifi_only) && $order->options->wifi_only;
    $carOnly = isset($order->options->car_only) && $order->options->car_only;
@endphp
<form action="" class="form-horizontal">
    <div class="row">
        <div class="col-12 col-xl-4">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Transaction Details</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-4 col-form-label">Reference Number</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $order->reference_number, array('class' => 'form-control', 'readonly', 'title' => $order->reference_number.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-4 col-form-label">Created At</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $order->created_at->setTimezone('Asia/Jakarta'), array('class' => 'form-control', 'readonly', 'title' => $order->created_at->setTimezone('Asia/Jakarta').' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    @if ($wifiOnly)
                    <div class="row">
                        <label class="col-4 col-form-label">Wifi Only</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $wifiOnly ? 'Yes' : 'No', array('class' => 'form-control', 'readonly', 'title' => $wifiOnly ? 'Yes' : 'No')) }}
                            </div>
                        </div>
                    </div>
                    @elseif ($carOnly)
                    <div class="row">
                        <label class="col-4 col-form-label">Car Only</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $carOnly ? 'Yes' : 'No', array('class' => 'form-control', 'readonly', 'title' => $carOnly ? 'Yes' : 'No')) }}
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        <label class="col-4 col-form-label">Wifi Rental</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $wifiRental ? 'Yes' : 'No', array('class' => 'form-control', 'readonly', 'title' => $wifiRental ? 'Yes' : 'No')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-4 col-form-label">Car Rental</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $carRental ? 'Yes' : 'No', array('class' => 'form-control', 'readonly', 'title' => $carRental ? 'Yes' : 'No')) }}
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <label class="col-4 col-form-label">Total Payment</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', '$'.$order->total, array('class' => 'form-control', 'readonly', 'title' => '$'.$order->total.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-4 col-form-label">Status</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', OrderStatus::getString($order->status), array('class' => 'form-control', 'readonly', 'title' => OrderStatus::getString($order->status).' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <label class="col-4 col-form-label">Email</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $contactPerson->email, array('class' => 'form-control', 'readonly', 'title' => $contactPerson->email.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-4 col-form-label">Phone Number</label>
                        <div class="col-8">
                            <div class="form-group">
                                {{ Form::text('', $contactPerson->phone_number, array('class' => 'form-control', 'readonly', 'title' => $contactPerson->phone_number.' | Readonly')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-xl-8">
            @if (count($dTickets) > 0)
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Ticket Details</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <label class="col-2 col-form-label">Pax Quantity</label>
                            <div class="col-10">
                                <div class="form-group">
                                    {{ Form::text('', $order->options->quantity, array('class' => 'form-control', 'readonly', 'title' => $order->options->quantity.' | Readonly')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-2 col-form-label">Seat Type</label>
                            <div class="col-10">
                                <div class="form-group">
                                    {{ Form::text('', TicketType::getString($order->options->type), array('class' => 'form-control', 'readonly', 'title' => TicketType::getString($order->options->type).' | Readonly')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-2 col-form-label">Departure Trip</label>
                            <div class="col-10">
                                <div class="form-group">
                                    {{ Form::text('', $order->options->departure_route.' | '.\Carbon\Carbon::parse($order->options->departure_date)->format('D, d M Y').' | '.$order->options->departure_time, array('class' => 'form-control', 'readonly', 'title' => $order->options->departure_route.' | '.\Carbon\Carbon::parse($order->options->departure_date)->format('D, d M Y').' | '.$order->options->departure_time.' | Readonly')) }}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-2 col-form-label">Return Trip</label>
                            <div class="col-10">
                                <div class="form-group">
                                    {{ Form::text('', $order->options->two_way ? $order->options->return_route.' | '.\Carbon\Carbon::parse($order->options->return_date)->format('D, d M Y').' | '.$order->options->return_time : '-', array('class' => 'form-control', 'readonly', 'title' => ($order->options->two_way ? $order->options->return_route.' | '.\Carbon\Carbon::parse($order->options->return_date)->format('D, d M Y').' | '.$order->options->return_time : '-').' | Readonly')) }}
                                </div>
                            </div>
                        </div>
                        <div class="mt-4">
                            @foreach($dTickets as $t)
                                <div class="{{ $loop->last ? '' : 'mb-4'}}">
                                    <h6 class="text-center mb-3">Pax {{ $loop->index + 1 }} ({{ $order->options->two_way ? 'Round Trip' : 'One Way' }})</h6>
                                    <div class="row">
                                        <div class="col-12 col-lg-6">
                                            <div class="row">
                                                <label class="col-3 col-form-label">Pax Type</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', $t->options->trip_type.' | '.$t->order->options->departure_route, array('class' => 'form-control', 'readonly', 'title' => $t->options->trip_type.' | '.$t->order->options->departure_route.' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Age Group</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', \App\Enums\OrderDetailType::getType($t->type), array('class' => 'form-control', 'readonly', 'title' => \App\Enums\OrderDetailType::getType($t->type).' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Name</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', $t->passenger->name, array('class' => 'form-control', 'readonly', 'title' => $t->passenger->name.' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Passport Number</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', $t->passenger->passport_number, array('class' => 'form-control', 'readonly', 'title' => $t->passenger->passport_number.' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-3 col-form-label">Boarding Pass</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        @if(isset($t->options->boarding_pass_number))
                                                            {{ Form::text('', $t->options->boarding_pass_number, array('class' => 'form-control', 'readonly', 'title' => $t->options->boarding_pass_number.' | Readonly')) }}
                                                        @elseif(isset($t->options->error_description))
                                                            {{ Form::text('', $t->options->error_description, array('class' => 'form-control', 'readonly', 'title' => $t->options->error_description.' | Readonly')) }}
                                                        @else
                                                            {{ Form::text('', '', array('class' => 'form-control', 'readonly', 'title' => 'Readonly')) }}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                @php
                                                    $eBP = isset($t->options->e_boarding_pass) ? $t->options->e_boarding_pass : null;
                                                    $eBPStatus = $eBP && ($eBP->eBPStatus == "True" || $eBP->eBPStatus == "true" || $eBP->eBPStatus === true) ? 'Yes' : 'No';
                                                @endphp
                                                <label class="col-3 col-form-label">E-BP</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', $eBPStatus, array('class' => 'form-control', 'readonly', 'title' => $eBPStatus.' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            @if ($order->options->two_way && $rTickets && count($rTickets) > 0)
                                                <div class="row">
                                                    <label class="col-3 col-form-label">Pax Type</label>
                                                    <div class="col-9">
                                                        <div class="form-group">
                                                            {{ Form::text('', $rTickets[$loop->index]->options->trip_type.' | '.$rTickets[$loop->index]->order->options->return_route, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->options->trip_type.' | '.$rTickets[$loop->index]->order->options->return_route.' | Readonly')) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-3 col-form-label">Age Group</label>
                                                    <div class="col-9">
                                                        <div class="form-group">
                                                            {{ Form::text('', \App\Enums\OrderDetailType::getType($rTickets[$loop->index]->type), array('class' => 'form-control', 'readonly', 'title' => \App\Enums\OrderDetailType::getType($rTickets[$loop->index]->type).' | Readonly')) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-3 col-form-label">Name</label>
                                                    <div class="col-9">
                                                        <div class="form-group">
                                                            {{ Form::text('', $rTickets[$loop->index]->passenger->name, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->passenger->name.' | Readonly')) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label class="col-3 col-form-label">Passport Number</label>
                                                    <div class="col-9">
                                                        <div class="form-group">
                                                            {{ Form::text('', $rTickets[$loop->index]->passenger->passport_number, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->passenger->passport_number.' | Readonly')) }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="boarding_pass_return{{ $loop->index }}" class="col-3 col-form-label">Boarding Pass</label>
                                                    <div class="col-9">
                                                        <div class="form-group">
                                                            @if(isset($rTickets[$loop->index]->options->boarding_pass_number))
                                                                {{ Form::text('', $rTickets[$loop->index]->options->boarding_pass_number, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->options->boarding_pass_number.' | Readonly')) }}
                                                            @elseif(isset($rTickets[$loop->index]->options->error_description))
                                                                {{ Form::text('', $rTickets[$loop->index]->options->error_description, array('class' => 'form-control', 'readonly', 'title' => $rTickets[$loop->index]->options->error_description.' | Readonly')) }}
                                                            @else
                                                                {{ Form::text('', '', array('class' => 'form-control', 'readonly', 'title' => 'Readonly')) }}
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                @php
                                                    $eBP = isset($rTickets[$loop->index]->options->e_boarding_pass) ? $rTickets[$loop->index]->options->e_boarding_pass : null;
                                                    $eBPStatus = $eBP && ($eBP->eBPStatus == "True" || $eBP->eBPStatus == "true" || $eBP->eBPStatus === true) ? 'Yes' : 'No';
                                                @endphp
                                                <label class="col-3 col-form-label">E-BP</label>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        {{ Form::text('', $eBPStatus, array('class' => 'form-control', 'readonly', 'title' => $eBPStatus.' | Readonly')) }}
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
            @if ($wifiRental)
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Wifi Details</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-2 col-form-label">Wifi Device(s)</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $order->options->wifi_devices, array('class' => 'form-control', 'readonly', 'title' => $order->options->wifi_devices)) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Wifi Usage In</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $order->options->departure_from == 'singapore' ? 'Batam' : 'Singapore', array('class' => 'form-control', 'readonly', 'title' => $order->options->departure_from == 'singapore' ? 'Batam' : 'Singapore')) }}
                            </div>
                        </div>
                    </div>
                    @php
                    $startDate = \Carbon\Carbon::parse($order->options->wifi_start_date);
                    $endDate = \Carbon\Carbon::parse($order->options->wifi_end_date);
                    @endphp
                    <div class="row">
                        <label class="col-2 col-form-label">Wifi Rental Date</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $startDate->format('d M Y').' - '.$endDate->format('d M Y').' | '.($startDate->diffInDays($endDate) + 1).' day(s)', array('class' => 'form-control', 'readonly', 'title' => $startDate->format('d M Y').' - '.$endDate->format('d M Y').' | '.($startDate->diffInDays($endDate) + 1).' day(s)')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if ($carRental)
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Car Details</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-2 col-form-label">Selected Car</label>
                        <div class="col-10">
                            <div class="form-group">
                                @php
                                $str = ucwords(str_replace('_', ' ', $order->options->selected_car));
                                @endphp
                                {{ Form::text('', $str, array('class' => 'form-control', 'readonly', 'title' => $str)) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Pickup at</label>
                        <div class="col-10">
                            <div class="form-group">
                                {{ Form::text('', $order->options->car_checkpoint.' | '.$order->options->car_pick_up_at, array('class' => 'form-control', 'readonly', 'title' => $order->options->car_checkpoint.' | '.$order->options->car_pick_up_at)) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-2 col-form-label">Car Rental Date</label>
                        <div class="col-10">
                            <div class="form-group">
                                @php
                                $carStartDate = \Carbon\Carbon::parse($order->options->car_start_date);
                                $carEndDate = \Carbon\Carbon::parse($order->options->car_end_date);
                                @endphp
                                {{ Form::text('', $carStartDate->format('d M Y').' - '.$carEndDate->format('d M Y').' | '.($carStartDate->diffInDays($carEndDate) + 1).' day(s)', array('class' => 'form-control', 'readonly', 'title' => $carStartDate->format('d M Y').' - '.$carEndDate->format('d M Y').' | '.($carStartDate->diffInDays($carEndDate) + 1).' day(s)')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</form>
@endsection