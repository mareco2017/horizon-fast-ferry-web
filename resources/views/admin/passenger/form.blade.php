@extends('templates.admin.master')

@section('title')
    {{ $passenger->id ? 'Edit' : 'Create'}} Passenger @parent
@endsection
@section('groupName', 'Passenger')

@section('content')
@if (!$passenger->id)
{!! Form::open(['route' => 'admin.passenger.store', 'method' => 'post', 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($passenger, ['route' => ['admin.passenger.update', $passenger], 'method' => 'post', 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $passenger->id ? 'Edit' : 'Create' }} Passenger</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="passport_number" class="col-2 col-form-label">Passport Number</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('passport_number', old('passport_number'), array('class' => 'form-control', 'id' => 'passport_number', 'placeholder' => 'Passport Number')) }}
                        @if($errors->has('passport_number'))
                        <p class="status-text">{{ $errors->first('passport_number') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="name" class="col-2 col-form-label">Name</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::text('name', old('name'), array('class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name')) }}
                        </div>
                        @if($errors->has('name'))
                        <p class="status-text">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="nationality" class="col-2 col-form-label">Nationality</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::select('nationality', $nationalities, old('nationality'), array('class' => 'form-control', 'id' => 'nationality', 'placeholder' => 'Pick Nationality')) }}
                        </div>
                        @if($errors->has('nationality'))
                        <p class="status-text">{{ $errors->first('nationality') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="gender" class="col-2 col-form-label">Gender</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::select('gender', $genders, old('gender'), array('class' => 'form-control', 'id' => 'gender', 'placeholder' => 'Pick Gender')) }}
                        </div>
                        @if($errors->has('gender'))
                        <p class="status-text">{{ $errors->first('gender') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="dob" class="col-2 col-form-label">Date of Birth</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::text('dob', old('dob') ?? ($passenger->dob ? $passenger->dob->format('Y-m-d') : ''), array('class' => 'form-control', 'id' => 'dob', 'placeholder' => 'Ex: 1999-12-31')) }}
                        </div>
                        @if($errors->has('dob'))
                        <p class="status-text">{{ $errors->first('dob') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="issuing_country" class="col-2 col-form-label">Issuing Country</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::select('issuing_country', $issuingNationalities, old('issuing_country'), array('class' => 'form-control', 'id' => 'issuing_country', 'placeholder' => 'Pick Issuing Country')) }}
                        </div>
                        @if($errors->has('issuing_country'))
                        <p class="status-text">{{ $errors->first('issuing_country') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="passport_issue" class="col-2 col-form-label">Passport Issue</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::text('passport_issue', old('passport_issue') ?? ($passenger->passport_issue ? $passenger->passport_issue->format('Y-m-d') : ''), array('class' => 'form-control', 'id' => 'passport_issue', 'placeholder' => 'Ex: 1999-12-31')) }}
                        </div>
                        @if($errors->has('passport_issue'))
                        <p class="status-text">{{ $errors->first('passport_issue') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="passport_expiry" class="col-2 col-form-label">Passport Expiry</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::text('passport_expiry', old('passport_expiry') ?? ($passenger->passport_expiry ? $passenger->passport_expiry->format('Y-m-d') : ''), array('class' => 'form-control', 'id' => 'passport_expiry', 'placeholder' => 'Ex: 1999-12-31')) }}
                        </div>
                        @if($errors->has('passport_expiry'))
                        <p class="status-text">{{ $errors->first('passport_expiry') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="type" class="col-2 col-form-label">Age Type</label>
                <div class="col-10">
                    <div class="form-group">
                        <div id="wrapper">
                            {{ Form::select('type', $types, old('type'), array('class' => 'form-control', 'id' => 'type', 'placeholder' => 'Pick Age Type')) }}
                        </div>
                        @if($errors->has('type'))
                        <p class="status-text">{{ $errors->first('type') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection