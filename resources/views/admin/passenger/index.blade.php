@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    Passenger List @parent
@endsection

@section('groupName', 'Passenger')

@section('content')
@include('templates.modal.modal-confirm')
<div class="card card-chart">
    <div class="card-header">
        <h4 class="card-title">Passenger List</h3>
        <div class="dropdown">
            <button type="button" class="btn btn-round btn-default dropdown-toggle btn-simple btn-icon no-caret" data-toggle="dropdown" aria-expanded="false">
                <i class="now-ui-icons loader_gear"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="{{ route('admin.passenger.create') }}">Create new passenger</a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Passport Number</th>
                    <th>Name</th>
                    <th>Nationality</th>
                    <th>Gender</th>
                    <th>Date of Birth</th>
                    <th>Issuing Country</th>
                    <th>Passport Issue</th>
                    <th>Passport Expiry</th>
                    <th>Type</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.passenger.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'passport_number', name: 'passport_number', className: 'text-center' },
                { data: 'name', name: 'name', className: 'text-center' },
                { data: 'nationality', name: 'nationality', className: 'text-center' },
                { data: 'gender', name: 'gender', className: 'text-center' },
                { data: 'dob', name: 'dob', className: 'text-center' },
                { data: 'issuing_country', name: 'issuing_country', className: 'text-center' },
                { data: 'passport_issue', name: 'passport_issue', className: 'text-center' },
                { data: 'passport_expiry', name: 'passport_expiry', className: 'text-center' },
                { data: 'type', name: 'type', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
			]
		});
        $(table.table().container()).removeClass('form-inline');
	});
</script>
@endpush