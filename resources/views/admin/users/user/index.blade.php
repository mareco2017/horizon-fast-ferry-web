@extends('templates.admin.master')
@extends('templates.datatables.datatables')

@section('title')
    User List @parent
@endsection

@section('groupName', 'Users')

@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">User List</h3>
    </div>
    <div class="card-body">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered datatable dataTable" id="users-table" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
	$(document).ready(function(){
		var table = $('#users-table').DataTable(
		{
			processing: true,
			serverSide: true,
            responsive: true,
			ajax: {
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				url: '{!! route('admin.users.user.ajax') !!}',
				type: 'POST'
			},
			columns: [
                { data: 'id', name: 'id', className: 'text-center' },
                { data: 'first_name', name: 'first_name', className: 'text-center' },
                { data: 'phone_number', name: 'phone_number', className: 'text-center' },
                { data: 'email', name: 'email', className: 'text-center' },
                { data: 'action', name: 'action', className: 'text-center', sortable: false },
			]
		});
		$(table.table().container()).removeClass('form-inline');
	});
</script>
@endpush