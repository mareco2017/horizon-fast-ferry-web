@extends('templates.admin.master')

@section('title')
    Dashboard @parent
@endsection

@section('groupName', 'Dashboard')

@section('panel-class', 'panel-header-lg')

@section('panel')
<canvas id="bigDashboardChart"></canvas>
@endsection

@section('content')
<div class="card card-stats">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="statistics">
                    <div class="info">
                        <div class="icon icon-primary">
                            <i class="now-ui-icons users_circle-08"></i>
                        </div>
                        <h3 class="info-title">{{ $users }}</h3>
                        <h6 class="stats-title">Users</h6>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="statistics">
                    <div class="info">
                        <div class="icon icon-success">
                            <i class="now-ui-icons business_money-coins"></i>
                        </div>
                        <h3 class="info-title">{{ $orders }}</h3>
                        <h6 class="stats-title">Orders</h6>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="statistics">
                    <div class="info">
                        <div class="icon icon-info">
                            <i class="now-ui-icons education_paper"></i>
                        </div>
                        <h3 class="info-title">{{ $articles }}</h3>
                        <h6 class="stats-title">Articles</h6>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="statistics">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="now-ui-icons business_badge"></i>
                        </div>
                        <h3 class="info-title">{{ $passengers }}</h3>
                        <h6 class="stats-title">Registered Passengers</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('pageRelatedJs')
<script>
$(document).ready(function() {
    var dates = {!! json_encode($dates) !!};
    var datas = {!! json_encode($datas) !!};

    var chartColor = "#FFFFFF";
    var ctx = document.getElementById('bigDashboardChart').getContext("2d");

    var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    gradientStroke.addColorStop(0, '#80b6f4');
    gradientStroke.addColorStop(1, chartColor);

    var gradientFill = ctx.createLinearGradient(0, 200, 0, 50);
    gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
    gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: dates,
            datasets: [{
                label: "Order",
                data: datas,
                borderColor: chartColor,
                pointBorderColor: chartColor,
                pointBackgroundColor: "#1e3d60",
                pointHoverBackgroundColor: "#1e3d60",
                pointHoverBorderColor: chartColor,
                pointBorderWidth: 1,
                pointHoverRadius: 7,
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                fill: true,
                backgroundColor: gradientFill,
                borderWidth: 2,
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Order Chart',
                fontFamily: '"Montserrat", "Helvetica Neue", Arial, sans-serif',
                fontColor: '#fff',
                fontStyle: 'normal',
                fontSize: 14,
            },
            layout: {
                padding: {
                    left: 20,
                    right: 30,
                    top: 0,
                    bottom: 0
                }
            },
            maintainAspectRatio: false,
            tooltips: {
            backgroundColor: '#fff',
            titleFontColor: '#333',
            bodyFontColor: '#666',
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
            },
            legend: {
                position: "bottom",
                fillStyle: "#FFF",
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 10
                    },
                    gridLines: {
                        drawTicks: true,
                        drawBorder: false,
                        display: true,
                        color: "rgba(255,255,255,0.1)",
                        zeroLineColor: "transparent"
                    }

                }],
                xAxes: [{
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: false,

                    },
                    ticks: {
                        padding: 10,
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold"
                    }
                }]
            }
        }
    });
});
</script>
@endpush