@extends('templates.admin.master')

@section('title')
    {{ $article->id ? 'Edit' : 'Create'}} Article @parent
@endsection
@section('groupName', 'Articles')

@section('content')
@if (!$article->id)
{!! Form::open(['route' => 'admin.article.store', 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@else
{!! Form::model($article, ['route' => ['admin.article.update', $article], 'method' => 'post', 'files' => true, 'class' => 'form-horizontal']) !!}
@endif
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{ $article->id ? 'Edit' : 'Create' }} Article</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <label for="title" class="col-2 col-form-label">Title</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('title', old('title'), array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Title', 'required')) }}
                        @if($errors->has('title'))
                        <p class="status-text">{{ $errors->first('title') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="tags" class="col-2 col-form-label">Tags</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::text('tags', old('tags'), array('class' => 'form-control v-hidden', 'id' => 'tags', 'placeholder' => 'Tags')) }}
                        @if($errors->has('tags'))
                        <p class="status-text">{{ $errors->first('tags') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="intro" class="col-2 col-form-label">Intro</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('intro', old('intro'), array('class' => 'form-control', 'id' => 'intro', 'placeholder' => 'Intro Text')) }}
                        @if($errors->has('intro'))
                        <p class="status-text">{{ $errors->first('intro') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="status" class="col-2 col-form-label">Status</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::select('status', $statusList, old('status'), array('class' => 'form-control', 'id' => 'status', 'placeholder' => 'Select Status')) }}
                        @if($errors->has('status'))
                        <p class="status-text">{{ $errors->first('status') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="desc" class="col-2 col-form-label">Description</label>
                <div class="col-10">
                    <div class="form-group">
                        {{ Form::textarea('desc', old('desc'), array('class' => 'form-control v-hidden', 'id' => 'desc', 'placeholder' => 'Description')) }}
                        @if($errors->has('desc'))
                        <p class="status-text">{{ $errors->first('desc') }}</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="col-2 col-form-label">Cover</label>
                <div class="col-10">
                    <div class="form-group">
                        <div class="fileinput text-center fileinput-new d-inline-block" data-provides="fileinput">
                            <div class="fileinput-new thumbnail">
                                <img src="{{ $article->id && $article->cover_url ? $article->cover_url : asset('/assets/images/img-placeholder.png') }}">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                            <div>
                                <span class="btn btn-primary btn-round btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists"><i class="fas fa-pencil-alt"></i> Change</span>
                                    {{ Form::file('cover') }}
                                </span>
                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fas fa-times-circle"></i> Remove</a>
                            </div>
                        </div>
                        @if($errors->has('cover'))
                        <p class="status-text">{{ $errors->first('cover') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-center">
                <button type="submit" class="btn btn-round btn-info"><i class="fas fa-paper-plane"></i> Submit</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@push('pageRelatedCss')
<link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.tagsinput.min.css') }}"/>
@endpush

@push('pageRelatedJs')
<script src="{{ asset('/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/jquery.tagsinput.min.js') }}"></script>
<script src="{{ asset('/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#tags').tagsInput({
        'width': '100%',
        'defaultText': 'New Tag',
    });

    CKEDITOR.replace('desc', {
        height: 500
    });
});
</script>
@endpush