@section('header')
<header class="header fixed-top d-flex {{ $type ?? '' }}">
    <div class="container d-flex flex-column justify-content-center">
        <div class="d-flex align-items-center py-1">
            <a href="/" data-rippleria data-rippleria-color="rgba(0, 0, 0, 0.35)" data-rippleria-duration="500" class="d-flex align-items-center m-auto mx-xl-0">
                <div class="navbar-brand logo mr-0"></div>
            </a>
            <div class="d-none d-xl-flex justify-content-between navbar-content pl-5">
                <div class="d-flex align-items-center tab-list">
                    <a class="py-4 px-3 active" href="#"> 
                        <p class="mb-0">HOME</p> 
                    </a>
                    <a class="py-4 px-3" href="#"> 
                        <p class="mb-0">BATAM HIGHLIGHTS</p> 
                    </a>
                    <a class="py-4 px-3" href="#"> 
                        <p class="mb-0">TRAVEL INFORMATION</p> 
                    </a>
                    <a class="py-4 px-3" href="#"> 
                        <p class="mb-0">CONTACT US</p> 
                    </a>
                </div>
                <div class="d-flex align-items-center">
                    <a class="py-4 px-3 text-green" href="#"> 
                        <div class="d-flex align-items-center">
                            <i class="fas fa-user fs-3 mr-2"></i><p class="mb-0">LOGIN</p>
                        </div>
                    </a>
                    <a class="py-4 px-3 text-red f-bold" href="#"> 
                        <p class="mb-0">SIGN UP</p> 
                    </a>
                </div>
                <div class="d-flex align-items-center">
                    <a class="py-4 px-3 text-blue" href="#"> 
                        <i class="fas fa-search fs-3"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection