<table border="1">
    <thead>
    <tr>
        <th rowspan="2">Transaction ID</th>
        <th rowspan="2">Website Booking Reference</th>
        <th colspan="2">Contact Person</th>
        <th rowspan="2">Created Date</th>
        <th colspan="3">Payment Details</th>
        <th colspan="3">Purchase Details</th>
        <th colspan="4">Passenger Details</th>
        <th colspan="4">Departure Details</th>
        <th colspan="4">Return Details</th>
    </tr>
    <tr>
        <th>Email Address</th>
        <th>Phone Number</th>

        <th>Stripe Payment ID</th>
        <th>Amount</th>
        <th>Fee</th>

        <th>Qty</th>
        <th>Trip Type</th>
        <th>Seat Type</th>

        <th>Passport No</th>
        <th>Passenger Full Name</th>
        <th>Nationality</th>
        <th>Age Category</th>

        <th>Departure Point</th>
        <th>Transaction Reference</th>
        <th>Departure Schedule</th>
        <th>Departure BP No.</th>

        <th>Return Point</th>
        <th>Transaction Reference</th>
        <th>Return Schedule</th>
        <th>Return BP No.</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        @php
            $departDetails = $order->details()->departure()->get();
            $returnDetails = $order->details()->return()->get();
            $length = $departDetails->count();
        @endphp
        @foreach($departDetails as $detail)
        <tr>
            @if ($loop->first)
            <td rowspan="{{ $length }}">{{ $order->id }}</td>
            <td rowspan="{{ $length }}">{{ $order->reference_number }}</td>

            <td rowspan="{{ $length }}">{{ $order->contactPerson->email }}</td>
            <td rowspan="{{ $length }}">{{ $order->contactPerson->phone_number }}</td>

            <td rowspan="{{ $length }}">{{ $order->created_at->timezone('Asia/Jakarta')->format('l, F j, Y g:i:s A') }}</td>

            <td rowspan="{{ $length }}">{{ $order->payment ? $order->payment->id : '-' }}</td>
            <td rowspan="{{ $length }}">{{ $order->total ? $order->total : '' }}</td>
            <td rowspan="{{ $length }}">{{ $order->total ? ($order->total * 3.4/100) + 0.5 : '' }}</td>

            <td rowspan="{{ $length }}">{{ $length }}</td>
            <td rowspan="{{ $length }}">{{ $order->options->two_way ? 'Round Trip' : 'One Way' }}</td>
            <td rowspan="{{ $length }}">{{ TicketType::getString($order->options->type) }}</td>
            @endif
            
            @if ($detail->passenger)
            <td>{{ $detail->passenger->passport_number }}</td>
            <td>{{ $detail->passenger->name }}</td>
            <td>{{ $detail->passenger->nationality }}</td>
            <td>{{ PassengerType::getString($detail->passenger->type) }}</td>
            @else
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            @endif

            @if ($loop->first)
            <td rowspan="{{ $length }}">{{ ucfirst($order->options->departure_from) }}</td>
            <td rowspan="{{ $length }}">{{ isset($order->options->departure_booking_reference) ? $order->options->departure_booking_reference : '-' }}</td>
            <td rowspan="{{ $length }}">{{ \Carbon\Carbon::parse($order->options->departure_date)->format('d M Y') }} @ {{ $order->options->departure_time }}</td>
            @endif
            <td>{{ isset($detail->options->boarding_pass_number) ? $detail->options->boarding_pass_number : '-' }}</td>

            @if ($loop->first)
            <td rowspan="{{ $length }}">{{ ucfirst($order->options->return_from) }}</td>
            <td rowspan="{{ $length }}">{{ isset($order->options->return_booking_reference) ? $order->options->return_booking_reference : '-' }}</td>
            <td rowspan="{{ $length }}">{{ \Carbon\Carbon::parse($order->options->return_date)->format('d M Y') }} @ {{ $order->options->return_time }}</td>
            @endif
            <td>{{ isset($returnDetails[$loop->index]->options->boarding_pass_number) ? $returnDetails[$loop->index]->options->boarding_pass_number : '-' }}</td>
        </tr>
        @endforeach
    @endforeach
    </tbody>
</table>

<br>

<table border="1">
    <tr>
        <td rowspan="3">Payment Data</td>
        <td>Payment Received:</td>
        <td>(Enable edit to view data)</td>
    </tr>
    <tr>
        <td>Payment Fee:</td>
        <td>(Enable edit to view data)</td>
    </tr>
    <tr>
        <td>Net Received:</td>
        <td>(Enable edit to view data)</td>
    </tr>
</table>

<table border="1">
    <tr>
        <td rowspan="10">Selling details(Adult)</td>
        <td rowspan="4">Departure from Singapore</td>
        <td rowspan="2">One Way</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['AdSgOwEc'] == 0 ? '' : $thirdTableData['AdSgOwEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['AdSgOwBc'] == 0 ? '' : $thirdTableData['AdSgOwBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="2">Round Trip</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['AdSgRtEc'] == 0 ? '' : $thirdTableData['AdSgRtEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['AdSgRtBc'] == 0 ? '' : $thirdTableData['AdSgRtBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="6">Departure from Batam</td>
        <td rowspan="2">One Way</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['AdBaOwEc'] == 0 ? '' : $thirdTableData['AdBaOwEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['AdBaOwBc'] == 0 ? '' : $thirdTableData['AdBaOwBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="4">Round Trip</td>
        <td>Economy - Indonesian</td>
        <td>{{ $thirdTableData['AdBaRtEcId'] == 0 ? '' : $thirdTableData['AdBaRtEcId'].' pax' }}</td>
    </tr>
    <tr>
        <td>Economy - Foreign</td>
        <td>{{ $thirdTableData['AdBaRtEcFo'] == 0 ? '' : $thirdTableData['AdBaRtEcFo'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class - Indonesian</td>
        <td>{{ $thirdTableData['AdBaRtBcId'] == 0 ? '' : $thirdTableData['AdBaRtBcId'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class - Foreign</td>
        <td>{{ $thirdTableData['AdBaRtBcFo'] == 0 ? '' : $thirdTableData['AdBaRtBcFo'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="10">Selling details(Infant)</td>
        <td rowspan="4">Departure from Singapore</td>
        <td rowspan="2">One Way</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['InSgOwEc'] == 0 ? '' : $thirdTableData['InSgOwEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['InSgOwBc'] == 0 ? '' : $thirdTableData['InSgOwBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="2">Round Trip</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['InSgRtEc'] == 0 ? '' : $thirdTableData['InSgRtEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['InSgRtBc'] == 0 ? '' : $thirdTableData['InSgRtBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="6">Departure from Batam</td>
        <td rowspan="2">One Way</td>
        <td>Economy</td>
        <td>{{ $thirdTableData['InBaOwEc'] == 0 ? '' : $thirdTableData['InBaOwEc'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class</td>
        <td>{{ $thirdTableData['InBaOwBc'] == 0 ? '' : $thirdTableData['InBaOwBc'].' pax' }}</td>
    </tr>
    <tr>
        <td rowspan="4">Round Trip</td>
        <td>Economy - Indonesian</td>
        <td>{{ $thirdTableData['InBaRtEcId'] == 0 ? '' : $thirdTableData['InBaRtEcId'].' pax' }}</td>
    </tr>
    <tr>
        <td>Economy - Foreign</td>
        <td>{{ $thirdTableData['InBaRtEcFo'] == 0 ? '' : $thirdTableData['InBaRtEcFo'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class - Indonesian</td>
        <td>{{ $thirdTableData['InBaRtBcId'] == 0 ? '' : $thirdTableData['InBaRtBcId'].' pax' }}</td>
    </tr>
    <tr>
        <td>Business Class - Foreign</td>
        <td>{{ $thirdTableData['InBaRtBcFo'] == 0 ? '' : $thirdTableData['InBaRtBcFo'].' pax' }}</td>
    </tr>
    <tr>
        <td colspan="4">Total Pax</td>
        <td>{{ array_sum($thirdTableData) }} pax</td>
    </tr>
</table>