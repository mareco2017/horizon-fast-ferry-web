<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <style>
    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 400;
        src: url({{ storage_path('fonts\OpenSans-Regular.ttf') }}) format('truetype');
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: 600;
        src: url({{ storage_path('fonts\OpenSans-SemiBold.ttf') }}) format('truetype');
    }

    @font-face {
        font-family: 'Open Sans';
        font-style: normal;
        font-weight: bold;
        src: url({{ storage_path('fonts\OpenSans-Bold.ttf') }}) format('truetype');
    }
    
    html {
        font-size: 8px;
    }

    @page {
        margin: 0;
    }

    body {
        margin: 40px 0;
        padding: 0;
        background: #fff;
        font-family: 'Open Sans', sans-serif;
        font-weight: 400;
        line-height: 1;
    }

    .pdf-container {
        width: 400px;
        margin: auto;
        border-radius: 15px;
        overflow: hidden;
        border: 1px solid #919191;
        min-height: 670px;
    }

    .pdf-logo-container {
        background-color: #0045a8;
        padding: 1.5rem;
    }

    .pdf-img {
        width: 52px;
        vertical-align: middle;
    }

    .pdf-title {
        margin-left: 30px;
        color: #a6a6a9;
        display: inline-block;
        font-size: 2rem;
    }

    p {
        margin: 0;
    }

    .label-text {
        color: #666666;
        margin-bottom: .25rem;
        font-size: 1rem;
    }

    .pad-x {
        padding-left: 1.5rem;
        padding-right: 1.5rem;
    }

    .pad-xy {
        padding: .5rem 1.5rem;
    }
    
    .place-text {
        color: #0045a8;
        font-size: 3rem;
        font-weight: bold;
    }

    .ship-img {
        /* max-width: none; */
        padding: .4rem 1rem 0;
        width: 180px;
        /* max-height: 50px; */
        /* width: 100%; */
    }

    hr {
        border: 0;
        border-top: 1px solid #b1b1b4;
        margin: 1rem 1.5rem;
    }

    .value-text {
        font-size: 1.5rem;
        margin-bottom: .5rem;
    }

    .table-info {
        padding: 0 1.5rem;
    }

    .table-info td {
        padding: 0 1rem;
        border-right: 1px solid #919191;
    }

    .table-info td:first-child {
        padding-left: 0;
    }

    .table-info td:last-child {
        padding-right: 0;
        border-right: 0;
    }

    .booking-ref {
        padding: 0 1.5rem .75rem;
    }

    .table-qrcode {
        padding: 3rem 1.5rem;
    }

    .barcode {
        padding: 2rem;
    }

    .barcode-img {
        /* width: 174px; */
        height: 70px;
    }

    .not-eligible {
        margin: 200px 0 200px 30px;
        transform: rotate(-45deg);
        font-size: 5rem;
    }

    td.td-vertical-line {
        width: 1px;
    }

    .vertical-line {
        height: 800px;
        border-left: dashed 1px #000;
        margin: -40px 0;
    }
    </style>
</head>
<body>
    <main>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    @if ($dEBP)
                    <div class="pdf-container">
                        <table cellpadding="0" cellspacing="0" class="pdf-logo-container" width="100%">
                            <tr>
                                <td width="72">
                                    <img class="pdf-img" src="{{ asset('assets/icons/logo-alt.png') }}" alt="">
                                </td>
                                <td>
                                    <div class="pdf-title">E-Boarding Pass</div>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-qrcode">
                            <tr>
                                <td align="center">
                                    <img width="160px" src="https://barcode.tec-it.com/barcode.ashx?data={{ $dEBP->bpNo }}&code=QRCode&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=240&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L" alt="QR Code Generator TEC-IT">
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="pad-x">
                            <tr>
                                <td>
                                    <p class="label-text">
                                        @if ($dEBP->from == 'batam')
                                            Batam
                                            <br/>
                                            Harbour Bay
                                        @else
                                            Singapore
                                            <br/>
                                            Harbour Front Centre
                                        @endif
                                    </p>
                                </td>
                                <td align="right">
                                    <p class="label-text">
                                        @if ($dEBP->from == 'batam')
                                            Singapore
                                            <br/>
                                            Harbour Front Centre
                                        @else
                                            Batam
                                            <br/>
                                            Harbour Bay
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="pad-x">
                            <tr valign="middle">
                                <td>
                                    <p class="place-text">
                                    @if ($dEBP->from == 'batam')
                                        BTM
                                    @else
                                        SG
                                    @endif
                                    </p>
                                </td>
                                <td align="center">
                                    <img class="ship-img" src="{{ asset('/assets/images/ship.png') }}" alt="">
                                </td>
                                <td align="right">
                                    <p class="place-text">
                                    @if ($dEBP->from == 'batam')
                                        SG
                                    @else
                                        BTM
                                    @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td>
                                    <p class="label-text">Departure Date</p>
                                    <p class="value-text">{{ $dEBP->date }}</p>
                                </td>
                                <td>
                                    <p class="label-text">Departure Time</p>
                                    <p class="value-text">{{ $dEBP->time }}</p>
                                </td>
                                <td>
                                    <p class="label-text">Destination</p>
                                    <p class="value-text">
                                        @if ($dEBP->from == 'batam')
                                            Harbour Front Centre
                                        @else
                                            Harbour Bay
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td>
                                    <p class="label-text">Passenger Name</p>
                                    <p class="value-text">{{ strtoupper($dEBP->eBP->passengerName) }}</p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td width="40%">
                                    <p class="label-text">Passport No.</p>
                                    <p class="value-text">{{ strtoupper($dEBP->eBP->passportNo) }}</p>
                                </td>
                                <td width="30%">
                                    <p class="label-text">Class</p>
                                    <p class="value-text">{{ $dEBP->seatClass }}</p>
                                </td>
                                <td width="30%">
                                    <p class="label-text">Gate open</p>
                                    <p class="value-text">{{ $dEBP->eBP->gateOpenTime }}</p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td width="40%">
                                    <p class="label-text">Boarding Time.</p>
                                    <p class="value-text">{{ $dEBP->eBP->boardingTime }}</p>
                                </td>
                                <td width="60%">
                                    <p class="label-text">Gate closed at</p>
                                    <p class="value-text">{{ $dEBP->eBP->gateCloseTime }}</p>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="barcode">
                            <tr>
                                <td align="center">
                                    <img class="barcode-img" src="https://barcode.tec-it.com/barcode.ashx?data={{ $dEBP->bpNo }}&code=Code128&dpi=96&dataseparator=" alt="Barcode Generator TEC-IT"/>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="booking-ref">
                            <tr valign="middle">
                                <td colSpan="2">
                                    <p class="label-text">Booking No. {{ $refNumber }}</p>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td width="60%">
                                    <p class="label-text">Ticket No. {{ $dEBP->ticketNo }}</p>
                                </td>
                                <td width="40%" align="right">
                                    <p class="label-text">BP: {{ $dEBP->bpNo }}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    @else
                    <div class="pdf-container pad-x">
                        <h1 class="not-eligible">[Depart]<br/>E-Boarding Pass<br/>not eligible</h1>
                    </div>
                    @endif
                </td>
                @if ($twoWay)
                <td class="td-vertical-line">
                    <div class="vertical-line"></div>
                </td>
                <td>
                    @if ($rEBP)
                    <div class="pdf-container">
                        <table cellpadding="0" cellspacing="0" class="pdf-logo-container" width="100%">
                            <tr>
                                <td width="72">
                                    <img class="pdf-img" src="{{ asset('assets/icons/logo-alt.png') }}" alt="">
                                </td>
                                <td>
                                    <div class="pdf-title">E-Boarding Pass</div>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-qrcode">
                            <tr>
                                <td align="center">
                                    <img width="160px" src="https://barcode.tec-it.com/barcode.ashx?data={{ $rEBP->bpNo }}&code=QRCode&multiplebarcodes=false&translate-esc=false&unit=Fit&dpi=240&imagetype=Gif&rotation=0&color=%23000000&bgcolor=%23ffffff&qunit=Mm&quiet=0&eclevel=L" alt="QR Code Generator TEC-IT">
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="pad-x">
                            <tr>
                                <td>
                                    <p class="label-text">
                                        @if ($rEBP->from == 'batam')
                                            Batam
                                            <br/>
                                            Harbour Bay
                                        @else
                                            Singapore
                                            <br/>
                                            Harbour Front Centre
                                        @endif
                                    </p>
                                </td>
                                <td align="right">
                                    <p class="label-text">
                                        @if ($rEBP->from == 'batam')
                                            Singapore
                                            <br/>
                                            Harbour Front Centre
                                        @else
                                            Batam
                                            <br/>
                                            Harbour Bay
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="pad-x">
                            <tr valign="middle">
                                <td>
                                    <p class="place-text">
                                    @if ($rEBP->from == 'batam')
                                        BTM
                                    @else
                                        SG
                                    @endif
                                    </p>
                                </td>
                                <td align="center">
                                    <img class="ship-img" src="{{ asset('/assets/images/ship.png') }}" alt="">
                                </td>
                                <td align="right">
                                    <p class="place-text">
                                    @if ($rEBP->from == 'batam')
                                        SG
                                    @else
                                        BTM
                                    @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td>
                                    <p class="label-text">Departure Date</p>
                                    <p class="value-text">{{ $rEBP->date }}</p>
                                </td>
                                <td>
                                    <p class="label-text">Departure Time</p>
                                    <p class="value-text">{{ $rEBP->time }}</p>
                                </td>
                                <td>
                                    <p class="label-text">Destination</p>
                                    <p class="value-text">
                                        @if ($rEBP->from == 'batam')
                                            Harbour Front Centre
                                        @else
                                            Harbour Bay
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td>
                                    <p class="label-text">Passenger Name</p>
                                    <p class="value-text">{{ strtoupper($rEBP->eBP->passengerName) }}</p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td width="40%">
                                    <p class="label-text">Passport No.</p>
                                    <p class="value-text">{{ strtoupper($rEBP->eBP->passportNo) }}</p>
                                </td>
                                <td width="30%">
                                    <p class="label-text">Class</p>
                                    <p class="value-text">{{ $rEBP->seatClass }}</p>
                                </td>
                                <td width="30%">
                                    <p class="label-text">Gate open</p>
                                    <p class="value-text">{{ $rEBP->eBP->gateOpenTime }}</p>
                                </td>
                            </tr>
                        </table>
                        <hr/>
                        <table cellpadding="0" cellspacing="0" width="100%" class="table-info">
                            <tr valign="top">
                                <td width="40%">
                                    <p class="label-text">Boarding Time.</p>
                                    <p class="value-text">{{ $rEBP->eBP->boardingTime }}</p>
                                </td>
                                <td width="60%">
                                    <p class="label-text">Gate closed at</p>
                                    <p class="value-text">{{ $rEBP->eBP->gateCloseTime }}</p>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="barcode">
                            <tr>
                                <td align="center">
                                    <img class="barcode-img" src="https://barcode.tec-it.com/barcode.ashx?data={{ $rEBP->bpNo }}&code=Code128&dpi=96&dataseparator=" alt="Barcode Generator TEC-IT"/>
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" width="100%" class="booking-ref">
                            <tr valign="middle">
                                <td colSpan="2">
                                    <p class="label-text">Booking No. {{ $refNumber }}</p>
                                </td>
                            </tr>
                            <tr valign="middle">
                                <td width="60%">
                                    <p class="label-text">Ticket No. {{ $rEBP->ticketNo }}</p>
                                </td>
                                <td width="40%" align="right">
                                    <p class="label-text">BP: {{ $rEBP->bpNo }}</p>
                                </td>
                            </tr>
                        </table>
                    </div>
                    @else
                    <div class="pdf-container pad-x">
                        <h1 class="not-eligible">[Return]<br/>E-Boarding Pass<br/>not eligible</h1>
                    </div>
                    @endif
                </td>
                @endif
            </tr>
        </table>
    </main>
</body>
</html>