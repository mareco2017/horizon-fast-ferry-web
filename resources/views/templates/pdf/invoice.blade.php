<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ public_path('css/styles.css') }}">
    <style>
    html {
        font-size: 14px;
    }

    @page {
        margin: 0;
    }

    body {
        margin: 87px 0 108px 0;
        padding: 32px 36px 32px;
        background: #fff;
        font-family: 'Open Sans', sans-serif;
    }

    header {
        position: fixed;
        top: 0px; 
        left: 0px; 
        right: 0px;
        z-index: 1;

        background-color: #0045a8;
        padding: 16px;
        text-align: center;
    }

    footer {
        position: fixed;
        bottom: -60px; 
        left: 0px; 
        right: 0px;
        height: 120px; 
        z-index: 1;

        background-color: #0045a8;
        padding: 26px;
        color: #fff !important;
        /* position: relative; */
    }

    .footer-text {
        margin-bottom: 0.35rem;
    }

    .pdf-container {
        position: relative;
    }

    .pif-img {
        width: 190px;
        height: auto;
        position: absolute;
        top: -16px;
        right: 0;
    }

    p {
        margin: 0 0 1rem 0;
    }

    .page-break {
        page-break-after: always;
    }

    .block {
        margin-bottom: 2rem;
    }

    .padded-right {
        padding-right: 2rem;
    }

    .padded-left {
        padding-left: 5rem;
    }

    .mb-0 {
        margin-bottom: 0;
    }

    .d-inline-block {
        display: inline-block;
    }

    table {
        border-collapse: collapse;
    }

    table.purchase, table.purchase th, table.purchase td {
        border: 1px solid #0045a8;
    }

    table.purchase {
        width: 100%;
    }

    table.purchase th {
        background-color: #ebebeb;
    }

    table.purchase th,
    table.purchase td {
        text-align: center;
    }

    .text-right {
        text-align: right;
    }

    .text-capitalize {
        text-transform: capitalize;
    }
    </style>
</head>
<body>
    <header>
        <img src="{{ public_path('assets/icons/logo-alt.png') }}" width="92px" alt="logo">
    </header>

    <footer>
        <p class="footer-text"><b>Horizon Fast Ferry (previously known as Prima Ferries)</b></p>
        <p class="footer-text">1 Maritime Square #03-47 Harbourfront Centre, Singapore 099253</p>
        <p><b>T</b> +65 6276 6711 <b>F</b> +65 6276 6551 <b>W</b> www.horizonfastferry.com</p>
    </footer>
    
    <main>
        <div class="pdf-container">
            <img class="pif-img" src="{{ public_path('assets/icons/paid-in-full.png') }}" alt="paid-in-full">
            <div class="block">
                <p class="text-blue fs-2"><b>RECEIPT</b></p>
                <p class="mb-0">Date: {{ \Carbon\Carbon::now()->format('d/m/y') }}</p>
            </div>
            <div class="block">
                <table class="mb-0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="top">
                            <p class="fs-2"><b>CUSTOMER DETAIL</b></p>
                            <table class="mb-0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <p class="padded-right mb-0">Email</p>
                                        <p class="padded-right mb-0">Contact Number</p>
                                    </td>
                                    <td>
                                        <p class="mb-0">: {{ $contactPerson->email }}</p>
                                        <p class="mb-0">: {{ $contactPerson->phone_number }}</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" class="padded-left">
                            <p class="fs-2"><b>PAYMENT DETAIL</b></p>
                            <table class="mb-0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <p class="padded-right mb-0">P.O Number</p>
                                        <p class="padded-right mb-0">Payment Method</p>
                                        <p class="padded-right mb-0">Status</p>
                                    </td>
                                    <td>
                                        <p class="mb-0">: {{ $order->reference_number }}</p>
                                        <p class="mb-0">: Credit Card</p>
                                        <p class="mb-0">: Paid</p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="block">
                <p class="fs-2"><b>PASSENGER DETAILS</b></p>
                <p class="mb-0">
                    @if (isset($order->options->wifi_only) && $order->options->wifi_only && $wifiRental)
                        <p> {{ $wifiRental->passenger->name }} </p>
                    @elseif (isset($order->options->car_only) && $order->options->car_only && $carRental)
                        <p> {{ $carRental->passenger->name }} </p>
                    @else
                        <p> 
                        @foreach($dTickets->sortBy('passenger.name') as $t)
                            {{ $t->passenger->name }}{{ $loop->last ? '' : ', ' }}
                        @endforeach
                        </p>
                    @endif
                </p>
            </div>
            <div class="block">
                <p class="text-blue fs-2"><b>PURCHASE DETAILS</b></p>
                <table class="purchase" cellspacing="0" cellpadding="8px">
                    <tr>
                        <th>Type of item</th>
                        <th>Item description</th>
                        <th width="10%">Qty</th>
                        <th width="11%">Price<br>per unit (SGD)</th>
                        <th width="11%">Total price (SGD)</th>
                    </tr>
                    @if ($adultLocalTicket && count($adultLocalTicket) > 0)
                        @php
                            $quantity = count($adultLocalTicket);
                            $pricePerUnit = $adultLocalTicket[0]->price - $adultLocalTicket[0]->discount + $adultLocalTicket[0]->terminal_fee + $adultLocalTicket[0]->surcharge + $adultLocalTicket[0]->upgrade_business;
                            if ($order->options->two_way) $pricePerUnit *= 2;
                        @endphp
                        <tr>
                            <td>{{ TicketType::getString($order->options->type) }} Class Ferry Ticket(s)<br>Adult{{ $order->options->departure_from == 'batam' ? ' | Indonesian' : '' }}</td>
                            <td>
                                <span class="no-wrap"><b>Depart:</b><span class="text-capitalize"> {{ $order->options->departure_from }}, </span> {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d/m/y').' | '.$order->options->departure_time }}</span>
                                <br>
                                <span class="no-wrap"><b>Return:</b>
                                    @if ($order->options->two_way)
                                    <span class="text-capitalize"> {{ $order->options->return_from }}, </span> {{ \Carbon\Carbon::parse($order->options->return_date)->format('d/m/y').' | '.$order->options->return_time }}
                                    @else
                                    <span>#N/A#</span>
                                    @endif
                                </span>
                            </td>
                            <td>{{ $quantity }}</td>
                            <td>${{ $pricePerUnit  }}</td>
                            <td>${{ $quantity * $pricePerUnit }}</td>
                        </tr>
                    @endif
                    @if ($adultForeignTicket && count($adultForeignTicket) > 0)
                        @php
                            $quantity = count($adultForeignTicket);
                            $pricePerUnit = $adultForeignTicket[0]->price - $adultForeignTicket[0]->discount + $adultForeignTicket[0]->terminal_fee + $adultForeignTicket[0]->surcharge + $adultForeignTicket[0]->upgrade_business;
                            if ($order->options->two_way) $pricePerUnit *= 2;
                        @endphp
                        <tr>
                            <td>{{ TicketType::getString($order->options->type) }} Class Ferry Ticket(s)<br>Adult{{ $order->options->departure_from == 'batam' ? ' | Foreign' : '' }}</td>
                            <td>
                                <span class="no-wrap"><b>Depart:</b><span class="text-capitalize"> {{ $order->options->departure_from }}, </span> {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d/m/y').' | '.$order->options->departure_time }}</span>
                                <br>
                                <span class="no-wrap"><b>Return:</b>
                                    @if ($order->options->two_way)
                                    <span class="text-capitalize"> {{ $order->options->return_from }}, </span> {{ \Carbon\Carbon::parse($order->options->return_date)->format('d/m/y').' | '.$order->options->return_time }}
                                    @else
                                    <span>#N/A#</span>
                                    @endif
                                </span>
                            </td>
                            <td>{{ $quantity }}</td>
                            <td>${{ $pricePerUnit  }}</td>
                            <td>${{ $quantity * $pricePerUnit }}</td>
                        </tr>
                    @endif
                    @if ($infantLocalTicket && count($infantLocalTicket) > 0)
                        @php
                            $quantity = count($infantLocalTicket);
                            $pricePerUnit = $infantLocalTicket[0]->price - $infantLocalTicket[0]->discount + $infantLocalTicket[0]->terminal_fee + $infantLocalTicket[0]->surcharge + $infantLocalTicket[0]->upgrade_business;
                            if ($order->options->two_way) $pricePerUnit *= 2;
                        @endphp
                        <tr>
                            <td>{{ TicketType::getString($order->options->type) }} Class Ferry Ticket(s)<br>Infant{{ $order->options->departure_from == 'batam' ? ' | Indonesian' : '' }}</td>
                            <td>
                                <span class="no-wrap"><b>Depart:</b><span class="text-capitalize"> {{ $order->options->departure_from }}, </span> {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d/m/y').' | '.$order->options->departure_time }}</span>
                                <br>
                                <span class="no-wrap"><b>Return:</b>
                                    @if ($order->options->two_way)
                                    <span class="text-capitalize"> {{ $order->options->return_from }}, </span> {{ \Carbon\Carbon::parse($order->options->return_date)->format('d/m/y').' | '.$order->options->return_time }}
                                    @else
                                    <span>#N/A#</span>
                                    @endif
                                </span>
                            </td>
                            <td>{{ $quantity }}</td>
                            <td>${{ $pricePerUnit  }}</td>
                            <td>${{ $quantity * $pricePerUnit }}</td>
                        </tr>
                    @endif
                    @if ($infantForeignTicket && count($infantForeignTicket) > 0)
                        @php
                            $quantity = count($infantForeignTicket);
                            $pricePerUnit = $infantForeignTicket[0]->price - $infantForeignTicket[0]->discount + $infantForeignTicket[0]->terminal_fee + $infantForeignTicket[0]->surcharge + $infantForeignTicket[0]->upgrade_business;
                            if ($order->options->two_way) $pricePerUnit *= 2;
                        @endphp
                        <tr>
                            <td>{{ TicketType::getString($order->options->type) }} Class Ferry Ticket(s)<br>Infant{{ $order->options->departure_from == 'batam' ? ' | Foreign' : '' }}</td>
                            <td>
                                <span class="no-wrap"><b>Depart:</b><span class="text-capitalize"> {{ $order->options->departure_from }}, </span> {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d/m/y').' | '.$order->options->departure_time }}</span>
                                <br>
                                <span class="no-wrap"><b>Return:</b>
                                    @if ($order->options->two_way)
                                    <span class="text-capitalize"> {{ $order->options->return_from }}, </span> {{ \Carbon\Carbon::parse($order->options->return_date)->format('d/m/y').' | '.$order->options->return_time }}
                                    @else
                                    <span>#N/A#</span>
                                    @endif
                                </span>
                            </td>
                            <td>{{ $quantity }}</td>
                            <td>${{ $pricePerUnit  }}</td>
                            <td>${{ $quantity * $pricePerUnit }}</td>
                        </tr>
                    @endif
                    @if ($wifiRental && ($wifiRental->status == \App\Enums\OrderDetailStatus::RESERVED || $wifiRental->status == \App\Enums\OrderDetailStatus::CHECKED_IN))
                        <tr>
                            <td>Wifi Rental</td>
                            <td>
                                <span class="no-wrap"><b>Duration:</b> {{ $wifiRental->options->number_of_days }} day(s)</span>
                                <br>
                                <span class="no-wrap"><b>Date:</b> {{ $wifiRental->start_date->format('d M Y') }} - {{ $wifiRental->end_date->format('d M Y') }}</span>
                            </td>
                            <td>{{ $wifiRental->quantity }}</td>
                            <td>${{ $wifiRental->price / $wifiRental->options->number_of_days }}</td>
                            <td>${{ $wifiRental->total }}</td>
                        </tr>
                    @endif
                    @if ($carRental && ($carRental->status == \App\Enums\OrderDetailStatus::RESERVED || $carRental->status == \App\Enums\OrderDetailStatus::CHECKED_IN))
                        <tr>
                            <td>Car Charter Rental ({{ $order->options->selected_car == 'car_5_seater' ? '5 Seat' : '10 Seat' }})</td>
                            <td>
                                <span class="no-wrap"><b>Duration:</b> {{ $carRental->options->number_of_days }} day(s)</span>
                                <br>
                                <span class="no-wrap"><b>Date:</b> {{ $carRental->start_date->format('d M Y') }} - {{ $carRental->end_date->format('d M Y') }}</span>
                            </td>
                            <td>{{ $carRental->quantity }}</td>
                            <td>${{ $carRental->price / $carRental->options->number_of_days }}</td>
                            <td>${{ $carRental->total }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td colspan="4">
                            <p class="mb-0 text-right"><b>Total payable price</b></p>
                        </td>
                        <td>${{ $order->total }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </main>
</body>
</html>