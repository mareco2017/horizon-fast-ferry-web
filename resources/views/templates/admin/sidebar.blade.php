<?php
$currentRoute = Route::getCurrentRoute();
$routeCollection = Route::getRoutes();
$routes = [
    (object) [
        'title' => 'Dashboard',
        'validate' => true,
        'icon' => 'now-ui-icons design_app',
        'href' => route('admin.dashboard'),
        'collection' => $routeCollection->getRoutesByName()['admin.dashboard']
    ],
    (object) [
        'title' => 'Users',
        'validate' => true,
        'icon' => 'now-ui-icons users_circle-08',
        'child' => [
            (object) [
                'title' => 'User List',
                'icon' => 'UL',
                'href' => route('admin.users.user.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.users.user.index']
            ],
        ]
    ],
    (object) [
        'title' => 'Order',
        'validate' => false,
        'icon' => 'now-ui-icons business_money-coins',
        'href' => route('admin.order.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.order.index']
    ],
    (object) [
        'title' => 'Passengers',
        'validate' => false,
        'icon' => 'now-ui-icons business_badge',
        'href' => route('admin.passenger.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.passenger.index']
    ],
    (object) [
        'title' => 'Articles',
        'validate' => true,
        'icon' => 'now-ui-icons education_paper',
        'href' => route('admin.article.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.article.index']
    ],
    (object) [
        'title' => 'Comments',
        'validate' => true,
        'icon' => 'now-ui-icons ui-2_chat-round',
        'href' => route('admin.comment.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.comment.index']
    ],
    (object) [
        'title' => 'Latest Promotion',
        'validate' => true,
        'icon' => 'now-ui-icons media-1_album',
        'href' => route('admin.latest-promotion.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.latest-promotion.index']
    ],
    (object) [
        'title' => 'System Config',
        'validate' => true,
        'icon' => 'now-ui-icons ui-2_settings-90',
        'href' => route('admin.config.index'),
        'collection' => $routeCollection->getRoutesByName()['admin.config.index']
    ],
    (object) [
        'title' => 'Preference',
        'validate' => true,
        'icon' => 'now-ui-icons design_bullet-list-67',
        'child' => [
            (object) [
                'title' => 'FAQ',
                'icon' => 'F',
                'href' => route('admin.preferences.faq.index'),
                'collection' => $routeCollection->getRoutesByName()['admin.preferences.faq.index']
            ],
        ]
    ],
]
?>
<div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="{{ asset('/assets/images/img-placeholder.png') }}" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <span>
                    {{ Auth::user()->fullname }}
                    <b class="caret"></b>
                </span>
            </a>
            <div class="clearfix"></div>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li>
                        {!! Form::open(['route' => 'admin.logout', 'method' => 'POST', 'name' => 'logoutForm']) !!}
                        {!! Form::close() !!}
                        <a href="" onclick="document.forms['logoutForm'].submit(); return false;">
                            <span class="sidebar-mini-icon">LO</span>
                            <span class="sidebar-normal">Log Out</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <ul class="nav">
        @foreach($routes as $key => $route)
            @if ($route->validate == true && Auth::user()->id == 1 || $route->validate == false)
            <?php
                if (isset($route->collection) && $route->collection) {
                    $active = $route->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                } else if (isset($route->child) && $route->child) {
                    $hasAccess = false;
                    $active = false;
                    $open = false;
                    foreach ($route->child as $childRoute) {
                        if (isset($childRoute->collection) && $childRoute->collection) {
                            if ($childRoute->collection->getPrefix() == $currentRoute->getPrefix()) {
                                $active = 'active';
                                $open = true;
                            }
                        }
                    }
                }
            ?>
            @if(isset($route->child))
                <li class="{{ $active }}">
                    <a data-toggle="collapse" href="#{{ 'dropdown'.$key }}" aria-expanded="{{ $open ? 'true' : 'false' }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}
                            <b class="caret"></b>
                        </p>
                    </a>
                    <div class="collapse {{ !$open ?: 'show' }}" id="{{ 'dropdown'.$key }}">
                        <ul class="nav mt-0">
                            @foreach($route->child as $child)
                            <?php
                                if (isset($child->collection) && $child->collection) {
                                    $childActive = $child->collection->getPrefix() == $currentRoute->getPrefix() ? 'active' : '';
                                }
                            ?>
                            <li class="{{ $childActive }}">
                                <a href="{{ $child->href }}">
                                    <span class="sidebar-mini-icon">{{ $child->icon }}</span>
                                    <span class="sidebar-normal">{{ $child->title }}</span>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @else
                <li class="{{ $active }}">
                    <a href="{{ $route->href }}">
                        <i class="{{ $route->icon }}"></i>
                        <p>{{ $route->title }}</p>
                    </a>
                </li>
            @endif
            @endif
        @endforeach
    </ul>
</div>