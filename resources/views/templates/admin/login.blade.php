@extends('structure')

@push('pageRelatedCss')
<link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet"/>
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('/now-ui-template/css/now-ui-dashboard.css?v=1.1.0') }}"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('/now-ui-template/css/now-ui-kit.css?v=1.2.0') }}">
@endpush

@section('body')
<body class="login-page sidebar-collapse cf-inspector-body">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent" color-on-scroll="400">
        <div class="container">
            
            <!-- <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-header">Dropdown header</a>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Separated link</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">One more separated link</a>
                </div>
            </div> -->
            

            <div class="navbar-translate">
                <a class="navbar-brand">
                    Horizon Fast Ferry 
                </a>
                <button class="navbar-toggler navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar top-bar"></span>
                    <span class="navbar-toggler-bar middle-bar"></span>
                    <span class="navbar-toggler-bar bottom-bar"></span>
                </button>
            </div>

            <div class="navbar-collapse justify-content-end collapse" id="navigation" style="" data-nav-image="../assets/img/blurred-image-1.jpg">
                <ul class="navbar-nav">
                
                    <li class="nav-item">
                        <a class="nav-link" href="/">Back to Dashboard</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="https://github.com/creativetimofficial/now-ui-kit/issues">Have an issue?</a>
                    </li>

                

                
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
                            <i class="fab fa-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
                            <i class="fab fa-facebook-square"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
                            <i class="fab fa-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li> -->
                
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" style="background-image:url(/assets/images/auth/login-bg.jpg)"></div>
        <div class="content">
            <div class="container">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="card card-login card-plain">
                        {!! Form::open(['route' => 'admin.login', 'method' => 'POST', 'class' => 'form']) !!}
                            <div class="card-header text-center">
                                <div class="logo-container">
                                    <img src="../assets/img/now-logo.png" alt="">
                                </div>
                                @if (Session::has('error'))
                                    <p class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </p>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="input-group no-border input-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="now-ui-icons users_circle-08"></i>
                                        </span>
                                        </div>
                                    <input type="text" name="username" class="form-control" placeholder="Email">
                                </div>

                                <div class="input-group no-border input-lg">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="now-ui-icons ui-1_lock-circle-open"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" placeholder="Password" class="form-control">
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary btn-round btn-lg btn-block">Login now</button>
                                <!-- <a href="#pablo" class="btn btn-primary btn-round btn-lg btn-block">Login now</a> -->

                                <div class="pull-left">
                                    <h6><a href="#" class="link">Create Account</a></h6>
                                </div>

                                <div class="pull-right">
                                    <h6><a href="#" class="link">Need Help?</a></h6>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
            
            <div class="container">
                <!-- <nav>
                    <ul>
                        <li>
                            <a href="https://www.creative-tim.com">
                                Creative Tim
                            </a>
                        </li>
                        <li>
                            <a href="http://presentation.creative-tim.com">
                            About Us
                            </a>
                        </li>
                        <li>
                            <a href="http://blog.creative-tim.com">
                            Blog
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div class="copyright" id="copyright">
                    © <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
                </div>
            </div>
        </footer>
    </div>
</body>
@endsection

@push('pageRelatedJs')
<script src="{{ asset('/now-ui-template/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- <script src="{{ asset('/now-ui-template/js/now-ui-dashboard.js?v=1.1.0') }}" type="text/javascript"></script> -->
<script src="{{ asset('/now-ui-template/js/now-ui-kit.min.js?v=1.2.0') }}" type="text/javascript"></script>
@endpush