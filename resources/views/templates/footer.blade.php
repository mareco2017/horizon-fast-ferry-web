@section('footer')
<footer class="footer py-4">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <p>Booking, review and advices on hotels, resorts, flights, vacation rentals, travel packages, and lots more!</p>
                <a href="https://mail.google.com/mail/u/0/?view=cm&fs=1&to=EDWARD.POH@HORIZONFASTFERRY.COM&tf=1" target="_blank" class="f-bold text-white">ADVERTISE ON OUR FERRY</a>
            </div>
            <div class="col-6">
                <p class="text-right mb-1 f-secondary fs-3">Have Questions?</p>
                <p class="text-right mb-1">+65 6276 6711</p>
                <p class="text-right mb-1">singapore@horizonfastferry.com</p>
            </div>
        </div>
    </div>
</footer>
@endsection