<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<style type="text/css" media="screen">
		html {
			width: 1000px;
			margin: auto;
			line-height: 20px;
			background-color: #eee;
		}

		body {
			margin: 0;
			font-family: ‘Open Sans’, Arial, sans-serif !important;
			background-color: #fff;
		}

		table {
			border-spacing: 0;
    		border-collapse: collapse;
		}

		.header {
			background-color: #0045a8;
			text-align: center;
			padding: 12px;
		}

		.header img {
			width: 92px;
		}

		.body {
			padding: 1.5rem;
			text-align: center;
			width: 600px;
			margin: auto;
		}

		p {
			margin: 0 0 1rem;
			color: #000;
		}

		.user-name {
			text-transform: uppercase;
		}

		.margin-y {
			margin: 1rem 0;
		}

		.barcode {
			display: inline-block;
			margin: auto auto 1.5rem;
		}

		.reference-number {
			margin: .25rem 0 1rem;
		}

		.bg-blue {
			background-color: #0045a8;
		}

		.ferry-booking {
			color: #fff;
			font-size: 22px;
			margin: 0;
			padding: 1rem 1.5rem;
			font-weight: bold;
		}

		.bg-cream {
			background-color: #fac598 !important;
		}

		.bg-light-cream {
			background-color: #f3e1d2 !important;
		}

		.transaction-ref {
			padding: .5rem 1.5rem;
			margin: 0;
			font-weight: bold;
		}

		.depart-text {
			padding: .5rem 1.5rem;
			margin: 0;
		}

		.bg-grey {
			background-color: #d6d6d6;
		}

		.seat {
			padding: .5rem 1.5rem;
			margin: 0;
		}

		.bg-light-grey {
			background-color: #e8e8e8;
			text-align: left;
		}

		.passenger {
			padding: 2.5rem 1.5rem;
		}

		.passenger p {
			margin: 0 0 .5rem;
		}

		.text-italic {
			padding: 0 1.5rem 1rem 1.5rem;
			margin: 0;
			font-style: italic;
			font-weight: bold;
		}

		.wifi-booking {
			background-color: #f3e1d2 !important;
			padding: 1.5rem;
		}
		
		.wifi {
			width: 80px;
		}
		
		.wifi-img {
			width: 50px;
    		padding-right: 30px;
		}

		.text-left {
			text-align: left;
		}

		.italic {
			font-style: italic;
		}

		.cancellation {
			font-style: italic;
			text-align: left;
			margin: 1rem 0 0;
		}

		.footer {
			text-align: center;
			margin: 3rem 0 5rem;
		}

		.footer p {
			margin: 0 0 .25rem;
		}

		.alert-danger {
			color: #761b18;
			background-color: #f9d6d5;
			border-color: #f7c6c5;
			position: relative;
			padding: 0.75rem 1.25rem;
			margin-bottom: 1rem;
			border: 1px solid transparent;
			text-align: center;
		}

		.wifi-failed {
			color: #e85f4b !important;
			font-size: 1.125rem;
			font-weight: 700 !important;
			margin: 0;
		}

		.charge-car {
			color: #0045a8 !important;
			font-weight: 700 !important;
			text-align: left;
		}

		.text-uppercase {
			text-transform: uppercase;
		}
	</style>
</head>
<body>
	<table width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<div class="header">
					<img src="{{ asset('/assets/icons/logo-alt.png') }}" alt="logo">
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="body">
					<p>
						Dear
						<span class="user-name">
							@if ($user)
								{{ $user->fullname }}
							@elseif (count($departureTickets) > 0)
								{{ $departureTickets[0]->passenger->name }}
							@elseif ($wifiOnly && $wifi)
								{{ $wifi->passenger->name }}
							@else
								user
							@endif
						</span>,
					</p>
					<p class="margin-y">Thank you for booking with Horizon Fast Ferry.</p>
					@if (count($departureTickets) > 0)
						<p class="margin-y">Please print out your E-Boarding Pass and proceed directly to custom for check in. Those withour a physical E-Boarding Pass will need to visit  <b>Horizon Fast Ferry counter 40 minutes before departure.</b></p>
						<p class="margin-y">If you did not receive an E-Boarding Pass for your departure or return trip, please visit the respective <b>Singapore or Batam Horizon Fast Ferry counter 40 minutes before your departure.</b></p>

						<p class="margin-y">Failure to do so will result in forfeiting of your tickets.</p>
						<p><b>Here is your booking reference: </b></p>
						<div class="barcode">
							{!! $barcode !!}
							<!-- <p class="reference-number">{{ $order->reference_number }}</p> -->
						</div>
					@endif
					@if ($wifiOnly)
						<p class="margin-y">Please collect your Wifi device at <b>Batam Horizon Fast Ferry Counter.</b></p>
					@endif
					@if (count($departureTickets) > 0)
						<div class="bg-blue">
							<p class="ferry-booking">FERRY BOOKING</p>
						</div>
						<div class="bg-light-cream">
							<p class="transaction-ref">Transaction Ref: {{ $order->options->departure_booking_reference }}</p>
						</div>
						<div class="bg-cream">
							<p class="depart-text"><b>Departure:</b> {{ $order->options->departure_from == 'batam' ? 'Batam, Harbour Bay' : 'SG, Harbourfront' }}<b>&nbsp;&nbsp;Date/time:</b> {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d M Y') }} {{ $order->options->departure_time }}</p>
						</div>
						@if ($order->options->two_way)
						<div class="bg-light-cream">
							<p class="transaction-ref">Transaction Ref: {{ $order->options->return_booking_reference }}</p>
						</div>
						<div class="bg-cream">
							<p class="depart-text"><b>Return:</b> {{ $order->options->return_from == 'batam' ? 'Batam, Harbour Bay' : 'SG, Harbourfront' }}<b>&nbsp;&nbsp;Date/time:</b> {{ \Carbon\Carbon::parse($order->options->return_date)->format('d M Y') }} {{ $order->options->return_time }}</p>
						</div>
						@endif
						<div class="bg-grey">
							<p class="seat"><b>Seat Class:</b> {{ TicketType::getString($order->options->type) }} x {{ $order->options->quantity }} pax</p>
						</div>
						<div class="bg-light-grey">
							<div class="passenger">
								@foreach ($departureTickets as $ticket)
								<p>
									{{-- <b>Passenger detail:</b><span class="text-uppercase"> {{ $ticket->passenger->passport_number }} / {{ $ticket->passenger->name }}</span> --}}
									<b>Passenger detail:</b>
									<span class="text-uppercase"> {{ $ticket->passenger->name }}</span>
								</p>
								@endforeach
							</div>
							<p class="text-italic">*) All tickets sold are non refundable. Reschedule of travel dates need to be made 72 hour in advance. Seats are subject to availability.<br><br>*) To amend your ferry schedule please email hff@horizonfastferry.com.  A admin fee of $10 will be charge to reschedule your ferry date or timing, payable to horizon counter when you collect your tickets.</p>
							@if ($order->options->two_way)
							<p class="text-italic">*) Please retain your return ticket for redeeming your return boarding pass in Horizon Fast Ferry counter.</p>
							@endif
						</div>
						<br>
					@endif

					@if ($wifi && ($wifi->status == \App\Enums\OrderDetailStatus::RESERVED || $wifi->status == \App\Enums\OrderDetailStatus::CHECKED_IN))
					<div class="bg-blue">
						<p class="ferry-booking">WIFI DEVICE BOOKING</p>
					</div>
					<div class="wifi-booking">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="wifi"><img class="wifi-img" src="{{ asset('/assets/icons/wifi.png') }}" alt="wifi"></td>
								<td class="text-left">
									<p><b>WIFI Device rental:</b> Yes</p>
									<p><b>Duration:</b> {{ $wifi->options->number_of_days }} days<b>&nbsp;Date:</b> {{ $wifi->start_date->format('d M Y') }} - {{ $wifi->end_date->format('d M Y') }}</p>
									<p><b>Due Date:</b> {{ $wifi->end_date->addDay()->format('d M Y') }} <b>&nbsp;&nbsp;No. of devices:</b> {{ $wifi->quantity }}</p>
									<p>
										<b>Pick up and return</b>
										device at Batam Horizon Ferry Counter Level 2 Bayfront Mall, #02-10 Harbour Bay Terminal
									</p>
									<p>Penalty of <b class="italic">SGD {{ $wifiPenaltyPrice }}/day</b> for late return of device.</p>
								</td>
							</tr>
						</table>
						<img width="100%" src="{{ asset('/assets/icons/route.png') }}" alt="route">
						<p class="cancellation"><b>Cancellation should be made 72 hours in advance for refund. Please email to wifi@horizonfastferry.com</b></p>
					</div>
					<br>
					@elseif ($wifi && $wifi->status == \App\Enums\OrderDetailStatus::ERROR)
					<div class="alert-danger">
						<p class="wifi-failed">Wifi booking failed due to insufficient fund<br><small>(Your credit card will not be charged)</small></p>
					</div>
					<br>
					@else
					@endif

					@if (isset($car) && $car && ($car->status == \App\Enums\OrderDetailStatus::RESERVED || $car->status == \App\Enums\OrderDetailStatus::CHECKED_IN))
					<div class="bg-blue">
						<p class="ferry-booking">CAR CHARTER BOOKING</p>
					</div>
					<div class="wifi-booking">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td class="wifi"><img class="wifi-img" src="{{ asset('/assets/icons/car.png') }}" alt="car"></td>
								<td class="text-left">
									<p><b>From:</b> {{ $car->start_date->format('d M Y') }}<b>&nbsp;&nbsp;Till:</b> {{ $car->end_date->format('d M Y') }}</p>
									<p><b>Duration:</b> {{ $car->options->number_of_days }} days (10 hours/day)</p>
									<p><b>Pick up time:</b> {{ $car->options->car_pick_up_at }} (Batam Time)</p>
									<p><b>Pick up point:</b> Will wait for you at {{ $order->options->car_checkpoint }}</p>
								</td>
							</tr>
						</table>
						<br/>
						<p class="text-left">
							Thank you for booking with us. We will contact you shortly for your driver detail.
						</p>
						<p class="charge-car">
							Overtime fees of IDR50.000 per hour will apply if you exceed the duration of this charter service, payable directly to the driver.
							<br>
							<br>
							For pick ups between 00:00-06:00 there will be a surcharge of IDR250.000 payable directly in cash to the driver.
						</p>
						<p class="cancellation">
							A admin charge of SGD 5 will be charged for any cancellations. Cancellation need to be made at least 72 hours prior to the activity, if not the full amount will be charged. Please email to 
							<b>carcharter@horizonfastferry.com</b>
						</p>
					</div>
					@elseif (isset($car) && $car && $car->status == \App\Enums\OrderDetailStatus::ERROR)
					<div class="alert-danger">
						<p class="wifi-failed">Car charter failed due to insufficient fund<br><small>(Your credit card will not be charged)</small></p>
					</div>
					@else
					@endif
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="footer">
					<p>Yours sincerely,</p>
					<p>Horizon Fast Ferry (previously known as Prima Ferries)</p>
					<p>1 Maritime Square #03-47 Harbourfront Centre,</p>
					<p>Singapore 099253</p>
					<br>
					<p><b>T</b> +65 6276 6711</p>
					<p><b>F</b> +65 6276 6551</p>
					<p><b>W</b> www.horizonfastferry.com</p>
				</div>
			</td>
		</tr>
	</table>
</body>
</html>



{{-- <p>Dear @if ($user) {{ $user->fullname }} @else user @endif,</p>
</br>

<p>Thank you for booking with Horizon Fast Ferry.</p>
</br>

<p>Please print out your E-Boarding Pass and proceed directly to custom for check-in. Those without a physical E-Boarding Pass will need to visit Horizonn Fast Ferry counter 40 minutes before your departure.</p>
<p>If you did not receive an E-Boarding Pass for your departure or return trip, please visit the respective Singapore or Batam Horizon counter 40 minutes before your departure.</p>
</br>

<p>Here is your booking detail:</p>
<p>Booking Reference: {{ $order->reference_number }}</p>
<p>Departure: {{ $order->options->departure_route }} on {{ \Carbon\Carbon::parse($order->options->departure_date)->format('d-M-Y') }} at {{ $order->options->departure_time }}</p>
@if ($order->options->two_way)
<p>Return: {{ $order->options->return_route }} on {{ \Carbon\Carbon::parse($order->options->return_date)->format('d-M-Y') }} at {{ $order->options->return_time }}</p>
@endif
<p>Seat Class: {{ TicketType::getString($order->options->type) }} x {{ $order->options->quantity }} pax</p>

</br>

<p>Boarding Pass (Departure):</p>
@foreach ($departureTickets as $ticket)
  <p>
    @if ($ticket->status == \App\Enums\OrderDetailStatus::ERROR)
      {{ $loop->iteration }}. {{ $ticket->options->error_description }} / {{ $ticket->passenger->name }}
    @else
      {{ $loop->iteration }}. {{ $ticket->options->boarding_pass_number }} / {{ $ticket->passenger->name }}
    @endif
  </p>
@endforeach

</br>

@if ($order->options->two_way)
  <p>Boarding Pass (Return):</p>
  @foreach ($returnTickets as $ticket)
    <p>{{ $loop->iteration }}. {{ $ticket->options->boarding_pass_number }} / {{ $ticket->passenger->name }}</p>
  @endforeach

  </br>
@endif

<p>Yours sincerely,</p>
<p>Horizon Fast Ferry (previously known as Prima Ferries)</p>
<p>1 Maritime Square #03-47</p>
<p>Harbourfront Centre,</p>
<p>Singapore 099253</p>

<p>T +65 6276 6711</p>
<p>F +65 6276 6551</p> --}}