<div class="modal fade modal-mini modal-primary" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalConfirmLabel" aria-hidden="true">
    <form action="/">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h5 class="modal-title" id="myModalConfirmLabel">Confirmation</h5>
                </div>
                <div class="modal-body">
                    <p class="mb-0" id="modalConfirmText">Are you sure to delete this data?</p>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-link btn-neutral">Yes</button>
                    <button type="button" class="btn btn-link btn-neutral" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

@push('pageRelatedJs')
<script>
$(document).ready(function(){
    var loadingHTML = '<span class="fas fa-circle-notch fa-spin"></span>';

    $('#modalConfirm').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        // var body = $(e.relatedTarget).data('body');
        var json = $(e.relatedTarget).data('json');
        // var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title') ? $(e.relatedTarget).data('title') : 'Confirmation';
        var message = $(e.relatedTarget).data('message') ? $(e.relatedTarget).data('message') : 'Are you sure to delete this data?';
        var action = $(e.relatedTarget).data('url');
        var redirect = $(e.relatedTarget).data('redirect') != undefined ? $(e.relatedTarget).data('redirect') : null;
        var tableName = $(e.relatedTarget).data('table-name') != undefined ? $(e.relatedTarget).data('table-name') : null;
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        var notifMessage = $(e.relatedTarget).data('notif-message') == undefined ? '' : $(e.relatedTarget).data('notif-message');
        $('#modalConfirm .modal-header #myModalConfirmLabel').text(title);
        $('#modalConfirm .modal-body #modalConfirmText').html(message);
        $('#modalConfirm form').attr('action', action).on("submit", function(e){
            let submitBtn = $('button[type=submit]');
            let oriHTML = submitBtn.html();
            let data = json ? $.param(json) : $(this).serialize();
            submitBtn.html(loadingHTML);
            submitBtn.prop('disabled', true);
            e.preventDefault();
            e.stopPropagation();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: action,
                type: method,
                data: data,
                success: function(res) {
                    if (typeof redirect === "string") {
                        window.location = redirect;
                    }
                    if (typeof tableName === "string") {
                        $(tableName).DataTable().ajax.reload();
                    }
                    $.notify({
                        icon: "now-ui-icons ui-1_bell-53",
                        message: notifMessage ? notifMessage : res.message
                    });
                },
                error: function (err) {
                    var errorMessage = "Process failed.";
                    if (err.responseJSON && err.responseJSON.message) {
                        errorMessage = err.responseJSON.message;
                    } else if (err.responseText) {
                        errorMessage = err.responseText;
                    }
                    $.notify({
                        icon: "now-ui-icons objects_spaceship",
                        message: errorMessage ? errorMessage : 'Something went wrong'
                    }, {
                        type: 'danger'
                    });
                },
                complete: function() {
                    $("#modalConfirm").modal('hide')
                    submitBtn.html(oriHTML);
                    submitBtn.prop('disabled', false);
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modalConfirm .modal-header p').text('');
        $('#modalConfirm .modal-body p').text('');
        $('#modalConfirm form').attr('action','/').off('submit');
    });
});
</script>
@endpush