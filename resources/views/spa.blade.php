<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="google-signin-client_id" content="288236375664-fvfncjntpuqiadn2na0fglnu86evf4i3.apps.googleusercontent.com">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

        <title>Horizon Ferry | Official Batam Ferry | 35 mins to Batam</title>
        {{-- <meta name="description" content="Economic and Business class. Priority queue in Batam custom clearance. Auto gate custom clearance for Foreigner. Wifi rental. Car Charter service. 10 minutes drive from Nagoya City Centre. Travel Content and info for first time traveller."> --}}
        <link rel="canonical" href="{{ URL::current() }}" />

        <meta property="fb:app_id" content="309559076426574">
        <meta property="og:type"content="website" />
        <meta property="og:site_name"content="Horizon Ferry" />
        <meta property="og:title"content="Horizon Ferry | Official Batam Ferry | 35 mins to Batam" />
        <meta property="og:description"content="Economic and Business class. Priority queue in Batam custom clearance. Auto gate custom clearance for Foreigner. Wifi rental. Car Charter service. 10 minutes drive from Nagoya City Centre. Travel Content and info for first time traveller." />
        <meta property="og:image"content="{{ asset('/assets/images/about-us/2.jpg') }}" />
        <meta property="og:url"content="{{ URL::current() }}" />

        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:site" content="@HorizonFerry">
        <meta property="twitter:title"content="Horizon Ferry | Official Batam Ferry | 35 mins to Batam" />
        <meta property="twitter:description"content="Economic and Business class. Priority queue in Batam custom clearance. Auto gate custom clearance for Foreigner. Wifi rental. Car Charter service. 10 minutes drive from Nagoya City Centre. Travel Content and info for first time traveller." />
        <meta property="twitter:image"content="{{ asset('/assets/images/about-us/2.jpg') }}" />

	    <link rel="stylesheet" type="text/css" href="{{ mix('/css/app.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
	    <link rel="stylesheet" type="text/css" href="{{ mix('/css/styles.css') }}"/>


        <!-- Facebook Pixel Code 
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1135910506596473'); 
            fbq('track', 'PageView');
            fbq('track', 'ViewContent');
            fbq('track', 'Search');
            fbq('track', 'Purchase', {value:'200.00', currency:'SGD'});
            fbq('track', 'Purchase', {value:'2000000.00', currency:'IDR'});
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=1135910506596473&ev=PageView &noscript=1"/>
        </noscript>
         End Facebook Pixel Code -->

         <!-- Facebook Pixel Code -->
         <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window,document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '1135910506596473'); 
                fbq('track', 'PageView');
            </script>
            <noscript>
                <img height="1" width="1" 
                src="https://www.facebook.com/tr?id=1135910506596473&ev=PageView
                &noscript=1"/>
            </noscript>
        <!-- End Facebook Pixel Code -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131579701-5"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-131579701-5');
        </script>

        <!-- Global site tag (gtag.js) - Google Ads: 772522198 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-772522198"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){
                dataLayer.push(arguments);
            } 
            gtag('js', new Date());
            gtag('config', 'AW-772522198');
        </script>

        <!-- Event snippet for horizon ferry purchase from ad 2 conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        
        <!-- Event snippet for horizon ferry purchase from ad 2 conversion page
        In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
        <script>
        function gtag_report_conversion(url) {
          var callback = function () {
            if (typeof(url) != 'undefined') {
              window.location = url;
            }
          };
          gtag('event', 'conversion', {
              'send_to': 'AW-772522198/P5TTCOiJv5oBENaBr_AC',
              'transaction_id': '',
              'event_callback': callback
          });
          return false;
        }
        </script>

    </head>
    <body>
        <div id="app"></div>
    </body>
    <script type="text/javascript" src="{{ asset('/js/snap.svg-min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript" src="{{ mix('/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/toastr.min.js') }}"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://apis.google.com/js/platform.js" defer></script>
    <script src="https://www.google.com/recaptcha/api.js?render=explicit" defer></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '309559076426574',
                autoLogAppEvents : true,
                xfbml            : true,
                cookie           : true,
                version          : 'v3.1'
            });
            FB.AppEvents.logPageView();
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.api('/me/permissions', function(response) {
                        var declined = [];
                        // console.log(response);
                    });
                    // The user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token 
                    // and signed request each expire.
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                } else if (response.status === 'authorization_expired') {
                    // The user has signed into your application with
                    // Facebook Login but must go through the login flow
                    // again to renew data authorization. You might remind
                    // the user they've used Facebook, or hide other options
                    // to avoid duplicate account creation, but you should
                    // collect a user gesture (e.g. click/touch) to launch the
                    // login dialog so popup blocking is not triggered.
                } else if (response.status === 'not_authorized') {
                    // The user hasn't authorized your application.  They
                    // must click the Login button, or you must call FB.login
                    // in response to a user gesture, to launch a login dialog.
                } else {
                    // The user isn't logged in to Facebook. You can launch a
                    // login dialog with a user gesture, but the user may have
                    // to log in to Facebook before authorizing your application.
                }
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));
  </script>
  <script type="text/javascript">
      window.stripeKey = "{{ config('services.stripe.key') }}";
  </script>
</html>
